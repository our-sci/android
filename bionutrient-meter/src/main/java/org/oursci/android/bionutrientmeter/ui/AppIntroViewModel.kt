package org.oursci.android.bionutrientmeter.ui

import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.ui.intro.BaseIntroViewModel
import org.oursci.android.common.vo.IntroSlide
import org.oursci.android.bionutrientmeter.App
import org.oursci.android.bionutrientmeter.R

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 25.07.17.
 */

class AppIntroViewModel(app: App) : BaseIntroViewModel(app) {

    val loginLiveData = ConsumableLiveData<(Boolean) -> Unit>()

    var loginSlide = IntroSlide(R.drawable.rfc_large
            , "Connect", "To start contributing log in or sign up to the Real Food Campaign network", "Log in or Sign up") {

        loginLiveData.value = { success ->
            if (success) {
                val v = slides.value
                v?.let {
                    v[2] = loginSuccessSlide()
                    slides.value = v
                }
            }
        }
    }

    fun loginSuccessSlide() =
            IntroSlide(R.drawable.rfc_large,
                    "Greetings ${app.user.username}", "you are successfully Logged in, continue to get started.")


    init {
        val slideArray = arrayOf(
                IntroSlide(R.drawable.rfc_large, "Bionutrient Sensor",
                        "Measure nutrient levels and collaborate in research"),
                if (loggedIn()) loginSuccessSlide() else loginSlide,
                IntroSlide(R.drawable.rfc_large
                        , "Supercharge the App", "Mark your data with Geo Location information for later evaluation.\n\n" +
                        "The App will ask you for permission to improve your data."))

        slides.value = slideArray
    }

    fun loggedIn(): Boolean {
        return !app.user.username.isEmpty()
    }
}