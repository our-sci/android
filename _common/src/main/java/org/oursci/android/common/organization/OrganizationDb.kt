package org.oursci.android.common.organization

import androidx.room.Room
import org.oursci.android.common.BaseApplication

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 10.05.18.
 */
class OrganizationDb(val app: BaseApplication) {
    val db by lazy {
        Room.databaseBuilder(app, OrganizationDatabase::class.java,
                "user_organizations")
                .fallbackToDestructiveMigration()
                .build()
    }


    companion object {

        private val lock = Object()

        private var sInstance: OrganizationDb? = null

        fun of(app: BaseApplication): OrganizationDb {
            synchronized(lock) {
                if (sInstance == null) {
                    sInstance = OrganizationDb(app)
                }

                return sInstance!!
            }
        }
    }
}