package org.oursci.android.common.survey

/**
 * Created by Manuel Di Cerbo on 09.11.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */

object Palette {

    val colors = arrayOf(
            0xC62828,
            0xD32F2F,
            0xF44336,
            0xD50000,
            0xEC407A,
            0xE91E63,
            0xAD1457,
            0x880E4F,
            0x9C27B0,
            0x7B1FA2,
            0x4A148C,
            0xAA00FF,
            0x673AB7,
            0x512DA8,
            0x311B92,
            0x651FFF,
            0x5C6BC0,
            0x3949AB,
            0x1A237E,
            0x304FFE,
            0x42A5F5,
            0x1976D2,
            0x0D47A1,
            0x0D47A1,
            0x03A9F4,
            0x0288D1,
            0x0277BD,
            0x0091EA,
            0x26C6DA,
            0x00ACC1,
            0x006064,
            0x00B8D4,
            0x009688,
            0x00796B,
            0x004D40,
            0x00BFA5,
            0x43A047,
            0x2E7D32,
            0x1B5E20,
            0x00C853,
            0x8BC34A,
            0x689F38,
            0x33691E,
            0x64DD17,
            0xFDD835,
            0xF9A825,
            0xF57F17,
            0xFFD600,
            0xFF5722,
            0xD84315,
            0xBF360C,
            0xDD2C00,
            0x8D6E63,
            0x6D4C41,
            0x4E342E,
            0x3E2723,
            0x616161
    )


    fun getColor(i: Int): Int {

        val r = colors.size
        val n = 4

        val c = i % r
        val idx = ((c * n) + c / n) % r

        return colors[idx]
    }
}