package org.oursci.android.common.ui.devices

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.adapters.ViewStubBindingAdapter
import org.joda.time.DateTime
import org.oursci.android.common.btlib.BluetoothStatus
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.R
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.device.bt.BtManager
import org.oursci.android.common.device.persist.DevicesDb
import org.oursci.android.common.device.persist.KnownBluetooth
import org.oursci.android.common.device.usb.UManager
import org.oursci.android.common.ui.BaseViewModel
import timber.log.Timber
import java.lang.Exception
import java.nio.charset.Charset

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 10.10.17.
 */
class DeviceListViewModel(app: BaseApplication) : BaseViewModel(app) {

    // deviceCache contains a list of Pair<Any, DeviceItemDisplay>
    var deviceCache = mutableSetOf<Pair<Any, DeviceItemDisplay>>()


    val deviceListResult = ConsumableLiveData<List<Pair<Any, DeviceItemDisplay>>>()
    val deviceConnectedResult = ConsumableLiveData<String>()
    val deviceScanDone = ConsumableLiveData<Void>()


    private var scanning = false
    private var pairing = false;


    private var deviceToConnect: BluetoothDevice? = null

    fun startDeviceScan() {
        scanning = true
        deviceCache.clear()

        UManager.of(app).scan()
        BtManager.of(app).scan()
    }

    fun stopDeviceScan() {
        BtManager.of(app).stopScan()
        scanning = false
    }

    fun clearCache() {
        synchronized(deviceCache) {
            deviceCache.clear()
            deviceListResult.postValue(createDeviceList())
        }
    }

    fun connect(device: UsbDevice) {
        UManager.of(app).apply {
            if (hasPermission(device)) {
                connect(device)
                deviceConnectedResult.postValue(if (connected.get()) device.deviceName else null)
                deviceListResult.postValue(createDeviceList())
            } else {
                requestPermission(device, { device ->
                    if (device == null) {
                        deviceConnectedResult.postValue(null)
                    } else {
                        connect(device)
                        deviceConnectedResult.postValue(if (connected.get()) device.deviceName else null)
                    }
                    deviceListResult.postValue(createDeviceList())
                })
            }
        }
    }

    fun connect(device: BluetoothDevice): Boolean {
        deviceToConnect = device

        if (!BluetoothAdapter.getDefaultAdapter().bondedDevices.contains(device)) {
            return false
        }

        BtManager.of(app).connect(device)
        return true
    }

    fun disconnect() {
        BtManager.of(app).cancelConnect(null)
    }

    fun disconnectUSB() {
        UManager.of(app).cancelConnect(null)
        deviceListResult.postValue(createDeviceList())
    }

    private val iconBt by lazy {
        colorIcon(R.drawable.ic_bluetooth_black_24dp)
    }

    private val iconUsb by lazy {
        colorIcon(R.drawable.ic_usb_black_24dp)
    }


    private fun colorIcon(icon: Int) = AppCompatResources.getDrawable(app, icon)?.let {
        DrawableCompat.setTint(it, ContextCompat.getColor(app, R.color.foregroundDeviceInfo))
        it
    }


    fun setupCallbacks() {
        BtManager.of(app).registerCallback({ device ->
            synchronized(deviceCache) {
                val state = when (device.bondState) {
                    BluetoothDevice.BOND_BONDED -> DeviceItemDisplay.State.PAIRED
                    BluetoothDevice.BOND_BONDING -> DeviceItemDisplay.State.CONNECTING
                    else -> DeviceItemDisplay.State.UNPAIRED
                }
                val display = DeviceItemDisplay(device, device.name
                        ?: "unknown name", device.address, iconBt, state)
                deviceCache.add(Pair(device, display))
                deviceListResult.postValue(createDeviceList())
            }
        }, { status ->
            when (status) {
                BluetoothStatus.CONNECTED -> {
                    app.setOurSciDevice(true) // TODO: figure out if this is an oursci device
                    deviceConnectedResult.postValue(deviceToConnect?.name)
                    addKnownBluetoothDevice(deviceToConnect)
                    deviceToConnect = null
                }
                BluetoothStatus.CONNECTING -> Timber.d("status connecting")
                BluetoothStatus.NONE -> {
                    deviceToConnect = null
                    deviceConnectedResult.postValue(null)
                }
            }
            deviceListResult.postValue(createDeviceList())

        }, { data ->
            Timber.d("data of device: %s", data)
        }, {
            scanning = false
            deviceScanDone.postValue(null)
        })

        UManager.of(app).registerCallback({ usbDevice: UsbDevice ->
            synchronized(deviceCache) {
                val display = DeviceItemDisplay(usbDevice, usbDevice.deviceName, "%04X:%04X\n%s".format(usbDevice.vendorId, usbDevice.productId,
                        UManager.of(app).info(
                                usbDevice.vendorId, usbDevice.productId
                        )), iconUsb)
                deviceCache.add(Pair(usbDevice, display))
                deviceListResult.postValue(createDeviceList())
            }
        }, { status ->
        }, { data ->
            Timber.d("received data from usb device: %s", data)
        }, {

        })

        deviceListResult.postValue(createDeviceList())
    }

    fun addKnownBluetoothDevice(device: BluetoothDevice?) {
        val kb = KnownBluetooth(device?.address
                ?: "??:??:??:??:??:??", device?.name
                ?: "unknown name", DateTime().millis, "")
        DevicesDb.of(app).db.knownBluetoothDao().insertKnownBluetooth(kb)
        deviceListResult.postValue(createDeviceList())
    }

    fun removeKnownBluetoothDevice(device: KnownBluetooth) {
        DevicesDb.of(app).db.knownBluetoothDao().deleteKnownBluetooth(device)
        deviceListResult.postValue(createDeviceList())
    }

    fun isScanning(): Boolean {
        return scanning
    }

    fun cancelConnect() {
        UManager.of(app).clearDevice()
        BtManager.of(app).cancelConnect(deviceToConnect)
    }

    fun isConnecting() = deviceToConnect != null

    fun isPairing() = pairing

    fun bond(device: BluetoothDevice) {
        pairing = true
        deviceToConnect = device
        BtManager.of(app).stopScan()
        device.setPin("1234".toByteArray(Charset.forName("UTF-8")))
        device.createBond()

        try {
            app.registerReceiver(pairingReceiver, IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED))
        } catch (e: Exception) {
            Timber.e(e)
        }
    }


    fun createDeviceList(): List<Pair<Any, DeviceItemDisplay>> {

        // get known Bluetooth devices
        val kbs = DevicesDb.of(app).db.knownBluetoothDao().getAll().map {
            it to DeviceItemDisplay(it, it.name, it.mac, iconBt, DeviceItemDisplay.State.KNOWN)
        }

        // set CONNECTED state in known Bluetooth devices if needed
        val cdi = currentDeviceInfo()

        if (isConnected() && cdi is BluetoothDevice) {
            kbs.forEach { knownDevice ->
                if (knownDevice.first.mac.equals(cdi.address, true)) {
                    knownDevice.second.state = DeviceItemDisplay.State.CONNECTED
                    knownDevice.second.title = "CONNECTED to " + knownDevice.second.title
                }
            }
        }

        // Usb devices
        val usbs = deviceCache.filter { it.first is UsbDevice }.map {
            val u = it.first as UsbDevice
            val display = DeviceItemDisplay(u, u.deviceName, "%04X:%04X\n%s".format(u.vendorId, u.productId,
                    UManager.of(app).info(
                            u.vendorId, u.productId
                    )), iconUsb, DeviceItemDisplay.State.PAIRED)

            if (isConnected() && cdi is UsbDevice) {
                display.state = DeviceItemDisplay.State.CONNECTED
                display.title = "CONNECTED to " + display.title
            }
            it.first to display
        }


        // return combined list and remove duplicates in scans
        return listOf(
                usbs.toList(),
                kbs.toList(),
                deviceCache.toList()
        ).flatten().distinctBy {
            val item = it.first
            when (item) {
                is KnownBluetooth -> item.mac
                is BluetoothDevice -> item.address
                else -> ""
            }
        }

    }

    fun currentDeviceInfo() = if (UManager.of(app).hasDevice()) {
        UManager.of(app).currentDevice()
    } else {
        BtManager.of(app).currentDevice()
    }

    fun isConnected(): Boolean = if (UManager.of(app).hasDevice()) {
        UManager.of(app).isConnected()
    } else if (BtManager.of(app).hasDevice()) {
        BtManager.of(app).isConnected()
    } else false

    val pairingReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intentArg: Intent?) {
            val intent = intentArg ?: return
            val state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, -1)
            Timber.d("action: %s", intent.action)
            Timber.d("bond state: %d", state)

            when (state) {
                BluetoothDevice.BOND_BONDING -> return
                BluetoothDevice.BOND_BONDED -> {
                    deviceToConnect?.let(::connect)
                }
                else -> {
                    deviceConnectedResult.postValue(null)
                    deviceToConnect = null
                }
            }

            pairing = false
            app.unregisterReceiver(this)

        }

    }

}