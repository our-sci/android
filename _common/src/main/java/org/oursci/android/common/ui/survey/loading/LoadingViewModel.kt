package org.oursci.android.common.ui.survey.loading

import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.ui.BaseViewModel
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceControl
import timber.log.Timber
import java.io.File
import kotlin.concurrent.thread

/**
 * Created by Manuel Di Cerbo on 27.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class LoadingViewModel(app: BaseApplication) : BaseViewModel(app) {

    val loadControlLiveData = ConsumableLiveData<Float>()
    val loadDoneLiveData = ConsumableLiveData<Void>()

    var loadingThread: Thread? = null

    fun loadSurveyImages(surveyName: String) {
        if (loadingThread != null) {
            return
        }

        loadingThread = thread {
            val survey = SurveyManager.of(app).loadFormFromName(surveyName)
            survey?.let {
                val urls = it.controls.flatMap {
                    when (it) {
                        is MultipleChoiceControl -> return@flatMap it.options.mapNotNull {
                            when (it.type) {
                                MultipleChoiceControl.Option.Type.TEXT -> null
                                MultipleChoiceControl.Option.Type.IMAGE -> it.url
                                MultipleChoiceControl.Option.Type.AUDIO -> null
                                MultipleChoiceControl.Option.Type.VIDEO -> null
                                MultipleChoiceControl.Option.Type.UNKNOWN -> null
                            }
                        }
                        else -> {
                            return@flatMap emptyList<String>()
                        }
                    }
                }

                if (urls.isEmpty()) {
                    loadDoneLiveData.postValue(null)
                    return@thread
                }

                val fraction = 1 / urls.size * 100f
                var progress = 0f

                urls.forEach {
                    Timber.d("loading %s", it)
                    val target = Glide.with(app)
                            .downloadOnly()
                            .load(it)
                            .listener(object : RequestListener<File> {
                                override fun onResourceReady(resource: File?, model: Any?, target: Target<File>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                    progress += fraction
                                    loadControlLiveData.postValue(progress)
                                    if (urls.last() == it) {
                                        loadDoneLiveData.postValue(null)
                                        loadingThread = null
                                    }

                                    return true
                                }

                                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<File>?, isFirstResource: Boolean): Boolean {
                                    loadDoneLiveData.postValue(null)
                                    loadingThread = null
                                    return true
                                }

                            })
                            .submit()
                }
            }
        }
    }

    fun createSurveyInstance(surveyId: String) {
        SurveyManager.of(app).createSurveyInstance(surveyId)
    }


}