package org.oursci.android.common.vo.survey

import org.javarosa.core.model.FormIndex
import org.javarosa.form.api.FormEntryController
import org.oursci.android.common.ui.controls.BaseControl
import java.io.File

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 21.09.17.
 */
class Survey(val controls: Array<BaseControl<*>>,
             val groups: Array<Group>,
             val name: String,
             val fec: FormEntryController, val instance: File? = null,
             val formId: String = "",
             val instanceID: String = "",
             val description: String = "",
             val startTime: Long = 0,
             val created: Long = 0,
             var modified: Long = 0,
             var uploaded: Boolean = false,
             val indexMap: Map<BaseControl<out Any>, FormIndex> = emptyMap(),
             var organizationId: String? = null,
             var summaryScript: String? = null,
             val groupIndexMap: Map<Group, FormIndex> = emptyMap())