package org.oursci.android.common.utils

import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.util.zip.ZipInputStream

/**
 * Created by Manuel Di Cerbo on 03.10.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class Compression {


    fun unzip(inputStream: InputStream, destination: File, date: Long? = null) {
        if (!destination.exists()) {
            Timber.d("creating directory %s", destination.absolutePath)
            if (!destination.mkdirs()) {
                throw IllegalArgumentException("unable to create directory ${destination.absolutePath}")
            }
        }

        if (!destination.isDirectory) {
            throw IllegalArgumentException("target location is not a directory: ${destination.absolutePath}")
        }

        if (!destination.canWrite()) {
            throw IllegalArgumentException("target location missing write permission: ${destination.absolutePath}")
        }

        val zipInputStream = ZipInputStream(inputStream)

        zipInputStream.use {
            while (true) {
                val entry = it.nextEntry ?: break

                if (entry.isDirectory) {
                    val d = File(destination.absolutePath + "/" + entry.name)
                    if (!d.isDirectory) {
                        if (!d.mkdirs()) {
                            throw IllegalArgumentException("unable to create directory of zip entry: ${d.absolutePath}")
                        }
                    }
                    continue
                }


                val outputStream = FileOutputStream(destination.absolutePath + "/" + entry.name)
                outputStream.use {
                    val len = zipInputStream.copyTo(outputStream)
                    Timber.d("copied bytes to ${destination.absolutePath + "/" + entry.name} : $len")
                }

                try {
                    File(destination.absolutePath, entry.name).setLastModified(date ?: entry.time)
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }
        }

        Timber.d("unzip finished")

    }
}