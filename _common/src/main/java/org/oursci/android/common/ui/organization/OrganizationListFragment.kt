package org.oursci.android.common.ui.organization

import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.FragmentGenericListBinding
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.GenericItemAdapter
import org.oursci.android.common.ui.GenericItemDisplay

import org.oursci.android.common.vo.Organization

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 09.05.18.
 */
class OrganizationListFragment : BaseFragment() {

    val adapter = GenericItemAdapter<Organization>(
            produceDisplay = {
                GenericItemDisplay(
                        it.name,
                        it.description
                )
            },
            onClick = { org, _ ->
                if (org.id == -1L) {
                    return@GenericItemAdapter
                }
                app().navigationManager.moveToOrganization(org)
            }
    )


    lateinit var binding: FragmentGenericListBinding
    val viewModel by lazyViewModel<OrganizationViewModel>()


    private val resultObserver = ConsumableObserver<List<Organization>>({ orgs, consumed ->

        if (consumed || orgs == null) {
            return@ConsumableObserver
        }

        if (orgs.isEmpty()) {
            adapter.items = listOf(
                    Organization(-1L, "", "Not part of an organization yet", "You are not part of an organization yet", -1, "", false)
            )
            adapter.notifyDataSetChanged()
            return@ConsumableObserver
        }

        adapter.items = orgs
        adapter.notifyDataSetChanged()
    })

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentGenericListBinding.inflate(inflater, container, false)
        binding.recyclerview.adapter = adapter
        binding.recyclerview.layoutManager = LinearLayoutManager(activity)
        binding.recyclerview.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))


        if (viewModel.isLoading.get()) {
            binding.recyclerview.visibility = View.GONE
            binding.progress.visibility = View.VISIBLE
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.loadResult.observe(this, resultObserver)
        viewModel.loadOrganizations()

        super.onViewCreated(view, savedInstanceState)
    }
}
