package org.oursci.android.common.server

import fi.iki.elonen.NanoWSD
import java.io.IOException

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 01.08.17.
 */
class WebsocketServer(port: Int) : NanoWSD("0.0.0.0", port) {

    override fun openWebSocket(handshake: IHTTPSession?): WebSocket {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    class SimpleWebSocket(handshake: IHTTPSession?) : WebSocket(handshake) {
        override fun onOpen() {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onClose(code: WebSocketFrame.CloseCode?, reason: String?, initiatedByRemote: Boolean) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onPong(pong: WebSocketFrame?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onMessage(message: WebSocketFrame?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onException(exception: IOException?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

    }
}