package org.oursci.android.common.ui.survey.results

import android.view.View
import org.oursci.android.common.vo.survey.SurveyResultInfo

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 13.09.19.
 */
class ReviewDisplay(var items: List<SurveyResultInfo>) {

    fun textErrors() = items.count { it.state == SurveyResultInfo.State.SCRIPT_ERROR }.let { "$it errors" }

    fun textWarnings() = items.count { it.state == SurveyResultInfo.State.SCRIPT_WARNING }.let { "$it warnings" }

    fun textIncomplete() = items.count { !it.done && it.state != SurveyResultInfo.State.UPLOADED }.let { "$it incomplete" }

    fun visibleErrors() = if (items.any { it.state == SurveyResultInfo.State.SCRIPT_ERROR }) View.VISIBLE else View.GONE

    fun visibleWarnings() = if (items.any { it.state == SurveyResultInfo.State.SCRIPT_WARNING }) View.VISIBLE else View.GONE

    fun visibleIncomplete() = if (items.any { !it.done && it.state != SurveyResultInfo.State.UPLOADED }) View.VISIBLE else View.GONE


}