package org.oursci.android.common.vo.survey.multiplechoice

import org.joda.time.DateTime
import org.oursci.android.common.vo.survey.BaseAnswer

/**
 * Created by Manuel Di Cerbo on 22.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class MultipleChoiceAnswer(
        override val answer: Array<String>? = null,
        answered: DateTime = DateTime(),
        modified: DateTime = DateTime(),
        anon: Boolean = false) : BaseAnswer<Array<String>>(answered, modified, anon) {
    override fun create(answer: Array<String>?, anon: Boolean)= if (this.answer == null) {
        MultipleChoiceAnswer(answer, DateTime(), DateTime(), anon)
    } else {
        MultipleChoiceAnswer(answer, this.answered, DateTime(), anon)
    }
}