package org.oursci.android.common.ui.survey.text

import android.annotation.SuppressLint
import androidx.lifecycle.ViewModel
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.ui.controls.text.TextControl
import org.oursci.android.common.ui.survey.ControlViewModel
import org.w3c.dom.Text
import kotlin.concurrent.thread

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 07.09.17.
 */

@SuppressLint("StaticFieldLeak")
class TextControlViewModel(app: BaseApplication) : ControlViewModel<TextControl>(app) {

    val dateLiveData = ConsumableLiveData<String?>()

    fun updateDate(date: String) {
        dateLiveData.postValue(date)
    }

}