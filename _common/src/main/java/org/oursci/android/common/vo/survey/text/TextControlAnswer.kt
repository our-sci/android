package org.oursci.android.common.vo.survey.text

import org.joda.time.DateTime
import org.oursci.android.common.vo.survey.BaseAnswer
import java.util.*

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 19.09.17.
 */
class TextControlAnswer(
        override val answer: String? = null,
        answered: DateTime = DateTime(),
        modified: DateTime = DateTime(),
        anon: Boolean = false
) : BaseAnswer<String>(answered, modified, anon) {
    override fun create(answer: String?, anon: Boolean) = if (this.answer == null) {
        TextControlAnswer(answer, DateTime(), DateTime(), anon)
    } else {
        TextControlAnswer(answer, this.answered, DateTime(), anon)
    }
}