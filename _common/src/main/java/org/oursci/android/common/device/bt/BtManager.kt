package org.oursci.android.common.device.bt

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import org.joda.time.DateTime
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.R
import org.oursci.android.common.btlib.*
import org.oursci.android.common.device.BaseDeviceManager
import org.oursci.android.common.device.persist.DevicesDb
import org.oursci.android.common.device.persist.KnownBluetooth
import org.oursci.android.common.pref
import org.oursci.android.common.storePref
import timber.log.Timber
import java.nio.charset.Charset
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean


/**
 * Created by Manuel Di Cerbo on 10.10.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class BtManager(app: BaseApplication) : BaseDeviceManager<BluetoothDevice>(app) {

    val devices = mutableSetOf<BluetoothDevice>()

    override val connected = AtomicBoolean(false)

    private val bt by lazy {
        BluetoothService.getDefaultInstance()
    }
    lateinit var writer: BluetoothWriter


    override var currentDevice: BluetoothDevice? = null

    companion object {
        private val lock = Object()
        private var sInstance: BtManager? = null

        fun of(app: BaseApplication): BtManager {
            synchronized(lock) {
                if (sInstance == null) {
                    sInstance = BtManager(app)
                }
                return sInstance!!
            }
        }
    }

    override fun scan() {
        bt.startScan()
    }

    fun stopScan() {
        bt.stopScan()
    }


    override fun reconnect() {
        currentDevice()?.let {
            currentDevice = it
            bt.disconnect()
            bt.connect(it)
        }
    }

    override fun write(data: String) {
        writer.write(data)
    }

    override fun cancelConnect(deviceToConnect: BluetoothDevice?) {
        //app.storePref(R.string.key_current_bt_mac, "")
        currentDevice = null
        bt.disconnect()
    }

    override fun isConnected() = connected.get()


    override fun hasDevice(): Boolean = DevicesDb.of(app).db.knownBluetoothDao().getAll().isNotEmpty()

    override fun init() {
        val config = BluetoothConfiguration()
        config.context = app
        config.bluetoothServiceClass = BluetoothClassicService::class.java // BluetoothClassicService.class or BluetoothLeService.class
        config.bufferSize = 8 * 4096
        config.characterDelimiter = '\n'
        config.deviceName = "Our Sci"
        config.callListenersInMainThread = false
        config.uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb")

        BluetoothService.init(config)
        writer = BluetoothWriter(bt)
    }

    override fun registerCallback(
            onScan: (BluetoothDevice) -> Unit,
            onStatusChange: (Any) -> Unit,
            onDataRead: (String) -> Unit, onScanDone: (() -> Unit)?) {
        bt.setOnScanCallback(object : BluetoothService.OnBluetoothScanCallback {
            override fun onStopScan() {
                Timber.d("on stop scan")
                onScanDone?.let { it() }
            }

            override fun onStartScan() {
                Timber.d("on start scan")
            }

            override fun onDeviceDiscovered(p0: BluetoothDevice?, p1: Int) {
                Timber.d("device with name ${p0?.name ?: "<missing name>"} discovered")
                p0?.let(onScan)
                p0?.let({ devices.add(it) })
            }
        })

        bt.setOnEventCallback(object : BluetoothService.OnBluetoothEventCallback {
            override fun onDataRead(p0: ByteArray?, p1: Int) {
                p0?.take(p1)?.toByteArray()?.toString(Charset.forName("utf-8"))?.let {

                    feedInterceptor("<font color='#040'>")
                    feedInterceptor(it.replace("\n", "<br>"))
                    feedInterceptor("</font>")


                    onDataRead(it)
                }
            }

            override fun onStatusChange(p0: BluetoothStatus) {
                Timber.d("onStatusChange: %s", p0)
                when (p0) {
                    BluetoothStatus.CONNECTED -> {
                        //app.storePref(R.string.key_current_bt_mac,
                        //        currentDevice?.address ?: "")

                        val kb = KnownBluetooth(currentDevice?.address
                                ?: "??:??:??:??:??:??", currentDevice?.name
                                ?: "unknown name", DateTime().millis, "")
                        DevicesDb.of(app).db.knownBluetoothDao().insertKnownBluetooth(kb)
                        connected.set(true)
                    }
                    BluetoothStatus.CONNECTING -> {
                    }
                    BluetoothStatus.NONE -> {
                        connected.set(false)
                    }

                }
                p0.let(onStatusChange)
            }

            override fun onDataWrite(p0: ByteArray?) {
                p0?.toString(Charset.forName("utf-8"))?.let {
                    feedInterceptor("<font color='blue'>")
                    feedInterceptor(it.replace("\n", "<br>"))
                    feedInterceptor("</font><br><br>")
                }
            }

            override fun onToast(p0: String?) {

            }

            override fun onDeviceName(p0: String?) {

            }
        })
    }


    override fun connect(device: BluetoothDevice) {
        currentDevice = device
        bt.connect(device)
    }

    override fun currentDevice(): BluetoothDevice? {
        if (!hasDevice()) {
            return null
        }

        //val mac = app.pref(R.string.key_current_bt_mac)
        val mac = DevicesDb.of(app).db.knownBluetoothDao().getAll().first().mac
        Timber.d("mac: ${mac}")
        return BluetoothAdapter.getDefaultAdapter().bondedDevices.firstOrNull {
            mac == it.address
        }
    }

}