package org.oursci.android.common.ui.survey.external

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.MultiAutoCompleteTextView
import androidx.lifecycle.ViewModelProviders
import org.oursci.android.common.R
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.ControlExternalSelectBinding
import org.oursci.android.common.databinding.LayoutControlFragmentBinding
import org.oursci.android.common.farmos.FarmOsDb
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.controls.external.ExternalControl
import org.oursci.android.common.ui.controls.external.ExternalControlDisplay
import org.oursci.android.common.ui.credentials.FarmOsViewModel
import timber.log.Timber


/**
 * Created by Manuel Di Cerbo on 16.03.19.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class ExternalFragment : BaseFragment() {

    override val searchable = true
    var idx = -1
    var selectedPosition = -1

    private lateinit var binding: LayoutControlFragmentBinding
    private var controlBinding: ControlExternalSelectBinding? = null
    private val viewModel: ExternalViewModel by lazy {
        ViewModelProviders.of(this, app().viewModelFactory).get(ExternalViewModel::class.java)
    }

    val farmOsViewModel by lazyViewModel<FarmOsViewModel>()


    data class AutocompleteItem(val key: String, val value: String, val format: (k: String, v: String) -> String) {
        override fun toString(): String {
            return format(key, value)
        }
    }

    val farmosFormat: (k: String, v: String) -> String = { k, v ->
        "$v ($k)"
    }
    val ontologiesFormat: (k: String, v: String) -> String = { k, v ->
        v
    }

    lateinit var adapter: ArrayAdapter<AutocompleteItem>

    fun buildAutocomplete() {

    }

    private val controlObserver = ConsumableObserver<ExternalControl> { control, consumed ->

        if (consumed || control == null) {
            return@ConsumableObserver
        }

        val url = when (control.params.type) {
            ExternalControl.Type.FARMOS_AREA -> FarmOsDb.of(app()).db.farmOsDao().getActiveServers().firstOrNull()?.url
            ExternalControl.Type.FARMOS_PLANTING -> FarmOsDb.of(app()).db.farmOsDao().getActiveServers().firstOrNull()?.url
            else -> null
        }


        controlBinding?.root?.postDelayed({
            buildAutoComplete(control)
        }, 500)

        val remembered = app().resolveRemember(control.dataName)



        binding.swRemember.visibility = View.VISIBLE
        binding.swRemember.isChecked = remembered?.let { true } ?: false


        val controlBinding = controlBinding ?: return@ConsumableObserver
        controlBinding.control = ExternalControlDisplay(control, url, null)
        val et = controlBinding.autocompleteTextview


        if (control.answer().isNullOrEmpty() && !viewModel.surveyUploaded()) { // only override if null or empty
            remembered?.let {
                val newAnswer = control.answer.create(it, control.answer.anon)
                control.answer = newAnswer
                viewModel.saveSurvey()
            }
        }

        controlBinding.autocompleteTextview.isEnabled = true
        controlBinding.autocompleteTextview.setOnEditorActionListener { v, actionId, event ->
            when (actionId) {
                EditorInfo.IME_ACTION_DONE -> onConfirmed()
                else -> false
            }
        }

        controlBinding.invalidateAll()
    }

    private fun buildAutoComplete(control: ExternalControl) {
        var error: String? = null
        // only override if null or empty
        when (control.params.type) {
            ExternalControl.Type.FARMOS_AREA -> {
                val farmOsAreas = farmOsViewModel.getActiveAreas().filter { it.type == "bed" || it.type == "field" }
                Timber.d("adding areas to adapter: " + farmOsAreas.size)

                if (farmOsAreas.isEmpty()) {
                    error = "unable to find farmos areas"
                }


                adapter.clear()
                adapter.addAll(farmOsAreas.map {
                    AutocompleteItem("server: ${it.farmOsServer}, tid: ${it.tid}", it.name, farmosFormat)
                })
                controlBinding?.autocompleteTextview?.setOnItemClickListener { parent, view, position, id ->
                    val selected = parent.getItemAtPosition(position) as? AutocompleteItem
                    if (selected == null) {
                        Timber.e("unable to find item at position %d", position)
                        return@setOnItemClickListener
                    }

                    controlBinding?.autocompleteTextview?.setText(selected.toString())
                    onConfirmed()
                }
            }
            ExternalControl.Type.FARMOS_PLANTING -> {
                val farmOsPlantings = farmOsViewModel.getActivePlantings()
                Timber.d("adding plantings to adapter: " + farmOsPlantings.size)

                if (farmOsPlantings.isEmpty()) {
                    error = "unable to find farmos plantings"
                }


                adapter.clear()
                adapter.addAll(farmOsPlantings.map {
                    AutocompleteItem("server: ${it.farmOsServer}, id: ${it.id}", it.name, farmosFormat)
                })
                controlBinding?.autocompleteTextview?.setOnItemClickListener { parent, view, position, id ->
                    val selected = parent.getItemAtPosition(position) as? AutocompleteItem
                    if (selected == null) {
                        Timber.e("unable to find item at position %d", position)
                        return@setOnItemClickListener
                    }

                    controlBinding?.autocompleteTextview?.setText(selected.toString())
                    onConfirmed()
                }
            }
            ExternalControl.Type.GENERIC -> {
            }
            ExternalControl.Type.ONTOLOGY -> {

                val adapterItems = try {
                    app().getAllOntologies().find {
                        it.id == control.params.url
                    }?.items?.map {
                        AutocompleteItem(it.id, it.name, ontologiesFormat)
                    } ?: emptyList()
                } catch (e: Exception) {
                    activity.showErrorDialog("Error fetching ontologies", "exception while fetching ontologies: %s".format(e.message
                            ?: ""))
                    emptyList<AutocompleteItem>()
                }

                if (adapterItems.isEmpty()) {
                    error = "unable to find ontology items"
                }

                adapter.clear()
                adapter.addAll(adapterItems)

                controlBinding?.autocompleteTextview?.setOnItemClickListener { parent, view, position, id ->
                    val selected = parent.getItemAtPosition(position) as? AutocompleteItem
                    if (selected == null) {
                        Timber.e("unable to find item at position %d", position)
                        return@setOnItemClickListener
                    }
                    controlBinding?.autocompleteTextview?.setText(selected.key)
                    onConfirmed()
                }
            }
        }

        adapter.notifyDataSetChanged()


        controlBinding?.control?.error = error
        controlBinding?.invalidateAll()
        controlBinding?.autocompleteTextview?.showDropDown()
    }


    private fun onConfirmed(): Boolean {
        Timber.d("done pressed")
        val controlBinding = controlBinding ?: return false
        val text = controlBinding.autocompleteTextview.text.toString().trim()
        val control = viewModel.control ?: return false


        val valueToRemember = if (binding.swRemember.isChecked) text else null
        app().storeRemember(control.dataName, valueToRemember)

        control.answer.let {
            if (it.answer == text) {
                Timber.d("same sate, not updating")
                return false
            }
        }

        val newAnswer = control.answer.create(text, control.answer.anon)
        control.answer = newAnswer



        Timber.d("new answer is: %s, anon: %s", control.answer, control.answer.anon)
        viewModel.saveSurvey()
        controlBinding.invalidateAll()

        return false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity.allowOverview(true)
        activity.unlockMenus()

        binding.btNext.setOnClickListener {

            onConfirmed()


            // TODO refactor all of these in fragments
            // makes more sense that the navigationManager
            // ultimately decides whether to move on or to final overview

            if (viewModel.hasMoreQuestions(idx)) {
                app().navigationManager.moveToNextSurveyQuestion(idx + 1)
            } else {
                app().navigationManager.finalOverview()
            }
        }
        binding.swAnon.visibility = View.GONE
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        idx = arguments?.getInt("idx", 0) ?: 0
        binding = LayoutControlFragmentBinding.inflate(inflater, container, false)
        adapter = ArrayAdapter(activity, android.R.layout.simple_dropdown_item_1line)

        val viewStub = binding.viewStubControl.viewStub ?: return binding.root
        viewStub.layoutResource = R.layout.control_external_select
        viewStub.setOnInflateListener { _, view ->
            controlBinding = ControlExternalSelectBinding.bind(view).apply {
                autocompleteTextview.setTokenizer(MultiAutoCompleteTextView.CommaTokenizer())
                autocompleteTextview.setAdapter(adapter)
                autocompleteTextview.isEnabled = false
                /*
                autocompleteTextview.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
                    selectedPosition = position
                    autocompleteTextview.onEditorAction(EditorInfo.IME_ACTION_DONE)
                }
                */
                //autocompleteTextview.setOnTouchListener { v, event ->
                //    (v as AutoCompleteTextView).showDropDown()
                //   false
                //}

            }

            viewModel.loadControlLiveData.observe(this, controlObserver)
            viewModel.loadControl(idx, ExternalControl::class.java)

        }

        viewStub.inflate()
        return binding.root
    }

}