package org.oursci.android.common.webapi


import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 14.03.19.
 */
interface FarmOSApi {

    @GET("taxonomy_term.json")
    fun getAreas(@Header("Cookie") cookie: String,
                 @Header("X-CSRF-Token") token: String,
                 @Query("vocabulary") vocabulary: Int,
                 @Query("page") page: Int): Call<ResponseBody>

    @GET("farm_asset.json")
    fun getPlantings(@Header("Cookie") cookie: String,
                     @Header("X-CSRF-Token") token: String,
                     @Query("bundle") bundle: String = "planting",
                     @Query("page") page: Int): Call<ResponseBody>

    @GET("taxonomy_vocabulary.json")
    fun getAreasVocab(@Header("Cookie") cookie: String, @Header("X-CSRF-Token") token: String): Call<ResponseBody>

    @FormUrlEncoded
    @Headers(value = ["User-Agent: curl/7.58.0", "Content-Type: application/x-www-form-urlencoded", "Accept: */*"])
    @POST("user/login")
    fun authenticate(@Field("name") username: String,
                     @Field("pass") password: String,
                     @Field("form_id") formId: String = "user_login"): Call<ResponseBody>

    @GET("restws/session/token")
    fun getToken(@Header("Cookie") cookie: String): Call<ResponseBody>
}