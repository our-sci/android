package org.oursci.android.common.ui.controls

import org.javarosa.form.api.FormEntryPrompt
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.vo.survey.Group

/**
 * Created by Manuel Di Cerbo on 06.11.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
interface IControlAdapter<out T : BaseControl<*>> {

    fun produce(prompt: FormEntryPrompt, group: Group?): T?
    fun resolve(app: BaseApplication, control: BaseControl<*>): BaseFragment

}