package org.oursci.android.common.ui.survey.overview

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.*
import android.widget.Toast
import org.oursci.android.common.*
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.FragmentSurveyOverviewBinding
import org.oursci.android.common.organization.OrganizationDb
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.controls.measurement.MeasurementControl
import org.oursci.android.common.ui.survey.measurement.MeasurementViewModel
import org.oursci.android.common.vo.survey.Survey
import org.oursci.android.common.vo.survey.SurveyOverviewItem
import timber.log.Timber
import kotlin.concurrent.thread

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 18.09.17.
 */
class SurveyOverviewFragment : BaseFragment() {

    private var autoRun: Boolean = false
    private lateinit var binding: FragmentSurveyOverviewBinding

    private val measurementViewModel: MeasurementViewModel by lazy {
        ViewModelProviders.of(this, app().viewModelFactory).get(MeasurementViewModel::class.java)
    }

    private val adapter = SurveyOverviewAdapter { item ->
        Timber.d("item %s was pressed", item.title)
        activity.app.navigationManager.rebuildSurveyStack((item.index).toInt() - 1, moveToFirstControlInGroup = true)
    }

    private val viewModel: SurveyOverviewViewModel by lazy {
        ViewModelProviders.of(this, app().viewModelFactory).get(SurveyOverviewViewModel::class.java)
    }

    private val resultObserver = Observer<Array<SurveyOverviewItem>> { items ->

        items?.let {
            adapter.items.clear()
            adapter.items.addAll(items)
            adapter.notifyDataSetChanged()
        }

        val lastIndex = adapter.items.indexOfFirst {
            !it.done
        }

        if (lastIndex >= 0) {
            val idx = Math.min(lastIndex + 1, adapter.items.size - 1)
            binding.recyclerView.smoothScrollToPosition(idx)
        }

    }

    private val exportedObserver = ConsumableObserver<String> { t: String?, consumed: Boolean ->
        if (consumed) {
            return@ConsumableObserver
        }

        if (t == null) {
            toast("error exporting CSV")
        } else {
            toast("exported CSV to $t")
        }
    }


    private val uploadObserver = ConsumableObserver<Survey> { survey: Survey?, consumed: Boolean ->
        if (consumed) {
            return@ConsumableObserver
        }

        if (survey == null) {
            toast("error uploading survey")
            return@ConsumableObserver
        }

        if (survey.uploaded) {
            toast("survey uploaded")
            val summaryScript = survey.summaryScript ?: ""
            if (summaryScript.isNotEmpty()) {
                Timber.d("Running Summary Script: %s", summaryScript)
                app().navigationManager.navigateToSummaryFragment()
            } else {
                app().navigationManager.navigateToSurveyResultsList(finish = true, fromSurvey = SurveyManager.of(app()).currentSurvey?.formId
                        ?: "")
            }
        } else {
            toast("error uploading survey")
        }

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //activity.lockMenus()
        activity.unlockMenus()

        viewModel.loadSurveyControlsLiveData.observe(this, resultObserver)
        viewModel.loadControls()

        viewModel.exportDataResult.observe(this, exportedObserver)
        viewModel.uploadResult.observe(this, uploadObserver)

        activity.allowOverview(false)

        app().getCurrentContributionOrg()?.let {
            binding.tvHeader.text = "contributing to ${it.name}"
            binding.tvHeader.visibility = View.VISIBLE
        }

        if (autoRun) {
            viewModel.uploadSurvey()
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val isFinal = arguments?.getBoolean("final", false) ?: false
        autoRun = arguments?.getBoolean("autoRun", false) ?: false
        val currentSurvey = SurveyManager.of(app()).currentSurvey

        binding = FragmentSurveyOverviewBinding.inflate(inflater, container, false)
        binding.btDone.setOnClickListener { _ ->
            val survey = SurveyManager.of(app()).currentSurvey
            Timber.d("done pressed")
            val summary = currentSurvey?.summaryScript ?: ""
            if (summary.isNotEmpty()) {
                app().navigationManager.navigateToSummaryFragment()
                return@setOnClickListener
            }

            if (isFinal) {
                app().navigationManager.navigateToSurveyResultsList(finish = survey?.uploaded
                        ?: false, fromSurvey = survey?.formId
                        ?: "")
            } else {
                app().navigationManager.navigateToSurveyResultsList(finish = survey?.uploaded
                        ?: false, fromSurvey = survey?.formId
                        ?: "")
            }

//            val fec = SurveyManager.of(app()).loadSuvey(
//                    formPath = "soil_carbon.xml",
//                    instanceFile = File(File(app().filesDir, "instances"), "first_instance.xml"),
//                    isAsset = true)
        }

        binding.btUpload.setOnClickListener { _ ->
            startCheck()
        }

        SurveyManager.of(app()).currentSurvey?.let {
            if (it.uploaded) {
                binding.btUpload.text = "Resend"
            } else {
                binding.btUpload.text = "Send"
            }
        }

        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(activity)
        // binding.recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        return binding.root
    }

    override fun menuItems() = arrayOf(
            BaseActivity.MenuItemInfo(Menu.NONE, R.id.menu_item_delete, 0, "Delete",
                    R.drawable.ic_delete_black_24dp, false),
            BaseActivity.MenuItemInfo(Menu.NONE, R.id.menu_export, 0, "Export CSV to SD-Card", R.drawable.ic_sd_storage_black_24dp, false)
            //BaseActivity.MenuItemInfo(Menu.NONE, R.id.menu_upload, 0, "Upload", R.drawable.ic_cloud_upload_black_24dp, true)
    )


    private fun startCheck() {
        SurveyManager.of(app()).currentSurvey?.let(::checkRequired)
    }

    private fun checkRequired(survey: Survey) {

        Timber.d("allRequiredAnswered is ${survey.allRequiredAnswered()}")

        val basicCheck: () -> Unit = {
            if (survey.allRequiredAnswered()) {
                checkUploaded(survey)
            } else {
                dialog(activity) {
                    title = "Survey not complete"
                    message = "Some required answers are not set, would you still like to send?"
                    positive = action("Send") {
                        checkUploaded(survey)
                    }
                    neutral = action("Resolve") {
                        val undone = adapter.items.indexOfFirst {
                            !it.done
                        }
                        if (undone >= 0) {
                            activity.app.navigationManager.rebuildSurveyStack(undone, moveToFirstControlInGroup = true)
                        }
                    }
                }
            }
        }

        if (survey.hasMeasurementWarningsOrErrors()) {
            dialog(activity) {
                title = "Errors / Warnings in Scripts"
                message = "Some Scripts have Errors and/or Warnings"
                positive = action("Review ...") {
                    val scriptIndex = adapter.items.indexOfFirst {
                        it.warning || it.error
                    }
                    if (scriptIndex >= 0) {
                        activity.app.navigationManager.rebuildSurveyStack(scriptIndex, moveToFirstControlInGroup = true)
                    }
                }
                neutral = action("Ignore") {
                    basicCheck()
                }
            }
        } else basicCheck()

    }

    private fun checkUploaded(survey: Survey) {
        if (!survey.uploaded) {
            checkOrganization(survey)
        } else {
            AlertDialog.Builder(activity, activity.alertDialogTheme)
                    .setTitle("Survey already sent")
                    .setMessage("The survey has already been submitted. Would you like to resend it?")
                    .setPositiveButton("Resend") { dialog, _ ->
                        checkOrganization(survey)
                        dialog.dismiss()
                    }.setNegativeButton("Cancel") { dialog, _ ->
                        dialog.dismiss()
                    }.create().show()
        }
    }

    private fun autoRunAndUpload(survey: Survey) {
        if (!survey.allRelevantScriptsDone()) {
            AlertDialog.Builder(activity, activity.alertDialogTheme)
                    .setTitle("Run Scripts")
                    .setMessage("Some relevant scripts have not been run yet.")
                    .setPositiveButton("Run now and Submit") { dialog, _ ->
                        measurementViewModel.autoRunNext(0)
                        dialog.dismiss()
                    }.setNegativeButton("Cancel") { dialog, _ ->
                        dialog.dismiss()
                    }.setNeutralButton("Upload incomplete Data") { dialog, _ ->
                        dialog.dismiss()
                        viewModel.uploadSurvey()
                    }
                    .create().show()
        } else {
            viewModel.uploadSurvey()
        }
    }

    private fun checkOrganization(survey: Survey) {
        when {
            !app().user.loggedIn() -> autoRunAndUpload(survey)
            app().isOrganizationSelected() -> autoRunAndUpload(survey)
            !app().allowOrganizations -> autoRunAndUpload(survey)
            app().getCurrentContributionOrg() != null -> autoRunAndUpload(survey)
            else -> {
                thread {
                    val db = OrganizationDb.of(app()).db.orgainzationDao()
                    val orgs = db.getAllOrganizations()
                    if (orgs.isNotEmpty()) {
                        activity.showSelectOrganizationDialog(orgs) {
                            autoRunAndUpload(survey)
                        }
                    } else {
                        autoRunAndUpload(survey)
                    }
                }
            }

        }
    }

    override fun onMenuItemPressed(menuItemId: Int) = when (menuItemId) {
        R.id.menu_item_delete -> {
            Timber.d("menu item delete pressed")

            val icon = ContextCompat.getDrawable(activity, R.drawable.ic_delete_black_24dp)?.let {
                DrawableCompat.setTint(it, ContextCompat.getColor(activity, R.color.colorAccent))
                it
            }

            AlertDialog.Builder(activity, activity.alertDialogTheme)
                    .setTitle("Delete Survey")
                    .setMessage("Deleting this Survey will remove it of the overview and into the trash folder")
                    .setPositiveButton("Delete") { dialogInterface, _ ->
                        dialogInterface.dismiss()
                        if (viewModel.deleteSurvey()) {
                            activity.app.navigationManager.navigateToSurveyResultsList(true, surveyDeleted = true)
                        } else {
                            Toast.makeText(activity, "Unable to delete Survey", Toast.LENGTH_SHORT).show()
                        }
                    }.setCancelable(true)
                    .setNegativeButton("Cancel") { dialogInterface, _ ->
                        dialogInterface.dismiss()
                    }
                    .setIcon(icon)
                    .create()
                    .show()
            true
        }
        R.id.menu_export -> {
            Timber.d("exporting to csv")
            viewModel.export()
            true
        }
        R.id.menu_upload -> {
            startCheck()
            true
        }
        else -> {
            super.onMenuItemPressed(menuItemId)
        }
    }


}