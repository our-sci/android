package org.oursci.android.common.ui.survey.results

import android.widget.Toast
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.logSurveyEvent
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.survey.SurveyResultInfoDb
import org.oursci.android.common.ui.BaseViewModel
import org.oursci.android.common.vo.survey.SurveyResultInfo
import timber.log.Timber
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 20.09.17.
 */
class SurveyResultListViewModel(app: BaseApplication) : BaseViewModel(app) {

    data class UploadResult(val success: Boolean, val review: Boolean = false)

    val loadSurveyResults = ConsumableLiveData<List<SurveyResultInfo>>()
    val exportingDoneResult = ConsumableLiveData<String?>()
    val deleteResult = ConsumableLiveData<Boolean>()
    val uploadResult = ConsumableLiveData<UploadResult>()
    val uploadProgress = ConsumableLiveData<Pair<Int, String>>()

    val loading = AtomicBoolean(false)
    val exporting = AtomicBoolean(false)
    val deleting = AtomicBoolean(false)
    val uploading = AtomicBoolean(false)

    var filter = ""


    var reviewAfterUpload =  AtomicBoolean(false)


    fun loadSurveyResults() {

        if (loading.getAndSet(true)) {
            return
        }

        thread {
            var res = SurveyResultInfoDb.of(app).db.surveyInfoDao().apply {
                val numResults = numSurveyResults()
                Timber.d("num survey results: %d", numResults)
                if (numResults == 0) {
                    insertSurveyInfo(SurveyManager.of(app).loadSurveyResultsList())
                }
            }.getAllSurveyInfos()


            if (filter.isNotEmpty()) {
                res = res.filter {
                    it.textSearchAsList().forEach {
                        Timber.d("item: %s", it)
                    }
                    it.textSearchAsList().any { it.contains(filter) }
                }
            }

            //SurveyManager.of(app).loadSurveyResultsList()
            loading.set(false)
            loadSurveyResults.postValue(res)
        }
    }

    fun toSurvey(surveyResultInfo: SurveyResultInfo) {
        thread {
            SurveyManager.of(app).apply {
                currentSurvey = try {
                    loadSurveyFromName(
                            surveyResultInfo.name,
                            surveyResultInfo.instance
                    )
                } catch (e: Exception) {
                    Timber.e(e)
                    return@thread
                }
            }

            Timber.d("navigating to overview for survey %s", surveyResultInfo.instance.absolutePath)
            app.navigationManager.overviewFromSurveyResult()
        }
    }

    fun toSurvey(surveyResultInfo: SurveyResultInfo, index: Int) {
        // TODO: implement jumping to a survey and question at index
    }


    fun exportAll() {
        if (exporting.getAndSet(true)) {
            return
        }

        thread {
            val result = try {
                SurveyManager.of(app).exportAllSurveys()
            } catch (e: Exception) {
                Timber.e(e)
                null
            }

            exportingDoneResult.postValue(result)
            exporting.set(false)
        }

    }

    fun deleteSurveys(surveys: List<SurveyResultInfo>): Boolean {
        if (deleting.getAndSet(true)) {
            return false
        }

        thread {
            deleteResult.postValue(SurveyManager.of(app).deleteSurveys(surveys))
            deleting.set(false)
        }

        return true
    }

    fun upload(items: List<SurveyResultInfo>): Boolean {
        if (uploading.getAndSet(true)) {
            return false
        }

        thread {
            try {
                items.forEachIndexed { index, surveyResultInfo ->

                    val progress = (index.toFloat() / items.size) * 100
                    uploadProgress.postValue(progress.toInt() to "uploading %s".format(surveyResultInfo.instance.name))

                    SurveyManager.of(app).apply {
                        loadSurveyFromName(surveyResultInfo.name, surveyResultInfo.instance)?.let {
                            uploadSurvey(it)
                            it.uploaded = true
                            it.organizationId = app.getCurrentContributionOrg()?.id
                                    ?: app.defaultOrganizationId

                            Timber.d("saving survey: %s", it.instance?.name ?: "<no instance>")
                            saveSurvey(it)
                            updateSurveyInfoDb(it)
                        }
                    }

                }
                uploadResult.postValue(UploadResult(success = true, review = reviewAfterUpload.getAndSet(false)))
            } catch (e: Exception) {
                Timber.e(e)
                uploadResult.postValue(UploadResult(success = false, review = reviewAfterUpload.getAndSet(false)))
            }

            uploading.set(false)
        }

        return true
    }

    fun busy(): Boolean {
        return uploading.get() || exporting.get() || deleting.get() || loading.get()
    }

    fun showNextSnack(fromSurvey: String) {
        app.snackWithAction("Start next Survey", "Start") {
            logSurveyEvent(fromSurvey)

            thread {
                SurveyManager.of(app).createSurveyInstance(fromSurvey)
                app.navigationManager.startSurvey(true)
            }

        }
    }
}