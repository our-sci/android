package org.oursci.android.common.ui.survey.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import org.oursci.android.common.BaseActivity
import org.oursci.android.common.R
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.arch.Observable
import org.oursci.android.common.arch.FreshObserver
import org.oursci.android.common.databinding.DialogReviewBinding
import org.oursci.android.common.databinding.FragmentGenericListBinding
import org.oursci.android.common.dialog
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.survey.created
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.GenericItemAdapter
import org.oursci.android.common.ui.GenericItemDisplay
import org.oursci.android.common.ui.qrscanner.QrScannerViewModel
import org.oursci.android.common.ui.survey.results.ReviewDisplay
import org.oursci.android.common.ui.survey.results.SurveyResultListViewModel
import org.oursci.android.common.vo.survey.SurveyResultInfo
import timber.log.Timber
import java.lang.ref.WeakReference
import kotlin.concurrent.thread

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 20.09.17.
 */
class SurveyResultListFragment : BaseFragment() {

    var sharedView: WeakReference<View>? = null
    var sharedItem: SurveyResultInfo? = null

    override val searchable = true

    private val iconSurveyDone by lazy {
        colorIcon(R.drawable.ic_assignment_turned_in_black_24dp)
    }

    private val iconSurveyUploaded by lazy {
        colorIcon(R.drawable.ic_cloud_done_black_24dp)
    }

    private val iconSurveyReadyToUpload by lazy {
        colorIcon(R.drawable.ic_cloud_queue_black_24dp)
    }

    private val iconSurveyExported by lazy {
        colorIcon(R.drawable.ic_code_black_24dp)
    }

    private val iconWarning by lazy {
        simpleIcon(R.drawable.ic_warning_black_24dp)
    }

    private val iconError by lazy {
        simpleIcon(R.drawable.ic_error_24dp)
    }

    private fun colorIcon(icon: Int) = ContextCompat.getDrawable(activity, icon)?.let {
        DrawableCompat.setTint(it, ContextCompat.getColor(activity, R.color.colorPrimary))
        it
    }

    private fun simpleIcon(icon: Int) = ContextCompat.getDrawable(activity, icon)?.let {
        it
    }


    enum class Review {
        INCOMPLETE,
        WARNING,
        ERROR
    }

    var uploadErrorResult = Observable<Boolean>()
    var uploadWarningResult = Observable<Boolean>()
    var uploadIncompleteResult = Observable<Boolean>()
    var uploadReviewResult = Observable<Boolean>()


    val adapter = GenericItemAdapter(
            produceDisplay = { surveyResultInfo: SurveyResultInfo ->
                GenericItemDisplay(
                        title = surveyResultInfo.title,
                        description = surveyResultInfo.let {
                            "<small>%s</small><br><br>%s".format(it.created(),
                                    it.description
                                            .split("<br>")
                                            .take(3)
                                            .joinToString("<br>")
                            ).let {
                                if (surveyResultInfo.organizationName.isNotEmpty()) {
                                    "%s<br><small>contributed to</small> %s".format(it, surveyResultInfo.organizationName)
                                } else it
                            }
                        },
                        showLoading = surveyResultInfo.state == SurveyResultInfo.State.UPLOADING,
                        iconLeft = if (surveyResultInfo.selected) colorIcon(R.drawable.ic_check_circle_24dp) else null,
                        iconRight = when (surveyResultInfo.state) {
                            SurveyResultInfo.State.ONGOING -> null
                            SurveyResultInfo.State.UPLOADING -> null
                            SurveyResultInfo.State.UPLOADED -> iconSurveyUploaded
                            SurveyResultInfo.State.EXPORTED -> iconSurveyExported
                            SurveyResultInfo.State.SCRIPT_ERROR -> iconError
                            SurveyResultInfo.State.SCRIPT_WARNING -> iconWarning
                            else -> {
                                null
                            }
                        },
                        secondIconRight = if (surveyResultInfo.done) {
                            if (surveyResultInfo.state == SurveyResultInfo.State.UPLOADED) null else iconSurveyReadyToUpload
                        } else null)
            },
            onClick = { surveyResultInfo: SurveyResultInfo, view: View ->
                if (items.any { it.selected }) {
                    surveyResultInfo.selected = !surveyResultInfo.selected
                    activity.invalidateOptionsMenu()
                    itemChanged(surveyResultInfo)
                } else {
                    viewModel.toSurvey(surveyResultInfo)
                }

                updateTitle()
            },
            onLongClick = { surveyResultInfo: SurveyResultInfo, _: View ->
                surveyResultInfo.selected = !surveyResultInfo.selected
                activity.invalidateOptionsMenu()
                updateTitle()
            }
    )


    private fun itemChanged(surveyResultInfo: SurveyResultInfo) {
        adapter.notifyItemChanged(surveyResultInfo)
    }


    private var items: List<SurveyResultInfo> = emptyList()
    private val resultObserver = ConsumableObserver<List<SurveyResultInfo>> { items, consumed ->

        if (consumed || viewModel.loading.get()) {
            return@ConsumableObserver
        }



        this.items = items ?: emptyList()

        updateAdapter()

        activity.invalidateOptionsMenu()

        binding.tabLayout.visibility = View.VISIBLE
        binding.recyclerview.visibility = View.VISIBLE
        binding.progress.visibility = View.GONE

        binding.recyclerview.smoothScrollToPosition(0)

        updateTitle()

        val ready = this.items.filter { it.state == SurveyResultInfo.State.FINISHED }.size
        activity.findViewById<TextView>(R.id.badge)?.let { badgeTextView ->
            if(ready == 0){
                badgeTextView.text = ""
                badgeTextView.visibility = View.GONE
            } else {
                badgeTextView.text = "$ready"
                badgeTextView.visibility = View.VISIBLE
            }

        }

    }

    private val exportObserver = ConsumableObserver { t: String?, consumed: Boolean ->
        if (consumed) return@ConsumableObserver

        t?.let {
            toast("exported to $it")
        } ?: toast("failed to export")

    }

    private val deleteObserver = ConsumableObserver<Boolean> { t: Boolean?, consumed: Boolean ->
        if (consumed || t == null) return@ConsumableObserver


        if (!t) {
            toast("Error deleting all surveys")
        } else {
            toast("Successfully deleted selected surveys")
        }

        viewModel.loadSurveyResults()
    }


    private val uploadProgressObserver = ConsumableObserver<Pair<Int, String>> { t: Pair<Int, String>?, consumed: Boolean ->

        if (consumed || t == null) {
            return@ConsumableObserver
        }


        binding.progress.isIndeterminate = false
        binding.progress.progress = t.first
        binding.tvProgress.visibility = View.VISIBLE
        binding.tvProgress.text = t.second
    }

    private val uploadResultObserver = FreshObserver<SurveyResultListViewModel.UploadResult> {


        uploadErrorResult.update(it.success)
        uploadWarningResult.update(it.success)
        uploadIncompleteResult.update(it.success)
        uploadReviewResult.update(it.success)

        binding.tvProgress.visibility = View.GONE
        binding.tabLayout.getTabAt(1)?.select()
        binding.progress.isIndeterminate = true

        viewModel.loadSurveyResults()
    }

    lateinit var binding: FragmentGenericListBinding
    private val viewModel by lazyViewModel<SurveyResultListViewModel>()
    private val scannerViewModel by lazyViewModel<QrScannerViewModel>()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity.lockMenus()
        activity.allowOverview(false)


        scannerViewModel.mCurrentQrValue.let {
            if (it.isEmpty()) {
                return@let
            }
            viewModel.filter = it
        }

        scannerViewModel.mCurrentQrValue = ""

        viewModel.loadSurveyResults.observe(this, resultObserver)
        viewModel.deleteResult.observe(this, deleteObserver)
        viewModel.uploadProgress.observe(this, uploadProgressObserver)
        viewModel.uploadResult.observe(this, uploadResultObserver)
        viewModel.exportingDoneResult.observe(this, exportObserver)


        if (!viewModel.busy()) {
            view.postDelayed({
                viewModel.loadSurveyResults()
            }, 500)

        }

        updateTitle()

        arguments?.let {
            val fromSurvey = it.getString("fromSurvey", "")
            it.remove("fromSurvey")

            if (fromSurvey.isNotEmpty()) {
                binding.tvHeader.visibility = View.VISIBLE
                binding.tvHeader.text = ""
                binding.btAction.visibility = View.VISIBLE
                binding.btAction.text = "Next Survey"
                binding.btAction.setOnClickListener {
                    thread {
                        SurveyManager.of(app()).createSurveyInstance(fromSurvey)
                        app().navigationManager.startSurvey(true)
                    }
                }
                //viewModel.showNextSnack(fromSurvey)
            }

            if (it.getBoolean("finish", false)) {
                binding.tabLayout.getTabAt(1)?.select()
            }
        }

        app().getCurrentContributionOrg()?.let {
            binding.tvHeader.text = "contributing to ${it.name}"
            binding.tvHeader.visibility = View.VISIBLE
        }

        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                updateAdapter()
            }
        })
    }

    fun updateAdapter(review: Review? = null) {
        adapter.items = items.sortedByDescending { it.instance.lastModified() }.filter {
            // sent tab
            if (binding.tabLayout.selectedTabPosition == 1) { // uploaded tab
                return@filter it.state == SurveyResultInfo.State.UPLOADED
            }

            // active tab
            if (it.state == SurveyResultInfo.State.UPLOADED) {
                return@filter false
            }

            if (review == null) {
                return@filter true
            }

            return@filter when (review) {
                Review.INCOMPLETE -> it.state != SurveyResultInfo.State.UPLOADED && !it.done
                Review.WARNING -> it.state == SurveyResultInfo.State.SCRIPT_WARNING
                Review.ERROR -> it.state == SurveyResultInfo.State.SCRIPT_ERROR
            }


        }

        if(review != null){
            binding.tabLayout.getTabAt(0)?.select()
        }


        adapter.notifyDataSetChanged()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentGenericListBinding.inflate(inflater, container, false)
        binding.recyclerview.adapter = adapter
        binding.recyclerview.layoutManager = LinearLayoutManager(activity)
        binding.recyclerview.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        binding.progress.visibility = View.VISIBLE

        adapter.items = listOf()
        adapter.notifyDataSetChanged()

        return binding.root
    }


    private val selectedMenuItems = arrayOf(
            BaseActivity.MenuItemInfo(Menu.NONE, R.id.menu_item_delete, 0, "Delete", R.drawable.ic_delete_black_24dp, true),
            BaseActivity.MenuItemInfo(Menu.NONE, R.id.menu_upload, 0, "Upload", R.drawable.ic_cloud_upload_black_24dp, true),
            BaseActivity.MenuItemInfo(Menu.NONE, R.id.menu_select_all, 0, "Select All", R.drawable.ic_select_all_black_24dp, false),
            BaseActivity.MenuItemInfo(Menu.NONE, R.id.menu_export, 0, "Export CSV to SD-Card", R.drawable.ic_sd_storage_black_24dp, false)
    )

    private val defaultMenuItems = arrayOf(
            BaseActivity.MenuItemInfo(Menu.NONE, R.id.menu_select_all, 0, "Select All", R.drawable.ic_select_all_black_24dp, false),
            BaseActivity.MenuItemInfo(Menu.NONE, R.id.menu_export, 0, "Export All Surveys as CSV to SD-Card", R.drawable.ic_sd_storage_black_24dp, false),
            BaseActivity.MenuItemInfo(Menu.NONE, R.id.menu_upload, 0, "Upload Finished", R.drawable.ic_cloud_upload_black_24dp, true)

    )


    override fun menuItems() = if (items.any { it.selected }) {
        activity.deviceVisible.set(false)
        selectedMenuItems
    } else {
        activity.deviceVisible.set(true)
        defaultMenuItems
    }

    override fun onMenuItemPressed(menuItemId: Int) = when (menuItemId) {
        R.id.menu_export -> {
            Timber.d("exporting to csv")

            val d = dialog {
                title = "Should I export?"
                message = "Decide now!"
                positive = action("Export") {
                    viewModel.exportAll()
                }
            }

            viewModel.exportAll()
            true
        }
        R.id.menu_item_delete -> {
            val icon = ContextCompat.getDrawable(activity, R.drawable.ic_delete_black_24dp)?.let {
                DrawableCompat.setTint(it, ContextCompat.getColor(activity, R.color.colorAccent))
                it
            }

            AlertDialog.Builder(activity, activity.alertDialogTheme)
                    .setTitle("Delete Surveys")
                    .setMessage("Deleting these %d Survey will remove them and place them into the trash folder"
                            .format(items.count { it.selected }))
                    .setPositiveButton("Delete") { dialogInterface, _ ->
                        dialogInterface.dismiss()
                        if (viewModel.deleteSurveys(items.filter { it.selected })) {
                            binding.recyclerview.visibility = View.GONE
                            binding.progress.visibility = View.VISIBLE
                        } else {
                            toast("already deleting")
                        }
                    }
                    .setCancelable(true)
                    .setNegativeButton("Cancel") { dialogInterface, _ ->
                        dialogInterface.dismiss()
                    }
                    .setIcon(icon)
                    .create()
                    .show()
            true
        }
        R.id.menu_select_all -> {
            val selectSent = binding.tabLayout.selectedTabPosition == 1

            items.filter {
                if (selectSent) {
                    it.state == SurveyResultInfo.State.UPLOADED
                } else it.state != SurveyResultInfo.State.UPLOADED
            }.forEach {
                it.selected = true
            }
            adapter.notifyDataSetChanged()
            activity.invalidateOptionsMenu()
            updateTitle()
            true
        }
        R.id.menu_upload -> {
            startCheck()
            true
        }
        else -> {
            super.onMenuItemPressed(menuItemId)
        }
    }


    private fun cancelSelection() {
        items.forEach {
            it.selected = false
        }

        updateTitle()
        activity.invalidateOptionsMenu()
        adapter.notifyDataSetChanged()
    }

    private fun startCheck() {
        checkRequired()
    }

    private fun needReview() = items.filter {
        when {
            it.state == SurveyResultInfo.State.UPLOADED -> false
            !it.done && it.state != SurveyResultInfo.State.FINISHED -> true
            it.state == SurveyResultInfo.State.SCRIPT_WARNING -> true
            it.state == SurveyResultInfo.State.SCRIPT_ERROR -> true
            else -> false
        }

    }

    private fun checkRequired() {

        val submitAllReady: (finished: List<SurveyResultInfo>) -> Unit = { finished ->
            dialog {
                title = "${finished.size} surveys ready to send"
                message = "${needReview().size} surveys need review"
                positive = action("Submit") {
                    finished.forEach { it.selected = true }
                    checkUploaded()
                }
            }
        }

        val review: () -> Unit = f@{
            val dialog = dialog {
                title = "Review"
                message = "Some Surveys contain Warnings and/or Errors."
                positive = action("Done") {
                    updateAdapter()
                }
                negative = null
                layout = R.layout.dialog_review
            }

            val view = dialog.findViewById<ConstraintLayout>(R.id.dialog_review) ?: return@f
            val binding = DialogReviewBinding.bind(view)

            binding.btErrorReview.setOnClickListener {
                updateAdapter(review = Review.ERROR)
                dialog.dismiss()
            }

            binding.btWarningReview.setOnClickListener {
                updateAdapter(review = Review.WARNING)
                dialog.dismiss()
            }

            binding.btIncompleteReview.setOnClickListener {
                updateAdapter(review = Review.INCOMPLETE)
                dialog.dismiss()
            }


            val send: (button: View, state: Review) -> Unit = s@{ button, state ->
                if (viewModel.busy()) {
                    return@s
                }

                button.isEnabled = false
                binding.progressError.visibility = View.VISIBLE

                uploadErrorResult.observe {
                    binding.progressError.visibility = View.GONE
                    binding.ivErrorDone.visibility = View.VISIBLE
                }

                val filtered = items.filter {
                    when (state) {
                        Review.INCOMPLETE -> it.state != SurveyResultInfo.State.UPLOADED && !it.done
                        Review.WARNING -> it.state == SurveyResultInfo.State.SCRIPT_WARNING
                        Review.ERROR -> it.state == SurveyResultInfo.State.SCRIPT_ERROR
                    }
                }


                viewModel.upload(filtered)
            }

            binding.btErrorSend.setOnClickListener { button ->
                send(button, Review.ERROR)
            }


            binding.btWarningSend.setOnClickListener { button ->
                send(button, Review.WARNING)
            }

            binding.btIncompleteSend.setOnClickListener { button ->
                send(button, Review.INCOMPLETE)
            }

            binding.display = ReviewDisplay(items)
            binding.invalidateAll()
        }

        val selected = items.filter { it.selected }
        if (selected.isEmpty()) { // what happens if user right away presses upload
            val finished = items.filter { it.state == SurveyResultInfo.State.FINISHED }
            if (finished.isNotEmpty()) {
                uploadReviewResult.observe {
                    if (items.any { it.state != SurveyResultInfo.State.UPLOADED })
                        review()
                }
                submitAllReady(finished)
            } else {
                review()
            }
            return
        }


        if (items.any { !it.done }) {
            dialog {
                title = "Some surveys are not complete"
                message = "Some required answers are not set, would you still like to send all surveys?"
                positive = action("Send") {
                    checkUploaded()
                }
                negative = action("Cancel") {
                    cancelSelection()
                }
                neutral = action("Resolve") {
                    items.filter { it.selected }.find { !it.done }?.let { surveyResultInfo ->
                        viewModel.toSurvey(surveyResultInfo)
                    }
                }
            }
        } else checkUploaded()
    }


    private fun checkUploaded() {
        if (items.filter { it.selected }.any { it.state == SurveyResultInfo.State.UPLOADED }) {
            AlertDialog.Builder(activity, activity.alertDialogTheme)
                    .setTitle("Some surveys already sent")
                    .setMessage("Some of the selected surveys have already been sent, would you like to resend them?")
                    .setPositiveButton("Resend") { dialog, _ ->
                        uploadSelected()
                        dialog?.dismiss()
                    }.setNegativeButton("Cancel") { dialog, _ ->
                        cancelSelection()
                        dialog.dismiss()
                    }.create().show()
        } else uploadSelected()
    }

    private fun uploadSelected() {
        binding.recyclerview.visibility = View.GONE
        binding.progress.visibility = View.VISIBLE
        binding.tabLayout.visibility = View.GONE

        viewModel.upload(items.filter { it.selected })
        items.forEach {
            it.selected = false
        }

        updateTitle()
        activity.invalidateOptionsMenu()
    }


    override fun onBackPressed(): Boolean {
        if (items.any { it.selected }) {
            items.forEachIndexed { index, surveyResultInfo ->
                if (surveyResultInfo.selected) {
                    surveyResultInfo.selected = false
                }
            }

            updateTitle()
            activity.invalidateOptionsMenu()
            adapter.notifyDataSetChanged()
            return true
        }

        updateTitle()
        return false
    }


    private fun updateTitle() {

        when {
            items.any { it.selected } -> {
                activity.supportActionBar?.title = "%d".format(items.count { it.selected })
                activity.supportActionBar?.subtitle = ""
            }
            items.isNotEmpty() -> {
                activity.supportActionBar?.title = "Surveys"
                activity.supportActionBar?.subtitle = "%d items".format(items.size)
            }
            else -> {
                activity.supportActionBar?.title = "Surveys"
                activity.supportActionBar?.subtitle = ""
            }
        }
    }


    override fun onSearchClicked(query: String): Boolean {
        Timber.d("searching for query $query")
        viewModel.filter = query
        if (!viewModel.busy()) viewModel.loadSurveyResults()
        return true
    }

    override fun onSearchClosed(): Boolean {
        viewModel.filter = ""

        if (!viewModel.busy()) {
            viewModel.loadSurveyResults()
        }

        return true
    }

    override fun searchText(): String {
        return viewModel.filter
    }

    override fun onMenuRendered() {
        if (viewModel.filter.isNotEmpty()) {
            updateSearchView(viewModel.filter)
        }
    }


}