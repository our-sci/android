package org.oursci.android.common.farmos

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Manuel Di Cerbo on 15.03.19.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */

@Entity(tableName = "farmos_area")
data class FarmOsArea(
        @PrimaryKey(autoGenerate = true)
        val _id: Int = 0,
        val tid: Long,
        val type: String,
        val name: String,
        val farmOsServer: Long
) {
    override fun toString(): String {
        return "$name ($tid)"
    }
}