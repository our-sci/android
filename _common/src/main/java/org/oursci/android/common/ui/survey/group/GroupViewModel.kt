package org.oursci.android.common.ui.survey.group

import org.oursci.android.common.BaseApplication
import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.ui.survey.ControlViewModel

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 03.05.18.
 */
class GroupViewModel(app: BaseApplication) : ControlViewModel<BaseControl<out Any>>(app) {
}