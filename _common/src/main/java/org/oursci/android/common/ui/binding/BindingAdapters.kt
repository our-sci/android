package org.oursci.android.common.ui.binding

import androidx.databinding.BindingAdapter
import android.graphics.drawable.Drawable
import android.net.Uri
import com.google.android.material.textfield.TextInputLayout
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import org.oursci.android.common.R
import org.oursci.android.common.ui.profile.ProfileImageView
import org.oursci.android.common.ui.profile.ProfileImageViewColored
import org.oursci.android.common.ui.views.MultipleChoiceImageView
import timber.log.Timber


/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 25.07.17.
 */

@BindingAdapter("android:src")
fun setImageUri(view: ImageView, imageUri: String?) {
    if (imageUri == null) {
        view.setImageURI(null)
    } else {
        view.setImageURI(Uri.parse(imageUri))
    }
}

@BindingAdapter("android:src")
fun setImageUri(view: ImageView, imageUri: Uri?) {
    Timber.d("binding image to imageview")


    if (imageUri == null || imageUri == Uri.EMPTY) {
        Timber.d("id for drawable of binding is null")
        view.setImageDrawable(null)
        return
    }

    if (listOf("http", "https").contains(imageUri.scheme)) {
        Glide.with(view.context).load(imageUri).apply(RequestOptions
                .circleCropTransform()

        ).into(view)
    } else {
        view.setImageURI(imageUri)
    }
}

@BindingAdapter("android:src")
fun setImageUri(view: MultipleChoiceImageView, imageUri: Uri?) {
    Timber.d("binding image to imageview")


    if (imageUri == null || imageUri == Uri.EMPTY) {
        Timber.d("id for drawable of binding is null")
        view.setImageDrawable(null)
        return
    }

    if (listOf("http", "https").contains(imageUri.scheme)) {
        Glide.with(view.context)
                .load(imageUri)
                .apply(RequestOptions
                        .centerCropTransform()
                        .onlyRetrieveFromCache(true)
                ).into(view)
    } else {
        view.setImageURI(imageUri)
    }
}

@BindingAdapter(value = *arrayOf("bind:profile", "bind:empty"), requireAll = true)
fun setImageUriWithBackup(view: ImageView, imageUri: Uri?, resource: Int) {
    Timber.d("binding profile and empty")

    if (imageUri == null || imageUri == Uri.EMPTY) {
        Glide.with(view.context).load(resource).apply(RequestOptions.circleCropTransform()).into(view)
        return
    }

    if (listOf("http", "https").contains(imageUri.scheme)) {
        Glide.with(view.context).load(imageUri).apply(RequestOptions.circleCropTransform()).into(view)
    } else {
        view.setImageURI(imageUri)
    }
}

@BindingAdapter("android:src")
fun profile(view: ProfileImageView, imageUri: Uri?) {
    Timber.d("binding profile")

    if (imageUri == null || imageUri == Uri.EMPTY) {
        Glide.with(view.context).load(R.drawable.owl).apply(RequestOptions.circleCropTransform()).into(view)
        return
    }

    if (listOf("http", "https").contains(imageUri.scheme)) {
        Glide.with(view.context).load(imageUri).apply(
                RequestOptions.circleCropTransform()).into(view)
    } else {
        view.setImageURI(imageUri)
    }
}

@BindingAdapter("android:src")
fun profile(view: ProfileImageViewColored, imageUri: Uri?) {
    Timber.d("binding profile")

    if (imageUri == null || imageUri == Uri.EMPTY) {
        Glide.with(view.context).load(R.drawable.owl_red).apply(RequestOptions.circleCropTransform()).into(view)
        return
    }

    if (listOf("http", "https").contains(imageUri.scheme)) {
        Glide.with(view.context).load(imageUri).apply(
                RequestOptions.circleCropTransform()).into(view)
    } else {
        view.setImageURI(imageUri)
    }
}

@BindingAdapter("android:src")
fun setImageDrawable(view: ImageView, drawable: Drawable?) {
    view.setImageDrawable(drawable)
}

@BindingAdapter("android:src")
fun setImageResource(imageView: ImageView, resource: Int) {
    if (resource == -1) {
        imageView.setImageDrawable(null)
    } else {
        imageView.setImageResource(resource)
    }

}

@BindingAdapter("error")
fun setErrorMessage(view: TextInputLayout, errorMessage: String) {
    view.error = errorMessage
}

@BindingAdapter("android:layout_height")
fun setLayoutHeight(view: View, height: Float) {
    val layoutParams = view.layoutParams
    layoutParams.height = height.toInt()
    view.layoutParams = layoutParams
}

@BindingAdapter("android:layout_width")
fun setLayouWidth(view: View, width: Float) {
    val layoutParams = view.layoutParams
    layoutParams.width = width.toInt()
    view.layoutParams = layoutParams
}