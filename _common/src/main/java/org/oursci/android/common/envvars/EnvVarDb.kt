package org.oursci.android.common.envvars

import androidx.room.Room
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.organization.OrganizationDatabase

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 10.05.18.
 */
class EnvVarDb(val app: BaseApplication) {
    val db by lazy {
        Room.databaseBuilder(app, EnvVarDatabase::class.java,
                "environment_variables")
                .allowMainThreadQueries()
                .build()
    }


    companion object {

        private val lock = Object()

        private var sInstance: EnvVarDb? = null

        fun of(app: BaseApplication): EnvVarDb {
            synchronized(lock) {
                if (sInstance == null) {
                    sInstance = EnvVarDb(app)
                }

                return sInstance!!
            }
        }
    }
}