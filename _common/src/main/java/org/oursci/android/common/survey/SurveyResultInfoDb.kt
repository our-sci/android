package org.oursci.android.common.survey

import androidx.room.Room
import org.oursci.android.common.BaseApplication

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 11.12.17.
 */
class SurveyResultInfoDb(app: BaseApplication) {

    val db by lazy {
        Room.databaseBuilder(app, SurveyInfoResultsDatabase::class.java,
                "survey_info_results")
                .fallbackToDestructiveMigration()
                .build()
    }

    companion object {

        private val lock = Object()

        private var sInstance: SurveyResultInfoDb? = null

        fun of(app: BaseApplication): SurveyResultInfoDb {
            synchronized(lock) {
                if (sInstance == null) {
                    sInstance = SurveyResultInfoDb(app)
                }

                return sInstance!!
            }
        }
    }

}