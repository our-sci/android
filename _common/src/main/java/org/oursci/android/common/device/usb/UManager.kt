package org.oursci.android.common.device.usb

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.hardware.usb.*
import com.felhr.usbserial.UsbSerialDevice
import com.felhr.usbserial.UsbSerialInterface
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.R
import org.oursci.android.common.device.BaseDeviceManager
import org.oursci.android.common.pref
import org.oursci.android.common.storePref
import timber.log.Timber
import java.nio.ByteBuffer
import java.nio.charset.Charset
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 16.11.17.
 */
class UManager(app: BaseApplication) : BaseDeviceManager<UsbDevice>(app) {


    var serialDevice: UsbSerialDevice? = null

    var scanCallback: ((UsbDevice) -> Unit)? = null
    var readCallback: ((String) -> Unit)? = null
    var scanDoneCallback: (() -> Unit)? = null

    companion object {
        private val lock = Object()
        private var sInstance: UManager? = null

        fun of(app: BaseApplication): UManager {
            synchronized(lock) {
                if (sInstance == null) {
                    sInstance = UManager(app)
                }
                return sInstance!!
            }
        }
    }

    override var currentDevice: UsbDevice? = null

    override fun registerCallback(
            onScan: (UsbDevice) -> Unit,
            onStatusChange: (Any) -> Unit,
            onDataRead: (String) -> Unit,
            onScanDone: (() -> Unit)?) {

        scanCallback = onScan
        readCallback = onDataRead
        scanDoneCallback = onScanDone
    }

    override fun currentDevice(): UsbDevice? {
        val vidPid = app.pref(R.string.key_current_pid_vid)
        Timber.d("usb device: $vidPid")
        if (vidPid.isNotEmpty()) {
            val mgr = (app.getSystemService(Context.USB_SERVICE) as? UsbManager) ?: return null
            mgr.deviceList.forEach { (t, dev) ->
                if (formatDevice(dev) == vidPid) {
                    Timber.d("found device: %s", formatDevice(dev))
                    return dev
                }
            }
        }
        return null
    }

    override fun connect(device: UsbDevice) {
        try {
            if (serialConnect(device)) {
                app.storePref(R.string.key_current_pid_vid,
                        formatDevice(device))
                Timber.d("storing device info")
                connected.set(true)
            } else {
                connected.set(false)
            }
        } catch (e: Exception) {
            Timber.e(e)
            connected.set(false)
        }
    }

    private fun serialConnect(device: UsbDevice): Boolean {
        val mgr = (app.getSystemService(Context.USB_SERVICE) as? UsbManager) ?: return false

        val fieldEpIn = UsbSerialDevice::class.java.getDeclaredField("inEndpoint")
        val fieldConnection = UsbSerialDevice::class.java.getDeclaredField("connection")
        fieldEpIn.isAccessible = true
        fieldConnection.isAccessible = true


        serialDevice = UsbSerialDevice.createUsbSerialDevice(device, mgr.openDevice(device))?.apply {
            try {


                syncOpen()
                setBaudRate(9600)
                setDataBits(UsbSerialInterface.DATA_BITS_8)
                setParity(UsbSerialInterface.PARITY_NONE)
                setStopBits(1)
                setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF)
                currentDevice = device

                val executorService = Executors.newSingleThreadExecutor()
                val readExecutors = Executors.newFixedThreadPool(8)


                val epIn = fieldEpIn.get(this) as UsbEndpoint
                val conn = fieldConnection.get(this) as UsbDeviceConnection
                val charset = Charset.forName("utf-8")


                val xferlen = 4096
                val numXfers = 64

                val requestMap = (0 until numXfers).map {
                    val request = UsbRequest()
                    request.initialize(conn, epIn)
                    val buffer = ByteBuffer.allocate(xferlen)
                    request.queue(buffer, xferlen)
                    request to buffer
                }.toMap()

                thread {
                    val target = ByteArray(xferlen)


                    while (true) {
                        val r = conn.requestWait()
                        if (r == null) {
                            Timber.e("error waiting for request, abort")
                            return@thread
                        }


                        requestMap[r]?.let {
                            val pos = it.position()
                            Timber.d("received buffer, %d bytes", pos)
                            it.position(0)
                            it.get(target, 0, pos)

                            if (pos != 0) {
                                val data = target.sliceArray(0 until pos).toString(charset)

                                executorService.submit {
                                    feedInterceptor("<font color='#040'>")
                                    feedInterceptor(data.replace("\n", "<br>"))
                                    feedInterceptor("</font><br><br>")
                                    readCallback?.invoke(data)
                                }

                                target.fill(0, 0, target.size)

                                it.clear()
                            }

                            r.queue(it, xferlen)
                        } ?: Timber.e("unable to find request in map")
                    }
                }


//
//                for (i in 0..8) {
//                    val request = UsbRequest()
//                    request.initialize(conn, epIn)
//
//                    readExecutors.submit {
//                        val buffer = ByteBuffer.allocate(16384)
//                        val target = ByteArray(16384)
//
//
//                        while (true) {
//                            if (!request.queue(buffer, 16384)) {
//                                Timber.e("unable to queue transfer, aborting")
//                                return@submit
//                            }
//                            val res = conn.requestWait()
//                            if (res == null) {
//                                Timber.e("error receiving USB xfer, aborting")
//                                return@submit
//                            }
//
//                            buffer.get(target)
//
//                            val data = target.toString(charset)
//                            Timber.d("thread %d dispatching usb data", i)
//
//                        }
//                    }
//                }

//
//                        while (true) {
//                            val len = syncRead(buffer, 0)
//                            if (len == 0) {
//                                continue
//                            }
//
//                            if (len < 0) {
//                                Timber.e("len < 0, returning")
//                                connected.set(false)
//                                try {
//                                    serialDevice?.syncClose()
//                                } catch (e: Exception) {
//                                    Timber.e(e)
//                                }
//                                return@submit
//                            }
//
//                            val data = buffer.copyOfRange(0, len)
//                                    .toString(Charset.forName("utf-8"))
//
//                            for (i in 0 until buffer.size) {
//                                buffer[i] = 0
//                            }
//
//
//                        }
//                read { data: ByteArray? ->
//                    if (data == null) {
//                        return@read
//                    }
//
//                    val str = data.toString(charset)
//
//                    executorService.submit {
//                        feedInterceptor("<font color='#040'>")
//                        feedInterceptor(str.replace("\n", "<br>"))
//                        feedInterceptor("</font><br><br>")
//                        readCallback?.invoke(str)
//                    }
//                }
            } catch (e: Exception) {
                Timber.e(e)
                return false
            }
            Timber.d("serial device connected")
        } ?: return false
        return true
    }

    private fun formatDevice(device: UsbDevice) = "%04X:%04X".format(device.vendorId, device.productId)

    override fun cancelConnect(deviceToConnect: UsbDevice?) {
        clearDevice()
    }

    override val connected = AtomicBoolean(false)


    override fun init() {

    }


    override fun scan() {
        (app.getSystemService(Context.USB_SERVICE) as? UsbManager)?.let { mgr ->
            mgr.deviceList.forEach { (_: String?, device: UsbDevice?) ->
                scanCallback?.invoke(device)
            }
        }
    }

    override fun reconnect() {
        serialDevice?.syncClose()

        currentDevice()?.let {
            connect(it)
        } ?: Timber.e("current device is null")
    }

    override fun write(data: String) {
        Timber.d("writing data to usb %s", data)
        Timber.d("serialDevice is %s", serialDevice)
        serialDevice?.let {
            feedInterceptor("<font color='blue'>")
            feedInterceptor(data.replace("\n", "<br>"))
            feedInterceptor("</font><br><br>")
            thread {
                it.syncWrite(data.toByteArray(), 0)
            }
        }
    }


    override fun isConnected() = connected.get()

    override fun hasDevice() = currentDevice() != null

    fun hasPermission(device: UsbDevice): Boolean =
            (app.getSystemService(Context.USB_SERVICE) as? UsbManager)?.hasPermission(device) ?: false

    fun requestPermission(device: UsbDevice, callback: (device: UsbDevice?) -> Unit) =
            (app.getSystemService(Context.USB_SERVICE) as? UsbManager)?.let { mgr ->
                app.registerPermissionCallback(callback)
                val permissionIntent =
                        PendingIntent.getBroadcast(app, 0, Intent(app.ACTION_USB_PERMISSION), 0)
                mgr.requestPermission(device, permissionIntent)
                true
                //val serial = UsbSerialDevice.createUsbSerialDevice(device, usbConnection);
            } ?: false


    data class Vendor(val id: Int, val name: String)
    data class Product(val id: Int, val name: String, val vendor: Vendor)

    fun info(vid: Int, pid: Int): String {
        val vendor = productList.map {
            it.vendor
        }.firstOrNull {
            vid == it.id
        }

        val product = productList.firstOrNull {
            it.id == pid && it.vendor.id == vid
        }

        return product?.let {
            "%s, %s".format(it.vendor.name, it.name)
        } ?: vendor?.let {
            "%s, <unknown product>".format(it.name)
        } ?: "<unknown vendor>, <unknown product>"

    }

    private val productList by lazy {
        var currentVendor: Vendor? = null
        val list: MutableList<Product> = mutableListOf()
        app.assets.open("usb-ids.txt").bufferedReader().useLines { lines: Sequence<String> ->
            lines.forEach {
                if (it.startsWith("\t")) {
                    val vendor = currentVendor ?: return@forEach
                    try {
                        Product(it.substring(1, 5).toInt(16),
                                it.substring(7, it.length), vendor)
                    } catch (e: Exception) {
                        null
                    }?.let {
                        list.add(it)
                    }
                } else {
                    currentVendor = try {
                        Vendor(it.substring(0, 4).toInt(16), it.substring(6, it.length))
                    } catch (e: Exception) {
                        null
                    }
                }
            }
        }

        list
    }

    fun clearDevice() {
        app.storePref(R.string.key_current_pid_vid,
                "")
    }
}
