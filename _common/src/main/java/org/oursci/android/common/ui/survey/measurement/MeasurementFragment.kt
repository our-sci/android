package org.oursci.android.common.ui.survey.measurement

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.os.Build
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.webkit.WebView
import androidx.lifecycle.ViewModelProviders
import org.oursci.android.common.*
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.ControlMeasurementBinding
import org.oursci.android.common.databinding.LayoutControlFragmentBinding
import org.oursci.android.common.device.bt.BtManager
import org.oursci.android.common.device.usb.UManager
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.controls.measurement.MeasurementControl
import org.oursci.android.common.ui.controls.measurement.MeasurementDisplay
import timber.log.Timber


/**
 * Created by Manuel Di Cerbo on 29.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class MeasurementFragment : BaseFragment() {
    override val searchable = true

    private lateinit var controlBinding: ControlMeasurementBinding
    lateinit var binding: LayoutControlFragmentBinding

    private var idx: Int = -1
    lateinit var measurementId: String
    private var autoRun = false

    private var filePath: String = ""
    private var cachedState = MeasurementDisplay.State.IDLE

    private val measurementViewModel: MeasurementViewModel by lazy {
        ViewModelProviders.of(this, app().viewModelFactory).get(MeasurementViewModel::class.java)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    private val cookieObserver = ConsumableObserver<Pair<String, String>> { value, consumed ->
        if (consumed || value == null) {
            return@ConsumableObserver
        }

        val cookieManager = CookieManager.getInstance()
        cookieManager.setCookie(value.first, value.second)
    }

    private val sensorProgressObserver = ConsumableObserver<Float> { value, consumed ->

        Timber.d("updating progessbar with value: %d", value?.toInt() ?: 0)
        controlBinding.progressMeasurement.progress = value?.toInt() ?: 0
        controlBinding.progressMeasurement.invalidate()
    }


    private val sensorResultObserver = ConsumableObserver<Void> { value, consumed ->
        if (consumed) {
            return@ConsumableObserver
        }

        Timber.d("sensorResultObserver")
        controlBinding.webviewMeasurementResult.loadUrl("about:blank")

        controlBinding.root.postDelayed({
            app().navigationManager.navigateToMeasurement(measurementId, true, idx = idx, autoRun = autoRun)
        }, 150)
    }


    private val onDataAvailableObserver = ConsumableObserver<Void> { _, consumed ->
        if (consumed) {
            return@ConsumableObserver
        }

        controlBinding.webviewMeasurementResult.loadUrl("javascript:serial.onDataAvailable()")
    }


    private val measurementControlObserver = ConsumableObserver<MeasurementControl> { control, consumed ->
        Timber.d("measurementControlObserver")
        if (control == null) {
            Timber.e("control is null")
            return@ConsumableObserver
        }

        if (consumed) {
            Timber.e("control already consumed")
            return@ConsumableObserver
        }

        Timber.d("setting control")
        controlBinding.control = MeasurementDisplay(control,
                measurementViewModel.currentMeasurementScript?.actionName ?: "Start Measurement",
                measurementViewModel.currentMeasurementScript?.manifestCreated)


        if (measurementViewModel.requireDevice() && !measurementViewModel.isConnected()) {
            controlBinding.btMeasurement.text = "Connect to device"
        } else {
            controlBinding.btMeasurement.text = measurementViewModel.currentMeasurementScript?.actionName
                    ?: "Start Measurement"
        }


        if (autoRun) {
            binding.tvProgress.text = "Running ${control.label}"
        }


        controlBinding.invalidateAll()


        binding.root.postDelayed({

            if (autoRun) {
                controlBinding.btMeasurement.callOnClick()
            }
            controlBinding.root.visibility = View.VISIBLE
        }, 200)


    }


    private val webAnswerObserver = ConsumableObserver<String> { str: String?, consumed: Boolean ->
        if (consumed || str == null) {
            return@ConsumableObserver
        }

        measurementViewModel.webInputData.append(str)
        controlBinding.webviewMeasurementResult.loadUrl("javascript:serial.onWebInputAvailable()")
        controlBinding.invalidateAll()
    }

    private val deviceConnectedResultObserver = ConsumableObserver<String> { status, consumed ->
        if (consumed) {
            return@ConsumableObserver
        }

        when (status) {
            "CONNECTED" -> {
                Timber.d("measurement fragment.. CONNECTED")
                controlBinding.control?.state = MeasurementDisplay.State.IDLE
                controlBinding.btMeasurement.text = measurementViewModel.currentMeasurementScript?.actionName
                        ?: "Start Measurement"
            }
            "CONNECTING" -> {
                Timber.d("measurement fragment.. CONNECTING")
                controlBinding.tvConnecting.text = "Connecting to '${measurementViewModel.deviceName()}'"
                controlBinding.control?.state = MeasurementDisplay.State.CONNECTING
            }
            "NONE" -> {
                Timber.d("measurement fragment.. NONE")
                controlBinding.tvConnectingFailed.text = "Could not connect to '${measurementViewModel.deviceName()}'\n\nCheck if device is on.\nReset device and try again."
                controlBinding.control?.state = MeasurementDisplay.State.CONNECTING_FAILED
            }
        }

        controlBinding.invalidateAll()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        idx = arguments?.getInt("idx", -1) ?: -1
        filePath = arguments?.getString("file", "") ?: ""
        measurementId = arguments?.getString("id", "") ?: ""
        measurementViewModel.clearResult()
        autoRun = arguments?.getBoolean("autoRun", false) ?: false

        binding = LayoutControlFragmentBinding.inflate(inflater, container, false)


        binding.viewStubControl.viewStub?.layoutResource = R.layout.control_measurement
        binding.viewStubControl.viewStub?.setOnInflateListener { _, view ->
            postInflate(view)
        }


        binding.progress.visibility = if (autoRun) View.VISIBLE else View.GONE

        activity.unlockMenus()
        binding.root.postDelayed({
            binding.viewStubControl.viewStub?.inflate()
        }, 350)

        if (measurementId.isNotEmpty()) {
            activity.allowOverview(false)
            binding.btNext.visibility = View.GONE
        } else {
            activity.allowOverview(true)
        }

        binding.btNext.setOnClickListener {
            if (measurementViewModel.hasMoreQuestions(idx)) {
                app().navigationManager.moveToNextSurveyQuestion(idx + 1)
            } else {
                app().navigationManager.finalOverview()
            }
        }

        if (autoRun) {
            binding.btNext.visibility = View.GONE
        }

        val cookieManager = CookieManager.getInstance()
        cookieManager.setAcceptCookie(true)
        return binding.root
    }


    private fun postInflate(view: View) {
        controlBinding = ControlMeasurementBinding.bind(view)
        controlBinding.root.visibility = View.GONE

        if (0 != app().applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE) {
            WebView.setWebContentsDebuggingEnabled(true)
        }

        controlBinding.webviewMeasurementResult.clearCache(true)
        val cookieManager = CookieManager.getInstance()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.setAcceptThirdPartyCookies(controlBinding.webviewMeasurementResult, true)
        }

        controlBinding.webviewMeasurementResult.settings.apply {
            cacheMode = WebSettings.LOAD_NO_CACHE
            setAppCacheEnabled(false)
            javaScriptEnabled = true
            allowFileAccessFromFileURLs = true
            allowUniversalAccessFromFileURLs = true
        }


        controlBinding.webviewMeasurementResult.apply {
            addJavascriptInterface(measurementViewModel.applicationInterface, "android")
            addJavascriptInterface(measurementViewModel.sensorInterface, "sensor")
            addJavascriptInterface(measurementViewModel.processorInterface, "processor")
        }

        controlBinding.progressMeasurement.progress = 0

        controlBinding.webviewMeasurementResult.loadUrl("about:blank")
        controlBinding.webviewMeasurementResult.webViewClient = object : WebViewClient() {

        }

        controlBinding.textView.movementMethod = ScrollingMovementMethod()

        controlBinding.webviewMeasurementResult.webChromeClient = object : WebChromeClient() {
            override fun onPermissionRequest(request: PermissionRequest?) {
                request?.let {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        it.grant(it.resources)
                    }
                }
            }

            override fun onConsoleMessage(consoleMessage: ConsoleMessage?): Boolean {
                if (consoleMessage == null) {
                    return super.onConsoleMessage(consoleMessage)
                }

                val color = when (consoleMessage.messageLevel()) {
                    ConsoleMessage.MessageLevel.DEBUG -> "orange"
                    ConsoleMessage.MessageLevel.ERROR -> "red"
                    ConsoleMessage.MessageLevel.LOG -> "blue"
                    ConsoleMessage.MessageLevel.TIP -> "green"
                    ConsoleMessage.MessageLevel.WARNING -> "orange"
                    else -> "black"
                }

                val text = "<font color='%s'>%s (%d)</font><br><br>".format(color, consoleMessage.message(), consoleMessage.lineNumber())
                measurementViewModel.appendLog(text)
                controlBinding.textView.append(html(text))
                return super.onConsoleMessage(consoleMessage)
            }
        }

        if (measurementViewModel.requireDevice() && !measurementViewModel.isConnected()) {

        }

        controlBinding.btMeasurement.setOnClickListener {

            binding.progress.visibility = View.GONE
            binding.tvProgress.visibility = View.GONE

            if (measurementViewModel.requireDevice() && !measurementViewModel.isConnected()) {
                //Toast.makeText(activity, "Not Connected", Toast.LENGTH_SHORT).show()
                if (UManager.of(app()).hasDevice()) {
                    measurementViewModel.connectToLastDevice()
                } else if (BtManager.of(app()).hasDevice()) {
                    val adapter = BluetoothAdapter.getDefaultAdapter()
                    if (adapter.state == BluetoothAdapter.STATE_OFF) {
                        val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                        activity.startActivityForResult(enableBtIntent, BaseActivity.RC_BLUETOOTH_ENABLE_FROM_MEASUREMENT)
                        return@setOnClickListener
                    }
                    measurementViewModel.connectToLastDevice()
                } else {
                    app().navigationManager.deviceList()
                }

                return@setOnClickListener
            }

            logTakeMeasurementEvent(measurementId)

            measurementViewModel.clearResult()
            measurementViewModel.clearBuffer()

            controlBinding.progressMeasurement.progress = 0

            controlBinding.control?.state = MeasurementDisplay.State.MEASUREMENT
            controlBinding.invalidateAll()


            controlBinding.root.visibility = View.VISIBLE
            binding.tvProgress.visibility = View.GONE

            measurementViewModel.controlIdx = idx
            controlBinding.webviewMeasurementResult.loadUrl("file:///android_asset/sensor/sensor.html")
        }


        controlBinding.btCancel.setOnClickListener {
            controlBinding.control?.state = MeasurementDisplay.State.IDLE
            controlBinding.invalidateAll()
            controlBinding.webviewMeasurementResult.loadUrl("about:blank")
        }

        controlBinding.btConnectList.setOnClickListener {
            app().navigationManager.deviceList()
        }

        controlBinding.btConnectRetry.setOnClickListener {
            measurementViewModel.connectToLastDevice()
        }

        controlBinding.btCancelConnecting.setOnClickListener {
            measurementViewModel.cancelConnect()
        }


        measurementViewModel.loadControlLiveData.observe(this, measurementControlObserver)
        measurementViewModel.sensorProgressResult.observe(this, sensorProgressObserver)
        measurementViewModel.sensorResult.observe(this, sensorResultObserver)
        measurementViewModel.sensorDataAvailable.observe(this, onDataAvailableObserver)
        measurementViewModel.cookieResult.observe(this, cookieObserver)
        app().webAnswerData.observe(this, webAnswerObserver)
        measurementViewModel.deviceConnectedResult.observe(this, deviceConnectedResultObserver)

        if (measurementId.isNotEmpty()) {
            measurementViewModel.createMeasurementControl(measurementId)
        } else {
            Timber.d("loading control")
            measurementViewModel.loadControl(idx, MeasurementControl::class.java)
        }
    }

    override fun onBackPressed(): Boolean {
        try {
            if (controlBinding.control?.state == MeasurementDisplay.State.DEBUG) {
                controlBinding.control?.state = cachedState
                controlBinding.invalidateAll()
                return true
            }

        } catch (e: Exception) {
            Timber.e(e)
        }

        return super.onBackPressed()
    }

    override fun onMenuItemPressed(menuItemId: Int): Boolean = when (menuItemId) {
        R.id.console -> {
            if (controlBinding.control?.state != MeasurementDisplay.State.DEBUG) {
                cachedState = controlBinding.control?.state ?: MeasurementDisplay.State.IDLE
                controlBinding.control?.state = MeasurementDisplay.State.DEBUG
                controlBinding.invalidateAll()
            }
            true
        }
        else -> super.onMenuItemPressed(menuItemId)
    }

    override fun menuItems() = arrayOf(
            BaseActivity.MenuItemInfo(0, R.id.console, 0, "Debug Console", R.drawable.ic_terminal, true, false)
    )

}
