package org.oursci.android.common.ui.transition

import androidx.annotation.AnimRes
import org.oursci.android.common.R

/**
 * Created by Manuel Di Cerbo on 23.10.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
enum class Direction(
        @AnimRes val enterAnimation: Int,
        @AnimRes val exitAnimation: Int,
        val reverse: () -> Direction) {

    FROM_LEFT(R.anim.enter_from_left, R.anim.exit_to_right, { FROM_RIGHT }),
    FROM_RIGHT(R.anim.enter_from_right, R.anim.exit_to_left, { FROM_LEFT }),
    FROM_TOP(R.anim.enter_from_top, R.anim.exit_to_bottom, { FROM_BOTTOM }),
    FROM_BOTTOM(R.anim.enter_from_bottom, R.anim.exit_to_top, { FROM_TOP }),
}