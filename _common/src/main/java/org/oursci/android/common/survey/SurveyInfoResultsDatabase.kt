package org.oursci.android.common.survey

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import org.oursci.android.common.vo.survey.SurveyResultInfo

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 11.12.17.
 */

@Database(
        entities = [(SurveyResultInfo::class)],
        version = 6,
        exportSchema = false
)
@TypeConverters(SurveyResultInfo.Converters::class)

abstract class SurveyInfoResultsDatabase: RoomDatabase() {
    abstract fun surveyInfoDao(): SurveyResultInfoDao
}