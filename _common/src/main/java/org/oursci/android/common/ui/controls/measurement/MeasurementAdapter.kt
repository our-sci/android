package org.oursci.android.common.ui.controls.measurement

import android.os.Bundle
import org.javarosa.form.api.FormEntryPrompt
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.measurement.MeasurementLoader
import org.oursci.android.common.resolvePathFromMeasurementAnswer
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.ui.controls.IControlAdapter
import org.oursci.android.common.ui.controls.questionId
import org.oursci.android.common.ui.survey.measurement.MeasurementFragment
import org.oursci.android.common.ui.survey.measurement.MeasurementResultFragment
import org.oursci.android.common.vo.survey.Group
import org.oursci.android.common.vo.survey.measurement.MeasurementAnswer
import timber.log.Timber

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 14.11.17.
 */
object MeasurementAdapter : IControlAdapter<MeasurementControl> {


    override fun produce(prompt: FormEntryPrompt, group: Group?): MeasurementControl? {
        val answerValue = prompt.answerValue
        val answer = answerValue?.value?.toString()

        return prompt.helpText?.let(MeasurementInfo.Companion::from)?.let {
            MeasurementControl(
                    answer = MeasurementAnswer(answer = answer),
                    id = it.id,
                    defaultValue = "",
                    label = prompt.longText ?: "",
                    readOnly = prompt.isReadOnly,
                    required = prompt.isRequired,
                    requiredText = "this input is required",
                    hint = it.hint,
                    dataName = prompt.questionId(),
                    numElements = it.count,
                    group = group
            )
        }
    }


    override fun resolve(app: BaseApplication, control: BaseControl<*>): BaseFragment {
        if(control !is MeasurementControl){
            return MeasurementFragment()
        }

        return if (control.answer.answer != null) {
            control.answer.answer?.let {
                val fileName = resolvePathFromMeasurementAnswer(it)
                val args = Bundle()

                args.putString("file", MeasurementLoader.of(app).resolveMeasurementFile(control.id, fileName)?.absolutePath ?: "")
                MeasurementResultFragment().apply {
                    arguments = args
                }
            } ?: MeasurementFragment()
        } else MeasurementFragment()
    }


    data class MeasurementInfo(val hint: String, val id: String, val count: Int) {
        companion object {
            fun from(flat: String) = try {
                val items = flat.split(";")
                if (items.size == 3) {
                    MeasurementInfo(items[0], items[1], items[2].toInt())
                } else {
                    MeasurementInfo(items[0], items[1], -1)
                }
            } catch (e: Exception) {
                Timber.e(e)
                null
            }
        }
    }
}