package org.oursci.android.common.resources

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 21.03.19.
 */
data class ExternalResource(val id: String, val description: String, val items: List<ResourceItem>)