package org.oursci.android.common.vo.survey

import android.graphics.Color
import android.text.Spannable
import android.text.Spanned
import android.view.View
import org.oursci.android.common.R
import org.oursci.android.common.html

/**
 * Created by Manuel Di Cerbo on 18.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class SurveyOverviewItem(val index: String,
                         val title: String,
                         val subtitle: String,
                         val done: Boolean,
                         val anon: Boolean = false,
                         val relevant: Boolean = true,
                         val group: Group? = null,
                         val firstInGroup: Boolean = false,
                         val lastInGroup: Boolean = false,
                         val warning: Boolean = false,
                         val error: Boolean = false) {


    fun content() = html(subtitle)

    fun icon() = when {
        error -> R.drawable.ic_error_24dp
        warning -> R.drawable.ic_warning_black_24dp
        done -> R.drawable.ic_check_circle_24dp
        else -> 0
    }

    fun anon(): Int {
        return if (anon) R.drawable.ic_anon_24dp else 0
    }

    fun foregroundColor() = if (relevant) Color.parseColor("#FF000000") else Color.parseColor("#FF888888")

    fun visibilityDivider() = when {
        group == null -> View.VISIBLE
        lastInGroup -> View.VISIBLE
        else -> View.INVISIBLE
    }

    fun group() = group?.let {
        it.description
    } ?: ""

    fun visibilityGroup() = if (firstInGroup) View.VISIBLE else View.GONE
}