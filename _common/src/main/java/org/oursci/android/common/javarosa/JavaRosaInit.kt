package org.oursci.android.common.javarosa

import org.javarosa.core.services.IPropertyManager
import org.javarosa.core.services.PrototypeManager
import org.javarosa.model.xform.XFormsModule

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 06.09.17.
 */
class JavaRosaInit {


    companion object {
        val SERIALIABLE_CLASSES = arrayOf("org.javarosa.core.services.locale.ResourceFileDataSource", // JavaRosaCoreModule
                "org.javarosa.core.services.locale.TableLocaleSource", // JavaRosaCoreModule
                "org.javarosa.core.model.FormDef", "org.javarosa.core.model.SubmissionProfile", // CoreModelModule
                "org.javarosa.core.model.QuestionDef", // CoreModelModule
                "org.javarosa.core.model.GroupDef", // CoreModelModule
                "org.javarosa.core.model.instance.FormInstance", // CoreModelModule
                "org.javarosa.core.model.data.BooleanData", // CoreModelModule
                "org.javarosa.core.model.data.DateData", // CoreModelModule
                "org.javarosa.core.model.data.DateTimeData", // CoreModelModule
                "org.javarosa.core.model.data.DecimalData", // CoreModelModule
                "org.javarosa.core.model.data.GeoPointData", // CoreModelModule
                "org.javarosa.core.model.data.GeoShapeData", // CoreModelModule
                "org.javarosa.core.model.data.GeoTraceData", // CoreModelModule
                "org.javarosa.core.model.data.IntegerData", // CoreModelModule
                "org.javarosa.core.model.data.LongData", // CoreModelModule
                "org.javarosa.core.model.data.MultiPointerAnswerData", // CoreModelModule
                "org.javarosa.core.model.data.PointerAnswerData", // CoreModelModule
                "org.javarosa.core.model.data.SelectMultiData", // CoreModelModule
                "org.javarosa.core.model.data.SelectOneData", // CoreModelModule
                "org.javarosa.core.model.data.StringData", // CoreModelModule
                "org.javarosa.core.model.data.TimeData", // CoreModelModule
                "org.javarosa.core.model.data.UncastData", // CoreModelModule
                "org.javarosa.core.model.data.helper.BasicDataPointer", // CoreModelModule
                "org.javarosa.core.model.Action", // CoreModelModule
                "org.javarosa.core.model.actions.SetValueAction" // CoreModelModule
        )


        private var isJavaRosaInitialized = false


        /**
         * Isolate the initialization of JavaRosa into one method, called first
         * by the Collect Application.  Called subsequently whenever the Preferences
         * dialogs are exited (to potentially update username and email fields).
         */
        @Synchronized
        fun initializeJavaRosa(mgr: IPropertyManager) {
            if (!isJavaRosaInitialized) {
                // need a list of classes that formdef uses
                // unfortunately, the JR registerModule() functions do more than this.
                // register just the classes that would have been registered by:
                // new JavaRosaCoreModule().registerModule();
                // new CoreModelModule().registerModule();
                // replace with direct call to PrototypeManager
                PrototypeManager.registerPrototypes(SERIALIABLE_CLASSES)
                XFormsModule().registerModule()

                isJavaRosaInitialized = true
            }

            // needed to override rms property manager
            org.javarosa.core.services.PropertyManager
                    .setPropertyManager(mgr)
        }
    }

}