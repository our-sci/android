package org.oursci.android.common.ui.intro

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import org.oursci.android.common.R
import org.oursci.android.common.databinding.LayoutIntroItemBinding
import org.oursci.android.common.vo.IntroSlide
import timber.log.Timber

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 17.07.17.
 */
class IntroAdapter : RecyclerView.Adapter<IntroAdapter.ViewHolder>() {

    var slides = mutableListOf<IntroSlide>()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Timber.d("binding viewholder")
        holder.binding.item = slides[position]
        holder.binding.btSlide?.setOnClickListener {
            slides[position].buttonPressed?.let {
                it()
            }
        }
    }


    override fun getItemCount() = slides.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: LayoutIntroItemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.layout_intro_item,
                parent, false)

        return ViewHolder(binding)
    }

    class ViewHolder(val binding: LayoutIntroItemBinding) : RecyclerView.ViewHolder(binding.root)

    fun update(list: Array<out IntroSlide>) {
        slides.clear()
        slides.addAll(list.asList())
        notifyDataSetChanged()
    }

}