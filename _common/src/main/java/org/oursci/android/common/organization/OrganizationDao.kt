package org.oursci.android.common.organization

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import org.oursci.android.common.vo.Organization

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 10.05.18.
 */
@Dao
interface OrganizationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrganizations(info: List<Organization>)

    @Query("delete from user_organizations")
    fun deleteAllInfos()

    @Query("select * from user_organizations")
    fun getAllOrganizations(): List<Organization>
}