package org.oursci.android.common.ui.survey.browse

import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.measurement.MeasurementLoader
import org.oursci.android.common.survey.SurveyDb
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.ui.BaseViewModel
import org.oursci.android.common.ui.controls.measurement.MeasurementControl
import org.oursci.android.common.vo.survey.Survey
import org.oursci.android.common.webapi.Api
import retrofit2.Response
import timber.log.Timber
import java.io.File
import java.net.URLDecoder
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 16.10.17.
 */
class BrowseSurveysViewModel(app: BaseApplication) : BaseViewModel(app) {

    var search = ""

    val client = OkHttpClient()
    var currentSurvey: Survey? = null
        get() {
            synchronized(surveyLock) {
                return field
            }
        }

    val surveyLock = Any()


    val isLoading = AtomicBoolean(false)

    val surveysLoadedResult = ConsumableLiveData<Array<Api.SurveyBody>>()
    val surveyDownloaded = ConsumableLiveData<Survey>()
    val progress = ConsumableLiveData<Double>()

    var currentProgress = 0.0

    fun loadSurveys(search: String = "", downloadAll: Boolean = false) {
        this.search = search
        progress.postValue(currentProgress)

        if (isLoading.getAndSet(true)) {
            return
        }

        thread {
            val call: Response<Api.FindResponse>?
            try {
                val currentOrganization = app.getCurrentContributionOrg()

                val orgs = when {
                    currentOrganization != null -> emptyList()
                    !app.limitSurveysToOrganization.isEmpty() -> app.limitSurveysToOrganization.flatMap {organizationId ->
                        val resList = mutableListOf<String>()
                        app.api.fetchFavoritesOfOrganization(organizationId).execute().let { response ->
                            if (response.isSuccessful) {
                                response.body()?.string()?.let {body ->
                                    JSONObject(body).optJSONObject("favorites")?.optJSONArray("survey")?.let { surveys ->
                                        for (i in 0 until surveys.length()) {
                                            surveys.optJSONObject(i)?.optString("typeIdentifier")?.let {formId ->
                                                if (!resList.contains(formId)) {
                                                    resList.add(formId)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        resList
                    }
                    else -> emptyList()
                }

                call = when {
                    currentOrganization != null -> app.api.findSurveysByOrganization(currentOrganization.id, search)
                    else -> app.api.findSurveys(search)
                }.execute()


                if (call == null) {
                    throw IllegalStateException("unable to execute call")
                }
                if (!call.isSuccessful) {
                    Timber.e("request was not successful %s", call.errorBody()?.toString()
                            ?: "<unknown>")
                    Timber.e("call code: %d", call.code())
                }

                val body = call.body()

                val map = body?.content?.mapNotNull {
                    val isFromSameUser = it.user?.let { user ->
                        user.username == app.user.userID
                    } ?: false


                    if (orgs.isNotEmpty() && !isFromSameUser) {
                        if (!orgs.contains(it.formId)) {
                            return@mapNotNull null
                        }
                    }

                    it
                } ?: listOf()



                try {
                    map.map { it }.let {
                        // TODO remove surveys before inserting

                        if (downloadAll) {
                            SurveyDb.of(app).db.surveyInfoDao().deleteSurveys()
                            val toDownload = it.filter { form ->
                                orgs.contains(form.formId)
                            }

                            Timber.d("downloading n surveys: %d", toDownload.size)
                            downloadSync(toDownload)


                        }

                        SurveyDb.of(app).db.surveyInfoDao().insertSurveys(it)
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }


                surveysLoadedResult.postValue(map.toTypedArray().apply {
                    sortByDescending {
                        it.date
                    }
                })
            } catch (e: Throwable) {
                Timber.e(e)
                Timber.e("error loading surveys")
                surveysLoadedResult.postValue(null)
            }
            isLoading.set(false)
        }

    }

    fun download(surveyBody: Api.SurveyBody) {

        if (isLoading.getAndSet(true)) {
            return
        }

        thread {
            currentSurvey = null
            downloadSync(listOf(surveyBody))
            surveyDownloaded.postValue(currentSurvey)
            isLoading.set(false)
        }
    }

    fun downloadSync(surveys: List<Api.SurveyBody>) {

        val downloaded = mutableListOf<String>()
        surveys.forEachIndexed { index, surveyBody ->
            try {



                val call = app.api.fetchSurveyXml(surveyBody.formId).execute()
                        ?: throw IllegalStateException("unable to execute call")

                if (!call.isSuccessful) {
                    Timber.e("request was not successful %s", call.errorBody()?.toString()
                            ?: "<unknown>")
                    Timber.e("call code: %d", call.code())
                }

                val body = call.body() ?: throw IllegalStateException("body is null")

                val tmpFile = File(app.cacheDir, "tmpSurvey.xml")
                tmpFile.deleteOnExit()



                body.forEach innerFor@{ url ->
                    val splitted = url.split("?").firstOrNull() ?: return@forEachIndexed
                    val basename = splitted.split("/").lastOrNull() ?: return@forEachIndexed
                    if (URLDecoder.decode(basename, "utf-8") != surveyBody.formFile) {
                        return@innerFor
                    }



                    tmpFile.outputStream().use { outputStream ->
                        val request = Request.Builder()
                                .url(url).build()
                        val response = client.newCall(request).execute()
                        if (!response.isSuccessful) {
                            return@innerFor
                        }

                        response?.body()?.byteStream()?.use {
                            it.copyTo(outputStream)
                        } ?: return@innerFor

                        val survey = SurveyManager.of(app).createAndLoadSurvey(tmpFile)?.let {
                            Timber.d("successfully added survey: %s", it.name)
                            it
                        }

                        survey?.let {

                            val scriptIds = it.controls.filter { it is MeasurementControl }.mapNotNull { (it as? MeasurementControl)?.id }.toMutableList()

                            it.summaryScript?.let { summary ->
                                if (summary.isNotEmpty()) {
                                    scriptIds.add(summary)
                                }
                            }

                            scriptIds.forEachIndexed inner@{ index, scriptId ->

                                if (downloaded.contains(scriptId)) {
                                    return@inner
                                }

                                downloaded.add(scriptId) // add when attempting

                                val info = try {
                                    app.api.getScriptInformation(scriptId).execute()
                                } catch (e: Exception){
                                    Timber.d("unable to find script")
                                    return@inner;
                                }

                                if (!info.isSuccessful) {
                                    Timber.d("unable to download info for: %s".format(info.errorBody()
                                            ?: "<no error body>"))
                                    return@inner
                                }

                                val res = app.api.fetchScriptZip(scriptId).execute()
                                if (!res.isSuccessful) {
                                    Timber.d("unable to download: %s".format(res.errorBody()
                                            ?: "<no error body>"))
                                    return@inner
                                }

                                val outDir = File(app.cacheDir, "scripts_download")
                                outDir.mkdirs()
                                val outFile = File(outDir, "download.zip")
                                outFile.delete()
                                outFile.deleteOnExit()

                                outFile.outputStream().use { out ->
                                    res.body()?.byteStream()?.buffered()?.use {
                                        it.copyTo(out)
                                    }
                                }


                                val script = MeasurementLoader.of(app).extract(outFile, info.body()?.date)


                                script?.let {
                                    Timber.d("loaded script %s", it.name)
                                } ?: Timber.e("error loading script: %s", scriptId)

                                progress.postValue(currentProgress + (index + 1.0)/scriptIds.size * (1.0/surveys.size))
                            }
                        }

                        synchronized(surveyLock) {
                            currentSurvey = survey
                        }
                    }
                }
            } catch (e: Exception) {
                // TODO throw exception to user
                Timber.e(e)
            }

            currentProgress = ((index + 1.0) / surveys.size)
            progress.postValue(currentProgress)

        }


    }


}