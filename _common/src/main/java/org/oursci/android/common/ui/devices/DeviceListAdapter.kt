package org.oursci.android.common.ui.devices

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import org.oursci.android.common.R
import org.oursci.android.common.databinding.ItemDeviceBinding
import org.oursci.android.common.device.persist.KnownBluetooth
import timber.log.Timber

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 10.10.17.
 */
class DeviceListAdapter(
        private val onClick: (View, Any) -> Unit,
        private val context: Context
) : RecyclerView.Adapter<DeviceListAdapter.ViewHolder>() {

    data class DeviceDisplay(
            val device: Any,
            val title: String,
            val subTitle: String,
            val drawable: Drawable?
    )

    enum class State {
        CONNECTED,
        CONNECTING,
        KNOWN,
        PAIRED,
        UNPAIRED
    }

    private val iconBt by lazy {
        colorIcon(R.drawable.ic_bluetooth_black_24dp)
    }

    private val iconUsb by lazy {
        colorIcon(R.drawable.ic_usb_black_24dp)
    }


    private fun colorIcon(icon: Int) = AppCompatResources.getDrawable(context, icon)?.let {
        DrawableCompat.setTint(it, ContextCompat.getColor(context, R.color.foregroundDeviceInfo))
        it
    }


    var items = mutableListOf<Pair<Any, DeviceItemDisplay>>()

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.binding.root.tag = position

        // I know this is stupid but it works for now...
        holder.binding.btDisconnect.tag = position
        holder.binding.btConnect.tag = position
        holder.binding.btPair.tag = position
        holder.binding.btForget.tag = position
        holder.binding.btMore.tag = position

        items.getOrNull(position)?.let {
            holder.binding.item = it.second
        }


        if (position > 0 && ((items[position].first is KnownBluetooth && items[position - 1].first is KnownBluetooth) || items[position].first is BluetoothDevice && items[position - 1].first is BluetoothDevice)) {
            holder.binding.tvHeader.visibility = View.GONE
        } else {
            holder.binding.tvHeader.visibility = View.VISIBLE
        }

    }

    val ocl: View.OnClickListener = View.OnClickListener { view ->
        val idx = view.tag as Int?

        idx?.let {
            items.getOrNull(it)?.let { onClick(view, it.first) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemDeviceBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.item_device,
                parent, false)

        binding.btDisconnect.setOnClickListener(ocl)
        binding.btConnect.setOnClickListener(ocl)
        binding.btPair.setOnClickListener(ocl)
        binding.btForget.setOnClickListener(ocl)
        binding.btMore.setOnClickListener(ocl)

        return ViewHolder(binding)
    }

    class ViewHolder(val binding: ItemDeviceBinding) : RecyclerView.ViewHolder(binding.root)
}