package org.oursci.android.common.ui

import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import androidx.preference.PreferenceScreen
import org.joda.time.DateTime
import org.oursci.android.common.*
import org.oursci.android.common.ui.transition.Direction
import timber.log.Timber
import kotlin.concurrent.thread

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 10.10.17.
 */
abstract class BaseSettingsFragment : PreferenceFragmentCompat() {


    val app: BaseApplication
        get() {
            return (activity as BaseActivity).app
        }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        findPreference(getString(R.string.ontoligies))?.apply {
            val lastCommit = app.resourceCommitInformation()
            val time = DateTime.parse(lastCommit.createdAt)
            summary = "modified ${time.millis.timeAgo()}\nid: ${lastCommit.commitId}"

            setOnPreferenceClickListener {
                thread {
                    val baseActivity = (activity as? BaseActivity) ?: return@thread
                    baseActivity.showDialog("Ontologies", app.getAllOntologies().joinToString("\n\n") {
                        "==${it.id}==\n\n${it.items.joinToString("\n") {
                            it.name
                        }
                        }"
                    })
                }
                true
            }

        }
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        val key = getString(R.string.spectrometer_hostname)
        findPreference(key)?.apply {
            summary = PreferenceManager.getDefaultSharedPreferences(app).getString(key, "none")
            setOnPreferenceChangeListener { _, newValue ->
                summary = newValue.toString()
                true
            }
        }

        val keepAlive = getString(R.string.gps_lock)
        findPreference(keepAlive)?.setOnPreferenceChangeListener { _, newValue ->
            Timber.d("New value: %s", newValue)
            val v = (newValue as? Boolean) ?: return@setOnPreferenceChangeListener true
            val mainActivity = (activity as? BaseActivity)
                    ?: return@setOnPreferenceChangeListener true
            if (v) {
                if (!app().gpsLock(true)) {
                    mainActivity.showPermissionDialog(
                            "Permission Required",
                            "For using this feature, permissions are required",
                            { app().gpsLock(true) })
                }
            } else {
                app().gpsLock(false)
            }
            true
        }


    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        val dir = app.navigationManager.getDirection()
        return AnimationUtils.loadAnimation(activity, if (enter) dir.enterAnimation else dir.exitAnimation)
    }

    private fun directionFromArgs(key: String): Direction? = arguments?.getString(key)?.let {
        try {
            Direction.valueOf(it)
        } catch (e: Exception) {
            null
        }
    }

    fun popDirection() = directionFromArgs(app.navigationManager.keyPopDirection)
            ?: Direction.FROM_RIGHT.reverse()

}