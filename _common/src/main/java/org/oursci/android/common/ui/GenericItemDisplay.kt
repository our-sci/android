package org.oursci.android.common.ui

import android.graphics.drawable.Drawable
import android.text.Spanned
import android.view.View
import org.oursci.android.common.html

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 12.10.17.
 */
class GenericItemDisplay(
        var title: String,
        var description: String,
        var showLoading: Boolean = false,
        var iconLeft: Drawable? = null,
        var iconRight: Drawable? = null,
        var secondIconRight: Drawable? = null,
        var selected: Boolean = false,
        var large: Boolean = false,
        var largeIconRight: Boolean = false,
        var scrollHorizontal: Boolean = false
) {
    fun visibilityIconLeft() = if (iconLeft == null) View.GONE else View.VISIBLE
    fun visibilityIconRight() = if (iconRight == null || showLoading) View.GONE else View.VISIBLE
    fun visibilitySecondIconRight() = if (secondIconRight == null || showLoading) View.GONE else View.VISIBLE
    fun visibilityLoading() = if (showLoading) View.VISIBLE else View.GONE

    fun descriptionHtml(): Spanned {
        return html(description)
    }
}