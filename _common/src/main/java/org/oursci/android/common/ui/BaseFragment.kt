package org.oursci.android.common.ui

import androidx.lifecycle.ViewModelProviders
import androidx.fragment.app.Fragment
import androidx.appcompat.widget.SearchView
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import org.oursci.android.common.BaseActivity
import org.oursci.android.common.app
import org.oursci.android.common.ui.survey.results.SurveyResultListViewModel
import org.oursci.android.common.ui.transition.Direction
import timber.log.Timber

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 11.10.17.
 */
abstract class BaseFragment : Fragment(), IBackPressable {


    open val searchable = false

    inline fun <reified T : BaseViewModel> lazyViewModel(): Lazy<T> = lazy<T> {
        Timber.d("initializing view model for ${T::class.java}")
        ViewModelProviders.of(this, app().viewModelFactory).get(T::class.java)
    }

    val activity: BaseActivity
        get() = getActivity() as BaseActivity

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        val dir = activity.app.navigationManager.getDirection()
        Timber.d("fragment with dir: %s, %s".format(this::class.java.canonicalName, dir))
        if (activity.app.navigationManager.disableEntryAnimation) {
            activity.app.navigationManager.disableEntryAnimation = false
            return null
        }
        return animationFromDirection(dir, enter)
    }


    private fun directionFromArgs(key: String): Direction? = arguments?.getString(key)?.let {
        try {
            Direction.valueOf(it)
        } catch (e: Exception) {
            null
        }
    }


    private fun animationFromDirection(direction: Direction, enter: Boolean): Animation =
            AnimationUtils.loadAnimation(activity, if (enter) direction.enterAnimation else direction.exitAnimation)

    open fun onMenuItemPressed(menuItemId: Int): Boolean {
        return false
    }

    open fun menuItems() = emptyArray<BaseActivity.MenuItemInfo>()

    fun showMenuItem(id: Int, visible: Boolean) {
        activity.fragmentMenuItems.find {
            id == it.id
        }?.visible = visible
        activity.invalidateOptionsMenu()
    }

    fun toast(text: String) {
        activity.runOnUiThread {
            Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
        }
    }

    fun popDirection() = directionFromArgs(activity.app.navigationManager.keyPopDirection)
            ?: Direction.FROM_RIGHT.reverse()


    override fun onBackPressed(): Boolean {
        return false
    }

    private val viewModel by lazyViewModel<SurveyResultListViewModel>()


    open fun onSearchClicked(query: String): Boolean {
        Timber.d("searching for %s", query)
        viewModel.filter = query
        app().navigationManager.popRoot()
        app().navigationManager.navigateToSurveyResultsList(false)
        return true
    }

    fun searchView(): SearchView? = activity.searchView

    open fun onSearchClosed(): Boolean {
        return false
    }

    open fun searchText(): String {
        return ""
    }

    fun updateSearchView(text: String) {
        activity.expandSearchView()
        Timber.d("updating query text")
        activity.searchView?.setQuery(text, false)
        activity.hideKeyboard()
    }

    open fun onMenuRendered() {
    }

}