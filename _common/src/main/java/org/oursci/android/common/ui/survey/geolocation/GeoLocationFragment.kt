package org.oursci.android.common.ui.survey.geolocation

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import org.oursci.android.common.R
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.ControlGeolocationBinding
import org.oursci.android.common.databinding.LayoutControlFragmentBinding
import org.oursci.android.common.html
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.controls.geolocation.GeoLocationControl
import org.oursci.android.common.ui.controls.geolocation.GeoLocationDisplay
import timber.log.Timber

/**
 * Created by Manuel Di Cerbo on 03.11.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class GeoLocationFragment : BaseFragment() {

    val viewModel by lazyViewModel<GeoLocationViewModel>()
    var control: GeoLocationControl? = null
    var mapView: MapView? = null
    var map: GoogleMap? = null

    var selectedLocation = ""
    var savedInstance: Bundle? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        savedInstance = savedInstanceState
        super.onCreate(savedInstanceState)
    }


    private val controlLoadedResult = ConsumableObserver<GeoLocationControl> { control: GeoLocationControl?, consumed: Boolean ->
        if (control == null || consumed) {
            return@ConsumableObserver
        }


        this.control = control

        binding.swRemember
        controlBinding.control = object : GeoLocationDisplay(control = control) {
            override fun hint(): Spanned {
                return html("")
            }
        }
        controlBinding.invalidateAll()
    }


    private lateinit var binding: LayoutControlFragmentBinding
    private lateinit var controlBinding: ControlGeolocationBinding

    private var idx: Int = -1


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.loadControlLiveData.observe(this, controlLoadedResult)

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, p2: Bundle?): View? {
        idx = arguments?.getInt("idx", -1) ?: -1

        binding = LayoutControlFragmentBinding.inflate(inflater, container, false)


        binding.viewStubControl.viewStub?.layoutResource = R.layout.control_geolocation
        binding.viewStubControl.viewStub?.setOnInflateListener { _, view ->
            postInflate(view)
        }


        binding.viewStubControl.viewStub?.inflate()

        if (idx == -1) {
            activity.allowOverview(false)
            binding.btNext.visibility = View.GONE
        } else {
            activity.allowOverview(true)
        }

        binding.btNext.setOnClickListener {

            if (viewModel.hasMoreQuestions(idx)) {
                app().navigationManager.moveToNextSurveyQuestion(idx + 1)
            } else {
                app().navigationManager.finalOverview()
            }
        }

        binding.swAnon.visibility = View.GONE
        return binding.root
    }

    fun updateText(map: GoogleMap) {
        map.cameraPosition.target.let {
            it.latitude
            it.longitude
            selectedLocation = "${it.latitude}, ${it.longitude}"
            controlBinding.tvCoordinates.text = "Selected Location (click to copy to clipboard)\n${it.latitude}, ${it.longitude}\n"
        }

        app().lastLocation { location ->
            map.cameraPosition.target.let {
                it.latitude
                it.longitude
                controlBinding.tvCoordinates.text = "Selected Location (click to copy to clipboard)\n${it.latitude}, ${it.longitude}\nGPS Precision: ${location?.accuracy?.let { acc -> "$acc [m]" }
                        ?: "unknown"}"
            }
        }
    }


    private fun postInflate(view: View) {
        controlBinding = ControlGeolocationBinding.bind(view)
        controlBinding.progressMeasurement.progress = 0

        controlBinding.btPickLocation.setOnClickListener {
            controlBinding.layoutControl.visibility = View.GONE
            controlBinding.layoutPicker.visibility = View.VISIBLE
            binding.btNext.visibility = View.GONE
        }


        if (idx != -1) {
            viewModel.loadControl(idx, GeoLocationControl::class.java)
        }

        mapView = controlBinding.mapView

        val mv = mapView ?: return


        mv.onCreate(savedInstance)
        mv.getMapAsync {
            map = it
            it.uiSettings.setAllGesturesEnabled(true)
            it.uiSettings.isCompassEnabled = true
            it.uiSettings.isMapToolbarEnabled = true
            it.uiSettings.isMyLocationButtonEnabled = true
            it.uiSettings.isZoomControlsEnabled = true
            it.isMyLocationEnabled = true
            it.mapType = GoogleMap.MAP_TYPE_HYBRID

            app().lastLocation { location ->
                val l = location ?: return@lastLocation
                it.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(l.latitude, l.longitude), 17f))
            }

            it.setOnCameraMoveListener {
                updateText(it)
            }
            updateText(it)
        }

        controlBinding.tvCoordinates.setOnClickListener {

            val data = ClipData.newPlainText("Location", selectedLocation)
            val mgr = activity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            mgr.primaryClip = data
            Toast.makeText(activity, "Copied to Clipboard", Toast.LENGTH_SHORT).show()
        }

        controlBinding.btPicker.setOnClickListener {

            val lat = map?.cameraPosition?.target?.latitude
            val lon = map?.cameraPosition?.target?.longitude

            if (lat != null && lon != null) {
                viewModel.locationPicked(idx, lat, lon)
            } else {
                Toast.makeText(activity, "Unable to pick location", Toast.LENGTH_SHORT).show()
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            controlBinding.mapView.onDestroy()
        } catch (e: Exception) {
            Timber.d(e)
        }
    }

    override fun onPause() {
        try {
            controlBinding.mapView.onPause()
        } catch (e: Exception) {
            Timber.e(e)
        }
        super.onPause()
    }

    override fun onResume() {
        try {
            controlBinding.mapView.onResume()
        } catch (e: Exception) {
            Timber.d(e)
        }
        super.onResume()
    }

    override fun onStart() {
        try {
            controlBinding.mapView.onStart()
        } catch (e: Exception) {
            Timber.d(e)

        }
        super.onStart()
    }

    override fun onStop() {
        try {
            controlBinding.mapView.onStop()
        } catch (e: Exception) {
            Timber.e(e)
        }
        super.onStop()
    }

    override fun onLowMemory() {
        try {
            controlBinding.mapView.onLowMemory()
        } catch (e: Exception) {
            Timber.d(e)
        }
        super.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        try {
            controlBinding.mapView.onSaveInstanceState(outState)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        super.onSaveInstanceState(outState)
    }
}