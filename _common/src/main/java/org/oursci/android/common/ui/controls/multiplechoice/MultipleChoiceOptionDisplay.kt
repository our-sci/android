package org.oursci.android.common.ui.controls.multiplechoice

import android.net.Uri
import android.view.View
import timber.log.Timber

/**
 * Created by Manuel Di Cerbo on 22.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class MultipleChoiceOptionDisplay(
        val option: MultipleChoiceControl.Option
) {

    fun imageUrl(): Uri? {
        return when (option.type) {
            MultipleChoiceControl.Option.Type.IMAGE -> {
                val uri = Uri.parse(option.url)
                Timber.d("parsing uri")
                uri
            }
            else -> null
        }
    }

    fun text(): String = option.text

    fun checked(): Boolean {
        return option.selected
    }

    fun imageVisibility(): Int {
        return when (option.type) {
            MultipleChoiceControl.Option.Type.IMAGE -> View.VISIBLE
            else -> View.GONE
        }
    }

    fun checkBoxVisibility() = when (option.type) {
        MultipleChoiceControl.Option.Type.IMAGE -> View.GONE
        else -> View.VISIBLE
    }

}