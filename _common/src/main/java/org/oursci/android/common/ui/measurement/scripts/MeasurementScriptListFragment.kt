package org.oursci.android.common.ui.measurement.scripts

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.FragmentGenericListBinding
import org.oursci.android.common.measurement.MeasurementScript
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.GenericItemAdapter
import org.oursci.android.common.ui.GenericItemDisplay
import timber.log.Timber

/**
 * Created by Manuel Di Cerbo on 12.10.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class MeasurementScriptListFragment : BaseFragment() {

    val adapter = GenericItemAdapter(
            produceDisplay = { measurementScript: MeasurementScript ->
                GenericItemDisplay(
                        measurementScript.name,
                        measurementScript.description
                )
            },
            onClick = { measurementScript: MeasurementScript, _ ->
                app().navigationManager.navigateToMeasurement(measurementScript.id)
            })

    val viewModel by lazyViewModel<MeasurementScriptListViewModel>()
    lateinit var binding: FragmentGenericListBinding

    private val availableScriptsResultObserver =
            ConsumableObserver<Array<MeasurementScript>> { scripts, consumed ->
                if (consumed) {
                    return@ConsumableObserver
                }

                binding.progress.visibility = View.GONE

                scripts?.let {
                    adapter.items = it.asList()
                    adapter.notifyDataSetChanged()
                }
            }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Timber.d("onviewcreated")
        viewModel.availableScripsResult.observe(this, availableScriptsResultObserver)
        adapter.items = emptyList()

        activity.lockMenus()
        activity.allowOverview(false)
        binding.progress.visibility = View.VISIBLE

        view.postDelayed({
            viewModel.listMeasurementScripts()
        }, 500)



        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentGenericListBinding.inflate(inflater, container, false)
        binding.recyclerview.adapter = adapter
        binding.recyclerview.layoutManager = LinearLayoutManager(activity)
        return binding.root
    }

}