package org.oursci.android.common.ui.survey.browse

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.FragmentGenericListBinding
import org.oursci.android.common.timeAgo
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.GenericItemAdapter
import org.oursci.android.common.ui.GenericItemDisplay
import org.oursci.android.common.vo.survey.Survey
import org.oursci.android.common.webapi.Api
import timber.log.Timber

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 16.10.17.
 */
class BrowseSurveysFragment : BaseFragment() {

    override val searchable = true

    private val surveysResultObserver = ConsumableObserver<Array<Api.SurveyBody>> { surveys: Array<Api.SurveyBody>?, consumed: Boolean ->
        if (consumed) {
            return@ConsumableObserver
        }

        activity.invalidateOptionsMenu()


        binding.progress.visibility = View.GONE
        binding.recyclerview.visibility = View.VISIBLE
        binding.invalidateAll()

        if (surveys == null) {
            return@ConsumableObserver
        }

        adapter.items = surveys.asList().filter { !it.archived }
        adapter.notifyDataSetChanged()
    }

    private val surveyDownloadedObserver = ConsumableObserver<Survey> { survey, consumed ->
        if (consumed) {
            return@ConsumableObserver
        }

        survey?.let {
            activity.app.onSurveyLoaded(it)
        } ?: toast("error downloading survey")

        binding.progress.visibility = View.GONE
        binding.recyclerview.visibility = View.VISIBLE

    }

    val viewModel by lazyViewModel<BrowseSurveysViewModel>()
    lateinit var binding: FragmentGenericListBinding

    val adapter = GenericItemAdapter({ surveyBody: Api.SurveyBody ->

        val (title, description) = try {
            surveyBody.formTitle?.split(";")?.let {
                it[0] to it[1]
            } ?: (surveyBody.formTitle ?: "<title missing>") to ""
        } catch (e: Exception) {
            (surveyBody.formTitle ?: "<title missing>") to ""
        }

        GenericItemDisplay(
                title = title,
                description = "%s%s".format(description.let {
                    when {
                        it.isNotEmpty() -> "$it<br>"
                        else -> it
                    }
                }, "<small>updated ${surveyBody.date.timeAgo()}</small>")
                )
    }, { surveyBody: Api.SurveyBody, view: View ->
        Timber.d("clicked item %s", surveyBody.formTitle)
        viewModel.download(surveyBody = surveyBody)
        binding.recyclerview.visibility = View.GONE
        binding.progress.visibility = View.VISIBLE
    })


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity.lockMenus()
        activity.allowOverview(false)


        viewModel.surveysLoadedResult.observe(this, surveysResultObserver)
        viewModel.surveyDownloaded.observe(this, surveyDownloadedObserver)

        viewModel.loadSurveys()
        binding.recyclerview.visibility = View.GONE
        binding.progress.visibility = View.VISIBLE
        binding.invalidateAll()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentGenericListBinding.inflate(inflater, container, false)
        binding.recyclerview.adapter = adapter
        binding.recyclerview.layoutManager = LinearLayoutManager(activity)
        binding.recyclerview.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))


        if (viewModel.isLoading.get()) {
            binding.recyclerview.visibility = View.GONE
            binding.progress.visibility = View.VISIBLE
        }

        return binding.root
    }

    override fun onSearchClicked(query: String): Boolean {
        Timber.d("query="+query)
        viewModel.loadSurveys(query)
        return true
    }

    override fun onSearchClosed(): Boolean {
        viewModel.loadSurveys()
        return true
    }

    override fun searchText(): String {
        return viewModel.search
    }

    /*
    override fun onMenuRendered() {
        if (viewModel.filter.isNotEmpty()) {
            updateSearchView(viewModel.filter)
        }
    }
    */
}