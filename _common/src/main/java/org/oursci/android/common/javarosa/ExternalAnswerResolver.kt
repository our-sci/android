package org.oursci.android.common.javarosa

import org.javarosa.core.model.Constants
import org.javarosa.core.model.FormDef
import org.javarosa.core.model.SelectChoice
import org.javarosa.core.model.data.*
import org.javarosa.core.model.data.helper.Selection
import org.javarosa.core.model.instance.TreeElement
import org.javarosa.core.model.instance.utils.DefaultAnswerResolver
import org.javarosa.core.model.util.restorable.RestoreUtils
import org.javarosa.core.model.utils.DateUtils
import org.javarosa.xform.parse.XFormParser
import org.javarosa.xform.util.XFormAnswerDataSerializer
import org.javarosa.xpath.XPathParseTool
import org.javarosa.xpath.expr.XPathFuncExpr
import timber.log.Timber
import java.util.*
import java.util.regex.Pattern

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 06.09.17.
 */
class ExternalAnswerResolver : DefaultAnswerResolver() {

    override fun resolveAnswer(textVal: String, treeElement: TreeElement, formDef: FormDef): IAnswerData {
        val questionDef = XFormParser.ghettoGetQuestionDef(treeElement.dataType,
                formDef, treeElement.ref)
        if (questionDef != null && (questionDef.controlType == Constants.CONTROL_SELECT_ONE || questionDef.controlType == Constants.CONTROL_SELECT_MULTI)) {
            var containsSearchExpression = false

            var xpathExpression: XPathFuncExpr? = null
            try {
                xpathExpression = getSearchXPathExpression(
                        questionDef.appearanceAttr)
            } catch (e: Exception) {
                Timber.e(e)
                // there is a search expression, but has syntax errors
                containsSearchExpression = true
            }

            if (xpathExpression != null || containsSearchExpression) {
                // that means that we have dynamic selects

                // read the static choices of the options sheet
                val staticChoices = questionDef.choices
                for (index in staticChoices.indices) {
                    val selectChoice = staticChoices[index]
                    val selectChoiceValue = selectChoice.value

                    var isInt = true

                    try {
                        selectChoiceValue.trim().toInt()
                    } catch (e: Exception) {
                        isInt = false
                    }

                    if (isInt) {
                        val selection = selectChoice.selection()

                        when (questionDef.controlType) {
                            Constants.CONTROL_SELECT_ONE -> {
                                if (selectChoiceValue == textVal) {
                                    // This means that the user selected a static selection.
                                    //
                                    // Although (for select1 fields) the default implementation
                                    // will catch this and return the right thing
                                    // (if we call super.resolveAnswer(textVal, treeElement,
                                    // formDef))
                                    // we just need to make sure, so we will override that.
                                    if (questionDef.controlType == Constants.CONTROL_SELECT_ONE) {
                                        // we don't need another, just return the static choice.
                                        return SelectOneData(selection)
                                    }
                                }
                            }
                            Constants.CONTROL_SELECT_MULTI -> {
                                // we should search in a potential comma-separated string of
                                // values for a match
                                // copied of org.javarosa.xform.util.XFormAnswerDataParser
                                // .getSelections()
                                val textValues = DateUtils.split(textVal,
                                        XFormAnswerDataSerializer.DELIMITER, true)
                                if (textValues.contains(textVal)) {
                                    // this means that the user has selected AT LEAST the static
                                    // choice.
                                    if (selectChoiceValue == textVal) {
                                        // this means that the user selected ONLY the static
                                        // answer, so just return that
                                        val customSelections = ArrayList<Selection>()
                                        customSelections.add(selection)
                                        return SelectMultiData(customSelections)
                                    } else {
                                        // we will ignore it for now since we will return that
                                        // selection together with the dynamic ones.
                                    }
                                }
                            }
                            else -> {
                                // There is a bug if we get here, so let's throw an Exception
                                throw createBugRuntimeException(treeElement, textVal)
                            }
                        }

                    } else {
                        when (questionDef.controlType) {
                            Constants.CONTROL_SELECT_ONE -> {
                                // the default implementation will search for the "textVal"
                                // (saved answer) inside the static choices.
                                // Since we know that there isn't such, we just wrap the textVal
                                // in a virtual choice in order to
                                // create a SelectOneData object to be used as the IAnswer to the
                                // TreeElement.
                                // (the caller of this function is searching for such an answer
                                // to populate the in-memory model.)
                                val customSelectChoice = SelectChoice(textVal, textVal,
                                        false)
                                customSelectChoice.index = index
                                return SelectOneData(customSelectChoice.selection())
                            }
                            Constants.CONTROL_SELECT_MULTI -> {
                                // we should create multiple selections and add them to the pile
                                val customSelectChoices = createCustomSelectChoices(
                                        textVal)
                                val customSelections = ArrayList<Selection>()
                                for (customSelectChoice in customSelectChoices) {
                                    customSelections.add(customSelectChoice.selection())
                                }
                                return SelectMultiData(customSelections)
                            }
                            else -> {
                                // There is a bug if we get here, so let's throw an Exception
                                throw createBugRuntimeException(treeElement, textVal)
                            }
                        }

                    }
                }

                // if we get there then that means that we have a bug
                throw createBugRuntimeException(treeElement, textVal)
            }
        }
        // default behavior matches original behavior (for static selects, etc.)

        println("textval " + textVal)
        println("tree element" + treeElement)
        println("tree element data type" + treeElement.dataType)
        println("formdef " + formDef)
        println("super " + super.toString())
        val defaultAnswer = super.resolveAnswer(textVal, treeElement, formDef)
        println("default Answer: " + defaultAnswer)
        return defaultAnswer ?: StringData(textVal)
    }

    private fun createBugRuntimeException(treeElement: TreeElement, textVal: String): RuntimeException {
        return RuntimeException("The appearance column of the field " + treeElement.name
                + " contains a search() call and the field type is " + treeElement.dataType
                + " and the saved answer is " + textVal)
    }

    protected fun createCustomSelectChoices(completeTextValue: String): List<SelectChoice> {
        // copied of org.javarosa.xform.util.XFormAnswerDataParser.getSelections()
        val textValues = DateUtils.split(completeTextValue,
                XFormAnswerDataSerializer.DELIMITER, true)

        var index = 0
        val customSelectChoices = ArrayList<SelectChoice>()
        for (textValue in textValues) {
            val selectChoice = SelectChoice(textValue, textValue, false)
            selectChoice.index = index++
            customSelectChoices.add(selectChoice)
        }

        return customSelectChoices
    }

    private val SEARCH_FUNCTION_REGEX = Pattern.compile("search\\(.+\\)")

    fun getSearchXPathExpression(appearance: String?): XPathFuncExpr? {
        var appearance = appearance
        if (appearance == null) {
            appearance = ""
        }
        appearance = appearance.trim { it <= ' ' }

        val matcher = SEARCH_FUNCTION_REGEX.matcher(appearance)
        if (matcher.find()) {
            val function = matcher.group(0)
            try {
                val xpathExpression = XPathParseTool.parseXPath(function)
                if (XPathFuncExpr::class.java.isAssignableFrom(xpathExpression.javaClass)) {
                    val xpathFuncExpr = xpathExpression as XPathFuncExpr
                    return if (xpathFuncExpr.id.name.equals(
                            "search", ignoreCase = true)) {
                        // also check that the args are either 1, 4 or 6.
                        if (xpathFuncExpr.args.size == 1 || xpathFuncExpr.args.size == 4
                                || xpathFuncExpr.args.size == 6) {
                            xpathFuncExpr
                        } else {
                            throw IllegalArgumentException("wrong arguments")
                        }
                    } else {
                        // this might mean a problem in the regex above. Unit tests required.
                        throw IllegalArgumentException("wrong function")
                    }
                } else {
                    // this might mean a problem in the regex above. Unit tests required.
                    throw IllegalArgumentException("search bad function")
                }
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
                throw IllegalArgumentException("Generic Exception")
            }

        } else {
            return null
        }
    }
}