package org.oursci.android.common.ui.controls

import org.javarosa.form.api.FormEntryPrompt

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 27.02.18.
 */

fun FormEntryPrompt.questionId() = question.textID ?: index.reference.nameLast