package org.oursci.android.common.ui

import android.annotation.SuppressLint
import androidx.lifecycle.ViewModel
import org.oursci.android.common.BaseApplication

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 27.07.17.
 */
@SuppressLint("StaticFieldLeak")
abstract class BaseViewModel(val app: BaseApplication) : ViewModel()