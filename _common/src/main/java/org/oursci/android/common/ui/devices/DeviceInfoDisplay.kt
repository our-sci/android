package org.oursci.android.common.ui.devices

import android.view.View

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 11.10.17.
 */
class DeviceInfoDisplay(
        private val title: String = "",
        private val content: String = "",
        private val scanEnabled: Boolean = false,
        private val connectEnabled: Boolean = true,
        private val connecting: Boolean = false
) {

    fun title() = title
    fun content() = content
    fun scanEnabled() = scanEnabled
    fun connectEnabled() = connectEnabled
    fun visibilityConnecting() = if (connecting) View.VISIBLE else View.GONE
    fun visibilityRest() = if (visibilityConnecting() == View.VISIBLE) View.GONE else View.VISIBLE

}