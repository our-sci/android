package org.oursci.android.common.ui.survey.mutliplechoice

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.oursci.android.common.R
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.ControlMultiplechoiceBinding
import org.oursci.android.common.databinding.LayoutControlFragmentBinding
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceControl
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceDisplay
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceOptionDisplay
import timber.log.Timber
import java.net.URLDecoder
import java.net.URLEncoder
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Created by Manuel Di Cerbo on 22.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class MultipleChoiceFragment : BaseFragment() {

    override val searchable = true

    var idx = -1
    private val checkFromUi = AtomicBoolean(false)


    private val multipleChoiceViewModel: MultipleChoiceViewModel by lazy {
        ViewModelProviders.of(this, app().viewModelFactory).get(MultipleChoiceViewModel::class.java)
    }


    private val rvAdapter = MultipleChoiceAdapter(onClick = { item ->

        Timber.d("onclick")

        val current = mutableListOf<String>()


        control?.let {
            it.answer.answer?.let {
                current.addAll(it)
            }

            if (it.multiSelect) {
                if (current.contains(item.option.value)) {
                    current.remove(item.option.value)
                    item.option.selected = false
                } else {
                    current.add(item.option.value)
                    item.option.selected = true
                }

                if (current.isEmpty()) {
                    it.answer = it.answer.create(emptyArray(), it.answer.anon)
                    updateAdapter(item)
                } else {
                    updateAdapter(item)
                    it.answer = it.answer.create(current.toTypedArray(), it.answer.anon)
                }

            } else {
                val toDeselect = singleSelect(item, current)
                if (item.option.selected) {
                    it.answer = it.answer.create(arrayOf(item.option.value), it.answer.anon)
                } else {
                    it.answer = it.answer.create(emptyArray(), it.answer.anon)
                }
                updateAdapter(item)
                toDeselect.forEach { desel ->
                    updateAdapter(desel)
                }
            }


            val valueToRemember = if (binding.swRemember.isChecked) it.answer.answer?.joinToString(";") { URLEncoder.encode(it, "utf-8") } else null
            app().storeRemember(it.dataName, valueToRemember)

        }



        multipleChoiceViewModel.saveSurvey()
    })

    private fun deselectAll() {
        rvAdapter.items.forEach { it.option.selected = false }
    }

    private fun singleSelect(item: MultipleChoiceOptionDisplay, current: MutableList<String>): List<MultipleChoiceOptionDisplay> {
        val toDeselect = rvAdapter.items.filter {
            it.option.selected && it != item
        }

        if (current.contains(item.option.value)) {
            current.remove(item.option.value)
            item.option.selected = false
            return emptyList()
        }

        rvAdapter.items.forEach {
            it.option.selected = it == item
        }
        return toDeselect
    }

    private fun updateAdapter(item: MultipleChoiceOptionDisplay) {
        rvAdapter.refresh(item)
    }

    lateinit var binding: LayoutControlFragmentBinding
    var control: MultipleChoiceControl? = null

    private lateinit var multipleChoiceBinding: ControlMultiplechoiceBinding
    private lateinit var multipleChoiceDisplay: MultipleChoiceDisplay

    private var remembered: String? = null


    private val multipleChoiceControlObserver = ConsumableObserver<MultipleChoiceControl> { control, consumed ->

        this.control = control

        if (control == null || consumed) {
            return@ConsumableObserver
        }

        remembered = app().resolveRemember(control.dataName)

        if (control.answer().isNullOrEmpty() && !multipleChoiceViewModel.surveyUploaded()) { // only override if null or empty
            remembered?.let {
                val newAnswer = control.answer.create(it.split(";").map { URLDecoder.decode(it, "utf-8") }.toTypedArray(), control.answer.anon)
                control.answer = newAnswer
                multipleChoiceViewModel.saveSurvey()
            }
        }


        binding.swRemember.visibility = View.VISIBLE
        binding.swRemember.isChecked = remembered?.let { true } ?: false


        multipleChoiceDisplay = MultipleChoiceDisplay(control)
        multipleChoiceBinding.item = multipleChoiceDisplay
        //multipleChoiceBinding.invalidateAll()


        rvAdapter.items.clear()
        rvAdapter.items.addAll(control.options.map { option ->
            val selected = control.answer.answer?.let {
                if (it.contains(option.value)) {
                    return@let true
                }
                false
            } ?: false
            option.selected = selected
            MultipleChoiceOptionDisplay(option)
        })


        rvAdapter.notifyDataSetChanged()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity.allowOverview(true)
        activity.unlockMenus()

        binding.btNext.setOnClickListener {

            control?.let {
                val valueToRemember = if (binding.swRemember.isChecked) it.answer.answer?.joinToString(";") { URLEncoder.encode(it, "utf-8") } else null
                app().storeRemember(it.dataName, valueToRemember)
            }

            if (multipleChoiceViewModel.hasMoreQuestions(idx)) {
                app().navigationManager.moveToNextSurveyQuestion(idx + 1)
            } else {
                app().navigationManager.finalOverview()
            }
        }


        binding.swAnon.visibility = View.GONE
        checkFromUi.set(true)


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        idx = arguments?.getInt("idx", 0) ?: 0

        binding = LayoutControlFragmentBinding.inflate(inflater, container, false)
        binding.viewStubControl.viewStub?.layoutResource = R.layout.control_multiplechoice
        binding.viewStubControl.viewStub?.setOnInflateListener { _, view ->
            multipleChoiceBinding = ControlMultiplechoiceBinding.bind(view)
            multipleChoiceBinding.rvMultiplechoice.layoutManager = LinearLayoutManager(activity)
            multipleChoiceBinding.rvMultiplechoice.itemAnimator = null
            multipleChoiceBinding.rvMultiplechoice.adapter = rvAdapter
            multipleChoiceViewModel.loadControlLiveData.observe(this, multipleChoiceControlObserver)
            multipleChoiceViewModel.loadControl(idx, MultipleChoiceControl::class.java)
        }

        binding.viewStubControl.viewStub?.inflate()
        return binding.root
    }
}