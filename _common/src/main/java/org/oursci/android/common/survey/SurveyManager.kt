package org.oursci.android.common.survey

import com.crashlytics.android.Crashlytics
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.javarosa.core.model.FormIndex
import org.javarosa.core.model.GroupDef
import org.javarosa.core.model.SelectChoice
import org.javarosa.core.model.data.*
import org.javarosa.core.model.data.helper.Selection
import org.joda.time.DateTime
import org.json.JSONObject
import org.oursci.android.common.*
import org.oursci.android.common.javarosa.Event
import org.oursci.android.common.measurement.MeasurementLoader
import org.oursci.android.common.organization.OrganizationDb
import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.ui.controls.geolocation.GeoLocationControl
import org.oursci.android.common.ui.controls.measurement.MeasurementControl
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceControl
import org.oursci.android.common.ui.controls.text.TextControl
import org.oursci.android.common.vo.survey.*
import timber.log.Timber
import java.io.BufferedWriter
import java.io.File
import java.io.FileOutputStream
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.util.concurrent.TimeUnit

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 18.09.17.
 */

class SurveyManager(val app: BaseApplication) {


    var currentSurvey: Survey? = null
    //var controlMap: Map<BaseControl, FormIndex> = emptyMap()


    companion object {

        private val lock = Object()

        private var sInstance: SurveyManager? = null

        fun of(app: BaseApplication): SurveyManager {
            synchronized(lock) {
                if (sInstance == null) {
                    sInstance = SurveyManager(app)
                }

                return sInstance!!
            }
        }
    }

    private val loader = SurveyLoader(
            File(app.appFilesDir, "surveys")
    ) { app.user.userID }

    fun resolve(item: SurveyOverviewItem): Int {
        return item.index.toInt()
    }

    fun listOfflineSurveyIds() = loader.listAllSurveyIds()

    fun loadSurveyListItem(formId: String): SurveyListItem? {
        val id = loader.listAllSurveyIds().find { it == formId } ?: return null
        val file = loader.surveyFileFromId(id) ?: return null

        val survey = try {
            loader.fromFormEntryController(loader.loadFormByPath(file.absolutePath, null)).first
        } catch (e: Exception) {
            Timber.e(e)
            return null
        }

        return SurveyListItem(survey.name, "%s".format(survey.description, survey.formId),
                file.parentFile.name, surveyId = survey.formId)

    }

    fun loadSurveys(): Array<SurveyListItem> = loader.listAllSurveyFiles().mapNotNull {
        val survey = try {
            loader.fromFormEntryController(loader.loadFormByPath(it.absolutePath, null)).first
        } catch (e: Exception) {
            Timber.e(e)
            return@mapNotNull null
        }


        SurveyListItem(survey.name, "%s".format(survey.description, survey.formId),
                it.parentFile.name, surveyId = survey.formId)

    }.toTypedArray()


    private fun loadSurvey(formPath: String, instanceFile: File? = null): Survey? {
        val fec = loader.loadFormByPath(formPath, instanceFile)
        val res = loader.fromFormEntryController(fec, instanceFile)
        val survey = res.first

        Timber.d("map is: %s", survey.indexMap.toList().joinToString("\n") { entry ->
            "${entry.first} => ${entry.second}"
        })
        return survey
    }

    fun loadSurveyFromName(surveyName: String, instanceFile: File) = loader.loadSurvey(surveyName, instanceFile)
    fun loadFormFromName(formName: String) = loader.loadForm(formName)


    fun loadSampleSurvey() {
        // loadSurveyFromAsset(assetPath = "soil_carbon.xml",
        //        instanceFile = File(File(app.appFilesDir, "instances"), "first_instance.xml"))
    }

    fun saveSurvey(survey: Survey, modified: Boolean = false) {
        loader.persist(survey, modified)
        updateSurveyInfoDb(survey)
    }

    fun saveCurrentSurvey() {
        val survey = currentSurvey ?: throw IllegalStateException("survey not defined")
        saveSurvey(survey, true)
        updateSurveyInfoDb(survey)
    }


    fun loadSurveyResultsList(): List<SurveyResultInfo> {
        val map = loader.listAllInstances()


        Timber.d("loaded %d instances", map.size)
        return map.sortedByDescending { it.second.name }.mapNotNull { entry ->
            val surveyName = entry.first
            val instance = entry.second

            try {
                loadSurveyFromName(surveyName, instance)?.let { survey ->
                    return@mapNotNull populateSurveyResultInfo(survey, surveyName, instance)
                }
            } catch (err: Exception) {
                Timber.e(err)
                null
            }
        }
    }

    private fun populateSurveyResultInfo(survey: Survey, surveyName: String, instance: File): SurveyResultInfo {
        val orgs = OrganizationDb.of(app).db.orgainzationDao().getAllOrganizations()

        return SurveyResultInfo(
                title = survey.name,
                description = survey
                        .controls
                        .filter(survey::isControlRelevant)
                        .joinToString("<br>") {


                            val strRep = when (it) {
                                is GeoLocationControl -> {
                                    it.answer()?.let {
                                        if (it.isNotEmpty()) "<i>location entered</i>" else "<i>location pending</i>"
                                    } ?: "<i>location pending</i>"
                                }
                                is MeasurementControl -> {
                                    it.answer()?.let ans@{

                                        if (it.isEmpty()) {
                                            return@ans "<i>measurement pending</i>"
                                        }
                                        try {
                                            JSONObject(it).getJSONObject("data").let { json ->

                                                val buffer = StringBuffer()
                                                var idx = 0
                                                json.keys().forEach {
                                                    buffer.append(
                                                            "<font color='#%06X'>%s</font> ".format(
                                                                    Palette.getColor(idx),
                                                                    json.getString(it)
                                                            ))
                                                    idx++
                                                }

                                                if (buffer.toString().isNotEmpty()) {
                                                    buffer.toString()
                                                } else "<i>measurement done</i>"

                                            }
                                        } catch (e: Exception) {
                                            "<i>error parsing answer</i>"
                                        }
                                    } ?: "<i>measurement pending</i>"
                                }
                                else -> {
                                    it.answer()?.let {
                                        if (it.isNotEmpty()) it else "<i>answer pending</i>"
                                    } ?: "<i>answer pending</i>"
                                }
                            }.let { res -> "<small><font color='#000'>%s</small> %s".format(it.label, res) }
                            strRep
                        }.let {
                            if (it.length > 5000) {
                                it.substring(0, 5000) + "..."
                            } else {
                                it
                            }
                        },
                state = when {
                    isExported(survey.instance) -> SurveyResultInfo.State.EXPORTED
                    survey.uploaded -> SurveyResultInfo.State.UPLOADED
                    survey.controls
                            .filterIsInstance<MeasurementControl>()
                            .any {
                                it.status() == MeasurementControl.Companion.Status.ERROR
                            } -> SurveyResultInfo.State.SCRIPT_ERROR
                    survey.controls
                            .filterIsInstance<MeasurementControl>()
                            .any {
                                it.status() == MeasurementControl.Companion.Status.WARNING
                            } -> SurveyResultInfo.State.SCRIPT_WARNING
                    survey.allRequiredAnswered() -> SurveyResultInfo.State.FINISHED
                    else ->
                        SurveyResultInfo.State.ONGOING

                },
                done = survey.allRequiredAnswered(),
                exported = isExported(survey.instance),
                name = surveyName,
                instance = instance,
                textSearch = survey.controls.mapNotNull {
                    (it as? TextControl)?.answer?.answer?.let { URLEncoder.encode(it, "UTF-8") }

                }.joinToString("/"),
                organizationName = survey.organizationId?.let { orgId ->
                    if (orgId.isEmpty()) {
                        ""
                    } else {
                        orgs.find { it.organizationId == survey.organizationId }?.name
                                ?: "unknown organization"
                    }
                } ?: ""


        )
    }

    private fun isExported(instance: File?): Boolean {
        if (instance == null) {
            return false
        }
        return exportFileFromSurvey(instance)?.exists() ?: false
    }

    fun createSurveyInstance(surveyId: String): Survey? {
        try {
            currentSurvey = loader.createNewSurvey(surveyId)
            return currentSurvey
        } catch (e: Exception) {
            Timber.e(e)
        }
        return null
    }

    fun surveyFileFromName(s: String): File {
        return loader.PathSpec(s).formFile
    }


    fun createAndLoadSurvey(tmpFile: File): Survey? {
        val survey = try {
            loader.loadFormByPath(tmpFile.absolutePath)
        } catch (ex: Exception) {
            Timber.e(ex)
            null
        }

        survey?.let {
            // loaded successfully, move it

            val surveyName = loader.surveyId(it.model)
            Timber.d("survey title is: %s", surveyName)
            val file = surveyFileFromName(surveyName)
            Timber.d("destination file %s", file.absolutePath)
            file.parentFile.mkdirs()
            file.delete()
            tmpFile.copyTo(file)
            currentSurvey = loadFormFromName(surveyName)
            return currentSurvey
        }

        return null
    }

    fun deleteSurveyInstance(surveyInstance: File) {
        val parent = app.trashDir()
        if (!parent.exists()) {
            parent.mkdirs()
        }

        if (!parent.isDirectory) {
            throw IllegalStateException(
                    "unable to create parent dir: ${parent.absolutePath}")
        }
        surveyInstance.renameTo(File(app.trashDir(), surveyInstance.name))

    }

    private fun deleteMeasurement(control: MeasurementControl) {
        val parent = app.trashMeasurementsDir()
        if (!parent.exists()) {
            parent.mkdirs()
        }

        if (!parent.isDirectory) {
            throw IllegalStateException(
                    "unable to create parent dir: ${parent.absolutePath}")
        }

        try {
            val answer = control.answer.answer ?: return
            val fileName = resolvePathFromMeasurementAnswer(answer)
            val file = MeasurementLoader.of(app).resolveMeasurementFile(control.id, fileName)
            file?.absolutePath?.let {
                file.renameTo(File(app.trashMeasurementsDir(), file.name))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    fun deleteCurrentSurveyInstance() {
        currentSurvey?.let { survey ->
            survey.controls.forEach {
                try {
                    (it as? MeasurementControl)?.let(::deleteMeasurement)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            survey.instance?.let {
                deleteSurveyInstance(it)
            }

            deleteSurveyInfoFromDb(survey)
            currentSurvey = null
        }
    }

    fun downloadSurvey(s: String) {
        try {
            val file = File(app.cacheDir, "tmp-survey.xml")
            file.deleteOnExit()

            val conn = URL(s).openConnection() as HttpURLConnection
            val connStream = conn.inputStream
            connStream.use {
                file.outputStream().use {
                    connStream.copyTo(it)
                }
            }

            createAndLoadSurvey(file)
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    private fun exportFileFromSurvey(instance: File): File? {
        val name = instance.name.let {
            if (it.endsWith("xml")) {
                return@let it.substring(0, it.length - 3) + "csv"
            }
            Timber.e("file ${instance.absolutePath} does not end with xml")
            null
        } ?: return null

        val exportDir = File(app.appFilesDir, "csv_export")
        val surveyExportDir = File(exportDir, instance.parentFile.parentFile.name)
        return File(surveyExportDir, name)
    }

    fun exportCurrentSurvey(): String? {
        return currentSurvey?.let(this::exportSurvey)
    }

    fun exportSurvey(survey: Survey): String? {
        val instance = survey.instance ?: return null
        val data = inflateSurveyDataToCsv(survey)
        val csvFile = exportFileFromSurvey(instance)

        csvFile?.let {
            if (!it.parentFile.exists()) {
                it.parentFile?.mkdirs()
            }
            it.outputStream().bufferedWriter().use {
                it.write(data)
            }
        }

        updateSurveyInfoDb(survey)

        return csvFile?.absolutePath ?: ""
    }


    private fun inflateSurveyDataToCsv(survey: Survey): String {
        return survey.controls.joinToString(",") {
            val name = it.dataName.replaceFirst("/data/", "").let {
                if (it.endsWith(":label")) {
                    it.substring(0, it.length - ":label".length)
                } else {
                    it
                }
            }
            "$name,${it.answer() ?: ""}"
        }
    }

    fun exportAllSurveys(): String? {
        val time = DateTime().toString("yyyy-MM-dd HH:mm:ss")

        loader.listAllInstances().forEach { entry ->
            val surveyName = entry.first
            val instance = entry.second

            val output = File(File(app.appFilesDir, "csv_export"), "export_${surveyName}_$time.csv")
            if (!output.parentFile.exists()) {
                output.parentFile.mkdirs()
            }

            FileOutputStream(output, true).bufferedWriter().use { bufferedWriter: BufferedWriter ->
                try {
                    loadSurveyFromName(surveyName, instance)?.let { survey ->
                        val data = inflateSurveyDataToCsv(survey)
                        bufferedWriter.write(data)
                        bufferedWriter.newLine()
                    }
                } catch (e: Exception) {
                    Timber.e(e)
                    Timber.d("skipping entry for file %s", output.absolutePath)
                }

            }
        }

        return File(app.appFilesDir, "export_csv").absolutePath
    }

    fun deleteSurveys(surveys: List<SurveyResultInfo>) = try {

        surveys.forEach {
            try {
                loadSurveyFromName(it.name, it.instance)?.controls?.forEach {
                    (it as? MeasurementControl)?.let(::deleteMeasurement)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            deleteSurveyInstance(it.instance)
            SurveyResultInfoDb.of(app).db.surveyInfoDao().deleteSurveyInfo(it)
        }
        true
    } catch (e: Exception) {
        Timber.e(e)
        false
    }

    fun uploadSurvey(survey: Survey) {
        saveSurvey(survey)
        val instance = survey.instance ?: return

        val measurements = survey.controls.mapNotNull { control ->
            if (control !is MeasurementControl) {
                return@mapNotNull null
            }

            control.answer()?.let {
                val file = try {
                    JSONObject(it).optString("filename", null)?.let {
                        MeasurementLoader.of(app).resolveMeasurementFile(control.id, it)
                    } ?: JSONObject(it).optJSONObject("meta")?.optString("filename", null)?.let {
                        MeasurementLoader.of(app).resolveMeasurementFile(control.id, it)
                    }
                } catch (e: Exception) {
                    return@mapNotNull null
                } ?: return@mapNotNull null

                return@mapNotNull MultipartBody.Part.createFormData("files", file.name, RequestBody.create(MediaType.parse("text/json"), file))
            }
            null
        }

        val surveyPart = MultipartBody.Part.createFormData("files", "survey.xml", RequestBody.create(MediaType.parse("text/json"), instance))
        val override = MultipartBody.Part.createFormData("override", "true")


        var token: String? = null

        FirebaseAuth.getInstance()?.currentUser?.getIdToken(false)?.let {
            try {
                token = Tasks.await(it, 10, TimeUnit.SECONDS).token
            } catch (e: Exception) {
                Timber.e(e)
            }
        }

        val organizationId = MultipartBody.Part.createFormData("organizationId", app.getCurrentContributionOrg()?.id
                ?: app.defaultOrganizationId)

        uploadSurvey(token, surveyPart, override, organizationId, measurements, survey)
    }

    private fun uploadSurvey(token: String?, surveyPart: MultipartBody.Part, override: MultipartBody.Part, organizationId: MultipartBody.Part, measurements: List<MultipartBody.Part>, survey: Survey) {
        val res = try {
            token?.let {
                Timber.d("uploading with token")
                app.api.uploadSurveyResult(it, surveyPart, override, organizationId, measurements).execute()
            }
                    ?: app.api.uploadSurveyResult(surveyPart, override, organizationId, measurements).execute()
        } catch (e: Exception) {
            Timber.e(e)
            Crashlytics.setString("survey", survey.formId)
            Crashlytics.log(e.message)
            Crashlytics.logException(IllegalStateException("Unable to upload survey"))
            throw e
        }

        if (!res.isSuccessful) {
            Timber.e("error from server: %s".format(res.errorBody()?.string() ?: "<no error body>"))
            Crashlytics.setString("survey", survey.formId)
            Crashlytics.log(res.errorBody()?.string() ?: "<no error body>")
            Crashlytics.logException(IllegalStateException("Unable to upload survey"))
            return
        } else {
            try {
                logSuccessfulUpload(survey.formId)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }

        survey.uploaded = true
        survey.organizationId = app.getCurrentContributionOrg()?.id ?: ""

        saveSurvey(survey)
        updateSurveyInfoDb(survey)

        Timber.d("success: %s", res.body()?.string() ?: "<no body>")
    }


    fun updateSurveyInfoDb(survey: Survey) {

        val instance = survey.instance ?: return Unit.apply {
            Timber.e("no instance provided for survey %s", survey.name)
        }


        val info = populateSurveyResultInfo(survey, survey.formId, instance)

        SurveyResultInfoDb.of(app).db.surveyInfoDao().insertSurveyInfo(info)
    }

    fun deleteSurveyInfoFromDb(survey: Survey) {
        survey.instance?.let {
            SurveyResultInfoDb.of(app).db.surveyInfoDao().delete(it.absolutePath)
        }
    }


}