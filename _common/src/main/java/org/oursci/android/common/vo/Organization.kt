package org.oursci.android.common.vo

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json


/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 09.05.18.
 */

@Entity(tableName = "user_organizations")
data class Organization(

        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "id")
        @Json(name = "id")
        val id: Long,

        @ColumnInfo(name = "organizationId")
        @Json(name = "organizationId")
        val organizationId: String,

        @ColumnInfo(name = "name")
        @Json(name = "name")
        val name: String,

        @ColumnInfo(name = "description")
        @Json(name = "description")
        val description: String,

        @ColumnInfo(name = "created")
        @Json(name = "date")
        val created: Long,

        @ColumnInfo(name = "avatarUrl")
        @Json(name = "picture")
        val avatarUrl: String?,

        @ColumnInfo(name = "archived")
        @Json(name = "archived")
        val archived: Boolean
)