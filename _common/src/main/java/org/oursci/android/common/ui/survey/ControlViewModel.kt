package org.oursci.android.common.ui.survey

import org.oursci.android.common.BaseApplication
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.ui.BaseViewModel
import org.oursci.android.common.ui.controls.BaseControl
import timber.log.Timber
import kotlin.concurrent.thread

/**
 * Created by Manuel Di Cerbo on 22.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
abstract class ControlViewModel<C : BaseControl<out Any>>(app: BaseApplication) : BaseViewModel(app) {

    val loadControlLiveData = ConsumableLiveData<C>()

    val lock = Object()
    public var control: C? = null

    open fun loadControl(idx: Int, cls: Class<C>) {
        val v = SurveyManager.of(app).currentSurvey?.controls?.get(idx)

        if (v == null) {
            Timber.e("item not found at index %d", idx)
            return
        }

        Timber.d("checking if class is assignable of instance class: %s %s", cls.name, v::class.java)
        if (v::class.java.isAssignableFrom(cls)) {
            @Suppress("UNCHECKED_CAST")
            control = v as? C
            loadControlLiveData.postValue(v as C)
            return
        }

        Timber.e("class is not assignable of instance: %s %s", cls.name, v::class.java)

    }

    fun hasMoreQuestions(currentIndex: Int): Boolean {
        synchronized(lock) {
            SurveyManager.of(app).currentSurvey?.controls?.let {
                return currentIndex < it.size - 1
            }
        }

        return false
    }

    fun answer(control: BaseControl<out Any>) {


    }

    fun saveSurvey() {
        thread {
            try {
                SurveyManager.of(app).saveCurrentSurvey()
            } catch (e: Exception) {
                Timber.e("error saving survey")
                Timber.e(e)
            }
        }
    }

    fun surveyUploaded() =
            SurveyManager.of(app).currentSurvey?.uploaded ?: false

}