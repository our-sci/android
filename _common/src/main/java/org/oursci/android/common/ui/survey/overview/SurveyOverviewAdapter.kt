package org.oursci.android.common.ui.survey.overview

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import org.oursci.android.common.R
import org.oursci.android.common.databinding.ItemQuestionBinding
import org.oursci.android.common.vo.survey.SurveyOverviewItem

/**
 * Created by Manuel Di Cerbo on 18.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class SurveyOverviewAdapter(
        private val onClick: (SurveyOverviewItem) -> Unit
) : RecyclerView.Adapter<SurveyOverviewAdapter.ViewHolder>() {

    var items: ArrayList<SurveyOverviewItem> = ArrayList()

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.binding.root.tag = position

        items.getOrNull(position)?.let {
            holder.binding.item = it
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemQuestionBinding = ItemQuestionBinding.inflate(
                LayoutInflater.from(parent.context), parent, false)

        binding.root.setOnClickListener { view ->
            val idx = view.tag as Int?

            idx?.let {
                items.getOrNull(it)?.let { onClick(it) }
            }
        }

        return ViewHolder(binding)
    }

    class ViewHolder(val binding: ItemQuestionBinding) : RecyclerView.ViewHolder(
            binding.root)
}