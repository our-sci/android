package org.oursci.android.common.ui

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.oursci.android.common.R
import org.oursci.android.common.databinding.ItemGenericBinding

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 12.10.17.
 */
class GenericItemAdapter<T>(
        var produceDisplay: (T) -> GenericItemDisplay,
        private val onClick: (T, View) -> Unit,
        private val onLongClick: ((T, View) -> Unit)? = null
) : RecyclerView.Adapter<GenericItemAdapter.ViewHolder>() {

    var items: List<T> = listOf()

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.binding.root.tag = position

        items.getOrNull(position)?.let {
            holder.binding.item = produceDisplay(it)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemGenericBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.item_generic,
                parent, false)

        binding.root.setOnClickListener { view ->
            val idx = view.tag as Int?

            idx?.let {
                items.getOrNull(it)?.let {
                    onClick(it, binding.root)
                }
            }

        }

        binding.root.setOnLongClickListener { view ->
            if (onLongClick == null) {
                return@setOnLongClickListener false
            }

            val idx = view.tag as Int?


            idx?.let {
                items.getOrNull(it)?.let {
                    onLongClick.invoke(it, binding.root)
                    notifyItemChanged(idx)
                }
            }
            true
        }

        return ViewHolder(binding)
    }

    class ViewHolder(val binding: ItemGenericBinding) : RecyclerView.ViewHolder(
            binding.root)

    fun notifyItemChanged(item: T) {
        val idx = items.indexOf(item)
        if (idx >= 0) {
            notifyItemChanged(idx)
        }
    }
}