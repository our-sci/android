package org.oursci.android.common.ui.controls.measurement

import androidx.annotation.UiThread
import android.text.Spanned
import android.view.View
import org.oursci.android.common.html
import org.oursci.android.common.timeAgo

/**
 * Created by Manuel Di Cerbo on 29.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class MeasurementDisplay(
        val control: MeasurementControl, val actionName: String, val created: Long?) {

    enum class State {
        IDLE,
        MEASUREMENT,
        DEBUG,
        CONNECTING,
        CONNECTING_FAILED
    }

    var state = State.IDLE
        @UiThread
        set(value) {
            field = value
        }

    var progressText: String = ""
    var progress: Int = 0

    fun visibilityStartButton() = when (state) {
        MeasurementDisplay.State.IDLE -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityProgressBar() = when (state) {
        MeasurementDisplay.State.MEASUREMENT -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityText() = when (state) {
        MeasurementDisplay.State.DEBUG -> View.GONE
        else -> View.VISIBLE
    }

    fun visibilityWebView() = View.GONE

    fun visibilityDebug() = when (state) {
        MeasurementDisplay.State.DEBUG -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityConnecting() = when (state) {
        State.CONNECTING -> View.VISIBLE
        else -> View.GONE
    }

    fun visibilityConnectingFailed() = when (state) {
        State.CONNECTING_FAILED -> View.VISIBLE
        else -> View.GONE
    }

    fun textTitle(): String {
        return control.label + ""
    }

    fun hint(): Spanned = html((created?.let {
        "<small>updated ${created.timeAgo()}</small><br>"
    } ?: "") + control.hint)

}