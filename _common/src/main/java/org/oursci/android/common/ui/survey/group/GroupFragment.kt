package org.oursci.android.common.ui.survey.group

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.oursci.android.common.R
import org.oursci.android.common.app
import org.oursci.android.common.databinding.ControlGroupBinding
import org.oursci.android.common.databinding.LayoutControlFragmentBinding
import org.oursci.android.common.html
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.controls.geolocation.GeoLocationControl
import org.oursci.android.common.ui.controls.measurement.MeasurementControl
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceControl
import org.oursci.android.common.ui.controls.text.TextControl
import org.oursci.android.common.vo.survey.BaseAnswer


/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 03.05.18.
 */
class GroupFragment : BaseFragment() {

    var idx: Int = -1

    private lateinit var binding: LayoutControlFragmentBinding
    private lateinit var controlBinding: ControlGroupBinding


    val viewModel by lazyViewModel<GroupViewModel>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        idx = arguments?.getInt("idx", -1) ?: -1
        binding = LayoutControlFragmentBinding.inflate(inflater, container, false)

        binding.btRedo.visibility = View.GONE
        binding.btSkip.visibility = View.VISIBLE
        binding.btNext.visibility = View.VISIBLE

        binding.btNext.setOnClickListener {
            app().navigationManager.moveToNextSurveyQuestion(idx)
        }

        binding.btSkip.setOnClickListener {
            app().navigationManager.movePastGroup(idx)
        }

        binding.viewStubControl.viewStub?.layoutResource = R.layout.control_group
        binding.viewStubControl.viewStub?.setOnInflateListener { _, inflated ->
            controlBinding = ControlGroupBinding.bind(inflated)
            SurveyManager.of(app()).currentSurvey?.let { survey ->
                survey.controls[idx].let {
                    controlBinding.tvName.text = "Group"
                    controlBinding.tvDescription.text = it.group?.description ?: html("<i>&ltNo Description for Group&gt;</i>")

                    it.group?.let grp@{ grp ->
                        if (!grp.anon) return@grp

                        binding.swAnon.visibility = View.VISIBLE
                        val defaultAnon = app().rememberAnon(grp.id) ?: true
                        val useDefault = survey.controls.filter {
                            it.group?.id == grp.id
                        }.any {
                            it.answer().isNullOrEmpty()
                        }

                        if (useDefault) {
                            setAnonForGroup(defaultAnon)
                        }

                        binding.swAnon.isChecked = currentAnon()
                        binding.swAnon.setOnCheckedChangeListener { _, isChecked ->
                            setAnonForGroup(isChecked)
                        }
                    }
                }

                controlBinding.invalidateAll()
            }
        }
        binding.viewStubControl.viewStub?.inflate()



        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity.allowOverview(true)
        activity.unlockMenus()
    }

    private fun setAnonForGroup(anon: Boolean) {
        SurveyManager.of(app()).currentSurvey?.let { survey ->
            val currentGrp = survey.controls[idx].group ?: return
            if (!currentGrp.anon) return

            survey.controls.filter {
                currentGrp == it.group
            }.forEach {
                (it.answer as? BaseAnswer<*>)?.anon = anon
            }

            app().setRememberAnon(currentGrp.id, anon)
            viewModel.saveSurvey()
        }
    }

    private fun currentAnon() = SurveyManager.of(app()).currentSurvey?.let { survey ->
        val currentGrp = survey.controls[idx].group ?: return false

        if (!currentGrp.anon) return false

        survey.controls.filter {
            currentGrp == it.group
        }.all {
            (it.answer as? BaseAnswer<*>)?.anon ?: false
        }
    } ?: false
}