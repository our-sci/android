package org.oursci.android.common.ui

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 14.08.17.
 */
interface IBackPressable {
    fun onBackPressed(): Boolean
}