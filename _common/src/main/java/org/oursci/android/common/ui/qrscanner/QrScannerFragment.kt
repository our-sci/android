package org.oursci.android.common.ui.qrscanner

import androidx.fragment.app.Fragment
import me.dm7.barcodescanner.zbar.ZBarScannerView
import android.os.Bundle
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import me.dm7.barcodescanner.zbar.Result
import org.oursci.android.common.app
import org.oursci.android.common.ui.BaseFragment


/**
 * Created by Manuel Di Cerbo on 05.11.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class QrScannerFragment : BaseFragment() {

    private var mScannerView: ZBarScannerView? = null
    private lateinit var frame: FrameLayout
    private var toQrSearch: Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        activity.supportActionBar?.title = "Scanner"
        activity.supportActionBar?.subtitle = "Scan QR or Barcode"
        activity.allowOverview(false)

        arguments?.let {
            toQrSearch = it.getBoolean("to_qr_search", false)
        }

        frame.postDelayed({
            getActivity()?.let {
                mScannerView = ZBarScannerView(activity)
                frame.addView(mScannerView)

                mScannerView?.setResultHandler(resultHandler)
                mScannerView?.startCamera()
            }
        }, 350)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        frame = FrameLayout(context)
        return frame
    }

    private val viewModel by lazyViewModel<QrScannerViewModel>()

    private val resultHandler: ZBarScannerView.ResultHandler = ZBarScannerView.ResultHandler { rawResult ->
        Toast.makeText(activity, "Contents = " + rawResult.contents +
                ", Format = " + rawResult.barcodeFormat.name, Toast.LENGTH_SHORT).show()
        viewModel.mCurrentQrValue = rawResult.contents

        if(toQrSearch) {
            app().navigationManager.navigateToSurveyResultsList(false)
        } else {
            app().navigationManager.pop()
        }
    }

    private val resultHandlerCopy = resultHandler


    override fun onPause() {
        super.onPause()
        mScannerView?.stopCamera()
    }

}