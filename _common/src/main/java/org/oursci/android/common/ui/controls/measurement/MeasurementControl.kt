package org.oursci.android.common.ui.controls.measurement

import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.vo.survey.BaseAnswer
import org.oursci.android.common.vo.survey.Group
import org.oursci.android.common.vo.survey.geolocation.GeoLocationAnswer
import org.oursci.android.common.vo.survey.measurement.MeasurementAnswer

/**
 * Created by Manuel Di Cerbo on 29.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class MeasurementControl(
        val id: String,
        override var answer: MeasurementAnswer = MeasurementAnswer(),
        override val hint: String = "",
        override val defaultValue: String = "",
        override val label: String = "",
        override val dataName: String = "",
        override val readOnly: Boolean = false,
        override val required: Boolean = false,
        override val requiredText: String = "",
        val numElements: Int = 0,
        override val group: Group? = null) : BaseControl<MeasurementAnswer>() {

    override fun answer(): String? {
        return answer.answer
    }


    companion object {
        enum class Status(val state: String) {
            SUCCESS("success"),
            ERROR("error"),
            WARNING("warning"),
            PENDING("pending");

            companion object {
                fun from(state: String) = Status.values().find { state.equals(it.state, true) }
                        ?: PENDING
            }

        }

        const val STATE_SUCCESS = "success"
    }


}