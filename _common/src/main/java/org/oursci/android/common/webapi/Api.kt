package org.oursci.android.common.webapi

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import org.oursci.android.common.vo.Organization
import retrofit2.Call
import retrofit2.http.*

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 16.10.17.
 */
interface Api {



    @GET("survey/find")
    fun findSurveys(@Query("search") search: String = "", @Query("size") size: Long = 1000): Call<FindResponse>

    @GET("survey/find")
    fun findSurveysByOrganization(@Query("organization") organization: String, @Query("search") search: String = "", @Query("size") size: Long = 1000): Call<FindResponse>

    @GET("survey/download/listing/by-form-id/{formId}")
    fun fetchSurveyXml(@Path("formId") formId: String): Call<Array<String>>

    @GET("survey/by-form-id/{formId}")
    fun getSurveyInformation(@Path("formId") formId: String): Call<SurveyInformation>

    @GET("script/by-script-id/{scriptId}")
    fun getScriptInformation(@Path("scriptId") scriptId: String): Call<ScriptInformation>

    @GET("organization/by-user")
    fun fetchOrganizations(@Header("X-Authorization-Firebase") authorization: String): Call<Array<Organization>>


    @GET("https://app.our-sci.net/api/tpcredential/self/list")
    fun fetchTpCredentials(@Header("X-Authorization-Firebase") authorization: String): Call<Array<TpCredentials>>

    @GET("organization/id/{id}")
    fun fetchFavoritesOfOrganization(@Path("id") organizationId: String): Call<ResponseBody>

    @Streaming
    @GET("script/download/zip/by-script-id/{scriptId}")
    fun fetchScriptZip(@Path("scriptId") scriptId: String): Call<ResponseBody>

    @POST("signup")
    fun signUp(@Header("firebaseToken") token: String): Call<ResponseBody>

    @Multipart
    @POST("survey/result/create")
    fun uploadSurveyResult(@Header("X-Authorization-Firebase") authorization: String,
                           @Part survey: MultipartBody.Part,
                           @Part override: MultipartBody.Part,
                           @Part organizationId: MultipartBody.Part,
                           @Part results: List<MultipartBody.Part>): Call<ResponseBody>

    @Multipart
    @POST("survey/result/create")
    fun uploadSurveyResult(@Part survey: MultipartBody.Part,
                           @Part override: MultipartBody.Part,
                           @Part organizationId: MultipartBody.Part,
                           @Part results: List<MultipartBody.Part>): Call<ResponseBody>


    data class FindResponse(
            val content: List<SurveyBody>,
            val first: Boolean,
            val last: Boolean,
            val number: Long,
            val numberOfElements: Long,
            val size: Long,
            val totalElements: Long,
            val totalPages: Long

    )

    data class ScriptInformation(
            val id: String = "",
            val scriptId: String = "",
            val name: String = "",
            val description: String = "",
            val version: String = "",
            val picture: String?,
            val date: Long = -1,
            val user: UserBody = UserBody(-1, null, null, ""),
            val archived: Boolean = false
    )


    data class SurveyInformation(
            val id: String = "",
            val formTitle: String? = "",
            val description: String? = "",
            val formId: String = "",
            val formFile: String? = null,
            val date: Long = -1,
            val user: UserBody = UserBody(-1, null, null, ""),
            val archived: Boolean = false
    )

    @Entity(tableName = "online_surveys")
    data class SurveyBody(

            @PrimaryKey(autoGenerate = false)
            val id: Long,

            val formTitle: String?,
            val description: String?,
            val date: Long,
            val formId: String,
            val formFile: String,
            val formVersion: String?,

            @Embedded(prefix = "user_")
            val user: UserBody?,

            val picture: String?,
            var archived: Boolean
    )

    data class UserBody(
            val id: Long,
            val displayName: String?,
            val picture: String?,
            val username: String
    )


    data class TpCredentials (
        val id : Long,
        val user : UserBody,
        val url : String,
        val username : String,
        val password : String,
        val token : String,
        val parameters : String,
        val note : String,
        val type : String
    )


}