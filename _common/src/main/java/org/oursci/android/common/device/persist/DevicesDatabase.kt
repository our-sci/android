package org.oursci.android.common.device.persist

import androidx.room.Database
import androidx.room.RoomDatabase

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 13.02.19.
 */

@Database(entities = [KnownBluetooth::class], version = 1)
abstract class DevicesDatabase : RoomDatabase() {
    abstract fun knownBluetoothDao(): KnownBluetoothDao
}