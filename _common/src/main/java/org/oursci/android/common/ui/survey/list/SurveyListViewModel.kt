package org.oursci.android.common.ui.survey.list

import org.oursci.android.common.BaseApplication
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.survey.SurveyDatabase
import org.oursci.android.common.survey.SurveyDb
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.ui.BaseViewModel
import org.oursci.android.common.vo.survey.Survey
import org.oursci.android.common.vo.survey.SurveyListItem
import org.oursci.android.common.webapi.Api
import timber.log.Timber
import kotlin.concurrent.thread

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 20.09.17.
 */
class SurveyListViewModel(app: BaseApplication) : BaseViewModel(app) {

    val loadSurveyLiveData = ConsumableLiveData<Array<Api.SurveyBody>>()

    fun loadSurveys() {
        thread {
            try {

                // TODO we should probably sync the db with the forms that are actually on the device
                val baseList = SurveyManager.of(app).loadSurveys().apply {
                    reverse()
                }


                val online = SurveyDb.of(app).db.surveyInfoDao().getAllSurveys().filter { surveyInDb ->
                    baseList.any {
                        it.surveyId == surveyInDb.formId
                    }
                }

                val (archived, current) = online.reversed().partition {
                    it.archived
                }

                // archived.forEach { it.archived = true }

                loadSurveyLiveData.postValue(listOf(
                        archived.sortedByDescending {
                            it.date
                        },
                        current.sortedByDescending {
                            it.date
                        }
                ).flatten().toTypedArray())
            } catch (e: Exception) {
                Timber.e(e)
                loadSurveyLiveData.postValue(null)
            }
        }
    }

}