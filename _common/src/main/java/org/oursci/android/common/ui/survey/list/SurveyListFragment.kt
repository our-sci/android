package org.oursci.android.common.ui.survey.list

import android.Manifest
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.FragmentSurveyListBinding
import org.oursci.android.common.timeAgo
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.GenericItemAdapter
import org.oursci.android.common.ui.GenericItemDisplay
import org.oursci.android.common.vo.survey.Survey
import org.oursci.android.common.vo.survey.SurveyListItem
import org.oursci.android.common.webapi.Api
import timber.log.Timber
import java.lang.ref.WeakReference

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 20.09.17.
 */
class SurveyListFragment : BaseFragment() {

    var sharedView: WeakReference<View>? = null
    var sharedItem: Api.SurveyBody? = null

    val adapter = GenericItemAdapter<Api.SurveyBody>({ item: Api.SurveyBody ->
        GenericItemDisplay(
                title = item.formTitle ?: "",
                description = "%s%s".format(item.description?.let {
                    when {
                        it.isNotEmpty() -> "$it<br>"
                        else -> it
                    }
                } ?: "", "<small>updated ${item.date.timeAgo()}</small>"),
                /*iconRight = surveyBody.second ,*/
                largeIconRight = true)
    }, { item: Api.SurveyBody, view ->
        Timber.d("item with title %s has been pressed", item.formId)
        this.sharedView = WeakReference(view)
        this.sharedItem = item

        app().navigationManager.loadSurvey(view, this.sharedItem)
    })

    /*
    val adapter = SurveyListAdapter { sharedView, surveyListItem ->
        Timber.d("item with title %s has been pressed", surveyListItem.title)
        this.sharedView = WeakReference(sharedView)
        this.sharedItem = surveyListItem

        app().navigationManager.loadSurvey(sharedView, this.sharedItem)
    }
    */

    private val resultObserver = ConsumableObserver<Array<Api.SurveyBody>> { items, consumed ->

        if (consumed) {
            return@ConsumableObserver
        }

        if (items == null) {
            Timber.e("items are null")
            activity.requestPermissions(listOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            ))
            return@ConsumableObserver
        }

        Timber.d("received items, size %d", items.size)
        adapter.items = items.asList()
        adapter.notifyDataSetChanged()

        binding.progressLoading.visibility = View.GONE
        binding.recyclerView.visibility = View.VISIBLE
    }

    lateinit var binding: FragmentSurveyListBinding
    private val viewModel by lazyViewModel<SurveyListViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity.lockMenus()
        activity.allowOverview(false)

        viewModel.loadSurveyLiveData.observe(this, resultObserver)

        viewModel.loadSurveys()

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSurveyListBinding.inflate(inflater, container, false)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        return binding.root
    }
}