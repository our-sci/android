package org.oursci.android.common.measurement

import java.io.File

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 12.10.17.
 */
class MeasurementResultInfo(
        val measurementScript: MeasurementScript,
        val file: File,
        val description: String
)