package org.oursci.android.common.ui.controls.multiplechoice

import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.vo.survey.BaseAnswer
import org.oursci.android.common.vo.survey.Group
import org.oursci.android.common.vo.survey.multiplechoice.MultipleChoiceAnswer

/**
 * Created by Manuel Di Cerbo on 22.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
data class MultipleChoiceControl(
        override var answer: MultipleChoiceAnswer = MultipleChoiceAnswer(),
        val options: ArrayList<Option> = arrayListOf(),
        val multiSelect: Boolean = false,
        override val hint: String = "",
        override val defaultValue: String = "",
        override val label: String = "",
        override val dataName: String = "",
        override val readOnly: Boolean = false,
        override val required: Boolean = false,
        override val requiredText: String = "",
        override val group: Group? = null) : BaseControl<MultipleChoiceAnswer>() {

    data class Option(val value: String, val text: String = "", val url: String = "", val type: Type,
                      var selected: Boolean = false) {
        enum class Type {
            TEXT,
            IMAGE,
            AUDIO,
            VIDEO,
            UNKNOWN
        }
    }

    override fun answer(): String? {
        return answer.answer?.joinToString("/") { input ->
            try {
                options.first { it.value == input }.text
            } catch (e: Exception) {
                ""
            }
        }
    }
}

