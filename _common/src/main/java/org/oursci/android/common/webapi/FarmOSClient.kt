package org.oursci.android.common.webapi

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import org.oursci.android.common.farmos.FarmOsPlanting
import retrofit2.Retrofit
import timber.log.Timber
import java.lang.Exception

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 14.03.19.
 */
class FarmOSClient(
        val url: String,
        val user: String,
        val pass: String,
        val debug: Boolean = false
) {
    data class Credentials(val token: String, val cookie: String)
    data class Area(val id: Long, val name: String, val type: String?)
    data class Planting(val id: Long, val name: String, val type: String?)


    private val interceptor = HttpLoggingInterceptor()

    private val client: OkHttpClient
    private val retrofit: Retrofit
    private val svc: FarmOSApi

    init {
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        client = if (debug) {
            OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .followRedirects(false)
                    .build()
        } else {
            OkHttpClient.Builder()
                    .followRedirects(false)
                    .build()
        }


        retrofit = Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .build()

        svc = retrofit.create(FarmOSApi::class.java)
    }

    fun extractLastPage(url: String): Int? {
        val matcher = """page=(\d+)$""".toRegex()
        val (lastPage) = matcher.find(url)?.destructured ?: return null
        return lastPage.toIntOrNull()
    }

    fun authenticate(user: String, pass: String): Credentials? {
        val request = svc.authenticate(user, pass)
        val response = request.execute()

        println(request.request())
        val cookie = response.headers()["Set-Cookie"] ?: return null
        val r = svc.getToken(cookie).execute()
        val token = r.body()?.string() ?: return null

        return Credentials(token, cookie)
    }

    fun fetchAreasVocabId(page: Int, credentials: Credentials): Int? {
        val str = svc.getAreasVocab(credentials.cookie, credentials.token).execute().body()?.string()
                ?: return null

        val arr = JSONObject(str).getJSONArray("list")
        val len = arr.length()
        var id: Int? = null
        for (i in 0 until len) {
            val o = arr.getJSONObject(i)

            if (o.getString("machine_name") != "farm_areas") {
                continue
            }
            id = o.getInt("vid")
            break
        }

        return id
    }

    fun fetchAreas(credentials: Credentials, id: Int, page: Int): JSONObject? {
        val areasRes = svc.getAreas(credentials.cookie, credentials.token, id, page).execute().body()?.string()
                ?: return null
        return JSONObject(areasRes)
    }

    @Throws(Exception::class)
    fun fetchAllAreas(): List<Area>? {

        val areaList = mutableListOf<JSONObject>()

        val credentials = authenticate(user, pass) ?: return null
        val areasVocabId = fetchAreasVocabId(0, credentials) ?: return null

        if (debug) Timber.d("area id: $areasVocabId")

        val areasJson = fetchAreas(credentials, areasVocabId, 0) ?: return null
        areasJson.getJSONArray("list").let {
            val len = it.length()
            for (i in 0 until len) {
                areaList += it.getJSONObject(i)
            }
        }


        val lastPage = areasJson.getString("last")?.let(::extractLastPage) ?: return null

        if (debug) Timber.d("last page is $lastPage")
        if (debug) Timber.d(areasJson.toString(2))

        for (i in 1..lastPage) {
            fetchAreas(credentials, areasVocabId, i)?.getJSONArray("list")?.let {
                val len = it.length()
                for (j in 0 until len) {
                    areaList += it.getJSONObject(j)
                }
            }
        }

        if (debug) Timber.d(areaList.joinToString("\n") { it.getString("name") })
        return areaList.map { Area(it.getLong("tid"), it.getString("name"), it.optString("area_type")) }
    }

    fun fetchPlantings(credentials: Credentials, page: Int): JSONObject? {
        val areasRes = svc.getPlantings(cookie = credentials.cookie, token = credentials.token, page = page).execute().body()?.string()
                ?: return null
        return JSONObject(areasRes)
    }

    @Throws(Exception::class)
    fun fetchAllPlantings(): List<Planting>? {

        val plantingList = mutableListOf<JSONObject>()

        val credentials = authenticate(user, pass) ?: return null

        val plantingsJson = fetchPlantings(credentials, 0) ?: return null
        plantingsJson.getJSONArray("list").let {
            val len = it.length()
            for (i in 0 until len) {
                plantingList += it.getJSONObject(i)
            }
        }


        val lastPage = plantingsJson.getString("last")?.let(::extractLastPage) ?: return null

        if (debug) Timber.d("last page is $lastPage")
        if (debug) Timber.d(plantingsJson.toString(2))

        for (i in 1..lastPage) {
            fetchPlantings(credentials, i)?.getJSONArray("list")?.let { list ->
                val len = list.length()
                for (j in 0 until len) {
                    plantingList += list.getJSONObject(j)
                }
            }
        }

        if (debug) Timber.d(plantingList.joinToString("\n") { it.getString("name") })
        return plantingList.map { Planting(it.getLong("id"), it.getString("name"), it.optString("type")) }
    }

}