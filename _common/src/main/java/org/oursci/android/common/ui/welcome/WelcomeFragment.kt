package org.oursci.android.common.ui.welcome

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import org.oursci.android.common.R
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.FragmentGenericListBinding
import org.oursci.android.common.organization.OrganizationDb
import org.oursci.android.common.server.WebServer
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.survey.created
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.IBackPressable
import org.oursci.android.common.ui.organization.OrganizationViewModel
import org.oursci.android.common.ui.survey.results.SurveyResultListViewModel
import org.oursci.android.common.vo.Organization
import org.oursci.android.common.vo.survey.SurveyResultInfo
import kotlin.concurrent.thread

/**
 * Created by Manuel Di Cerbo on 01.05.18.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */

class WelcomeFragment : BaseFragment(), IBackPressable {
    lateinit var binding: FragmentGenericListBinding
    lateinit var webserver: WebServer

    val adapter = WelcomeAdapter()

    private val surveyResultListViewModel by lazyViewModel<SurveyResultListViewModel>()

    private val resultObserver = ConsumableObserver<List<SurveyResultInfo>> { items, consumed ->

        if (consumed || surveyResultListViewModel.loading.get()) {
            return@ConsumableObserver
        }

        /*
        adapter.items.add(WelcomeAdapter.WelcomeItem(
                title = "Location Picker",
                leftAction = "Pick Location" to {
                    app().navigationManager.standaloneGeolocationPicker()
                }
        ))
        */

        adapter.items.add(WelcomeAdapter.WelcomeItem(
                title = "GPS Keep-Alive",
                leftAction = "Enable" to {
                    if (!app().gpsLock(true)) {
                        activity.showPermissionDialog("" +
                                "Permission Required",
                                "For using this feature, permissions are required",
                                {
                                    app().gpsLock(true)
                                })
                    }
                },
                middleAction = "Disable" to {
                    app().gpsLock(false).run {}
                },
                rightAction = "Info" to {
                    val dialog = AlertDialog.Builder(activity, activity.alertDialogTheme)
                            .setTitle("Keep GPS alive")
                            .setMessage("Keep your GPS signal alive in order to not lose a location fix when taking surveys.\nThis will significantly speed up location fixes in between survey questions.")
                            .create()
                    dialog.show()
                }
        ))

        adapter.items.addAll(app().welcomeItems)


        val toShow = items?.sortedByDescending { it.instance.lastModified() } ?: emptyList()

        if (toShow.isEmpty()) {
            adapter.items.add(WelcomeAdapter.WelcomeItem(
                    title = "Get Started",
                    content = "Take a first survey to get started",
                    leftAction = "Take first Survey" to {
                        app().navigationManager.navigateToSurveyBrowser()
                    }
            ))
        } else {
            adapter.items.add(WelcomeAdapter.WelcomeItem(
                    title = "Making Progress!",
                    content = "%d Surveys started<br>".format(toShow.size) +
                            "%d Surveys uploaded<br>".format(toShow.count { it.state == SurveyResultInfo.State.UPLOADED }) +
                            "%s most often used<br>".format(toShow.groupBy { it.name }.maxBy { it.value.size }?.value?.first()?.title)
            ))
        }


        toShow.take(3).forEach {

            val desc = if (it.organizationName.isNotEmpty()) {
                "%s<br><small>contributed to</small> %s".format(it.description, it.organizationName)
            } else it.description
            adapter.items.add(WelcomeAdapter.WelcomeItem(
                    title = it.title,
                    subtitle = it.created(),
                    content = desc,
                    leftAction = "Continue" to { surveyResultListViewModel.toSurvey(it) },
                    middleAction = "New" to {
                        SurveyManager.of(app()).createSurveyInstance(it.name)
                        app().navigationManager.navigateToSurveyResultsList(false, false)
                        app().navigationManager.startSurvey(false)
                    }
            ))
        }

        if (app().hasLegacyOfflineSurveys) {

            adapter.items.add(WelcomeAdapter.WelcomeItem(
                    title = "Working Offline?",
                    content = "Your Surveys are also available offline. " +
                            "Be sure to run it before you go out to the Field. " +
                            "To have a Survey available offline, fetch it from the online browser first.",
                    leftAction = "Browse Online" to app().navigationManager::navigateToSurveyBrowser,
                    middleAction = "Browse Offline" to app().navigationManager::navigateToSurveyList
            ))
        }


        adapter.items.add(WelcomeAdapter.WelcomeItem(
                title = "Results on the Web",
                subtitle = "Take a look at your results online",
                leftAction = "Go to web application" to {
                    startActivity(Intent(Intent.ACTION_VIEW).apply {
                        data = Uri.parse(app().organizationURL)
                    })
                }
        ))

        adapter.notifyDataSetChanged()


        val relevant = items?.filter { it.state == SurveyResultInfo.State.FINISHED }
        if (relevant != null && relevant.isNotEmpty()) {
            activity.findViewById<TextView>(R.id.badge)?.let { badgeTextView ->
                badgeTextView.text = "${relevant.size}"
                badgeTextView.visibility = View.VISIBLE
            }

            adapter.items.add(0, WelcomeAdapter.WelcomeItem(
                    title = "${relevant.size} Surveys ready to submit",
                    subtitle = "Review and Send",
                    leftAction = "Review" to {
                        app().navigationManager.navigateToSurveyResultsList(false)
                    }))
        }


        reveal()

    }

    val organizationViewModel by lazyViewModel<OrganizationViewModel>()

    private val organizationResultObserver = ConsumableObserver<List<Organization>> { orgs, consumed ->

        if (consumed || orgs == null) {
            return@ConsumableObserver
        }

        if (orgs.isEmpty()) {
            return@ConsumableObserver
        }

        if (app().allowOrganizations) {
            adapter.items.add(0, WelcomeAdapter.WelcomeItem(
                    title = "Organizations",
                    subtitle = if (orgs.size == 1) "You are part of an organization" else "Your are part of %d organizations".format(orgs.size),
                    content = orgs.joinToString("<br>") {
                        it.name
                    } + "<br><br>%s".format(app().getCurrentContributionOrg()?.let {
                        "currently contributing to ${it.name}"
                    } ?: "currently not contributing to any organization"),
                    leftAction = "Contribute to Organization" to {
                        app().navigationManager.profile()
                    }
            ))
        }



        adapter.notifyDataSetChanged()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentGenericListBinding.inflate(inflater, container, false)

        binding.recyclerview.adapter = adapter
        binding.recyclerview.layoutManager = LinearLayoutManager(activity)


        return binding.root
    }

    private fun showBaseItems() {

        binding.recyclerview.visibility = View.GONE
        adapter.items.clear()

        if (activity.requiresPermission()) {
            adapter.items.add(WelcomeAdapter.WelcomeItem(
                    title = "App Permission",
                    content = "In order for the app to work correctly, these permissions are required<br>" +
                            "Camera: To capture QR codes<br>" +
                            "Storage: To save survey data<br>" +
                            "Location: To enable Geolocation data in surveys",
                    leftAction = "Grant Permissions" to {
                        activity.showPermissionDialog("Grant Permissions", "Permissions for surveys", {
                            showBaseItems()
                        })
                    }
            ))
            reveal()
        } else {
            surveyResultListViewModel.loadSurveyResults.observe(this, resultObserver)
            surveyResultListViewModel.loadSurveyResults()
        }

        if (!app().user.loggedIn()) {
            adapter.items.add(WelcomeAdapter.WelcomeItem(
                    title = "Login or Register",
                    subtitle = "Log in to share your data with other users",
                    leftAction = "Login or Sign up" to {
                        activity.login { success ->
                            if (success) {
                                showBaseItems()
                            }
                        }
                    }
            ))
        } else {
            organizationViewModel.loadResult.observe(this, organizationResultObserver)
            organizationViewModel.loadOrganizations()
        }


        binding.root.postDelayed({
            if (surveyResultListViewModel.busy()) {
                binding.tvProgress.text = "Refreshing Database"
                binding.progress.visibility = View.VISIBLE
                binding.tvProgress.visibility = View.VISIBLE

            }
        }, 500)
    }

    fun reveal() {
        binding.recyclerview.visibility = View.VISIBLE
        binding.progress.visibility = View.GONE
        binding.tvProgress.visibility = View.GONE
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity.allowOverview(false)
        activity.unlockMenus()

        showBaseItems()

        when {
            !app().user.loggedIn() -> return
            app().isOrganizationSelected() -> return
            !app().allowOrganizations -> return
            app().getCurrentContributionOrg() != null -> return
            else -> {
                thread {
                    val db = OrganizationDb.of(app()).db.orgainzationDao()
                    val orgs = db.getAllOrganizations()
                    if (orgs.isNotEmpty()) {
                        activity.showSelectOrganizationDialog(orgs)
                    }
                }
            }

        }


    }


    override fun onBackPressed(): Boolean {
        activity.finish()
        return true
    }
}