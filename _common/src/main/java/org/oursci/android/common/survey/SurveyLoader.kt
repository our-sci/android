package org.oursci.android.common.survey

import org.javarosa.core.model.Constants
import org.javarosa.core.model.FormIndex
import org.javarosa.core.model.GroupDef
import org.javarosa.core.model.SelectChoice
import org.javarosa.core.model.data.*
import org.javarosa.core.model.data.helper.Selection
import org.javarosa.core.model.instance.TreeElement
import org.javarosa.core.services.IPropertyManager
import org.javarosa.core.services.properties.IPropertyRules
import org.javarosa.form.api.FormEntryController
import org.javarosa.form.api.FormEntryModel
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import org.oursci.android.common.javarosa.Event
import org.oursci.android.common.javarosa.FormLoader
import org.oursci.android.common.javarosa.JavaRosaInit
import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.ui.controls.external.ExternalControl
import org.oursci.android.common.ui.controls.external.ExternalControlAdapter
import org.oursci.android.common.ui.controls.geolocation.GeoLocationAdapter
import org.oursci.android.common.ui.controls.geolocation.GeoLocationControl
import org.oursci.android.common.ui.controls.measurement.MeasurementAdapter
import org.oursci.android.common.ui.controls.measurement.MeasurementControl
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceAdapter
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceControl
import org.oursci.android.common.ui.controls.text.TextAdapter
import org.oursci.android.common.ui.controls.text.TextControl
import org.oursci.android.common.vo.survey.BaseAnswer
import org.oursci.android.common.vo.survey.Group
import org.oursci.android.common.vo.survey.Survey
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import timber.log.Timber
import java.io.File
import java.io.FileInputStream
import java.io.InputStream

/**
 * Created by Manuel Di Cerbo on 20.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class SurveyLoader(
        private val surveyDirectory: File,
        private val userId: () -> String) {


    private val formLoader = FormLoader()

    init {
        JavaRosaInit.initializeJavaRosa(object : IPropertyManager {
            override fun getProperty(propertyName: String): List<String> {
                Timber.d("getting property: %s", propertyName)
                return ArrayList()
            }

            override fun setProperty(propertyName: String, propertyValue: String) {

            }

            override fun setProperty(propertyName: String, propertyValue: List<String>) {

            }

            override fun getSingularProperty(propertyName: String): String {
                Timber.d("getting singular property: %s", propertyName)
                return ""
            }

            override fun addRules(rules: IPropertyRules) {

            }

            override fun getRules(): List<IPropertyRules> {
                return ArrayList()
            }
        })
    }

    fun initSurveyDirectory() {
        initDirectory(surveyDirectory)
    }

    fun initDirectory(directory: File) {
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                throw IllegalArgumentException("unable to create directory: ${directory.absolutePath}")
            }
        }

        if (!directory.isDirectory) {
            throw IllegalStateException("file is not a directory: ${directory.absolutePath}")
        }
    }

    fun listAllInstances(): List<Pair<String, File>> {
        initSurveyDirectory()
        return (surveyDirectory.list() ?: emptyArray<String>()).flatMap { surveyName ->
            Timber.d("mapping $surveyName")

            val instancesDir = PathSpec(surveyName).instanceDir
            if (!instancesDir.isDirectory) {
                return@flatMap emptyList<Pair<String, File>>()

            }

            instancesDir.list().map {
                surveyName to File(instancesDir, it)
            }
        }
    }

    fun createNewSurvey(formName: String): Survey? {
        val instanceFile = newInstancePath(formName)
        return loadSurvey(formName, instanceFile)
    }

    fun loadSurvey(formName: String, instanceFile: File): Survey? {
        val fec = loadFormByPath(
                PathSpec(formName).formFile.absolutePath,
                instanceFile)
        val res = fromFormEntryController(fec, instanceFile)
        return res.first
    }

    fun loadForm(formName: String): Survey? {

        val fec = loadFormByPath(
                PathSpec(formName).formFile.absolutePath,
                null)
        val res = fromFormEntryController(fec, null)
        return res.first
    }

    fun loadFormByPath(formPath: String, instanceFile: File? = null):
            FormEntryController {

        Timber.d("loading form with path, instance: %s, %s", formPath,
                instanceFile?.absolutePath)

        val formInputStream = File(formPath).inputStream()

        formInputStream.use {

            instanceFile?.let {
                if (!it.exists()) {
                    if (!it.parentFile.exists()) {
                        if (!it.parentFile.mkdirs()) {
                            throw IllegalArgumentException("unable to create parent directory for" +
                                    "instance file ${it.absolutePath}")
                        }
                    }

                    if (!it.parentFile.isDirectory) {
                        throw IllegalArgumentException("parent of instance file is not a directory ${it.absolutePath}")
                    }

                    if (!it.createNewFile()) {
                        throw IllegalArgumentException("unable to create file ${it.absolutePath}")
                    }
                }
            }

            val instanceInputStream: InputStream? = instanceFile?.let {
                FileInputStream(it)
            }

            val fec: FormEntryController
            try {
                fec = if (instanceInputStream != null) {
                    formLoader.loadForm(formInputStream, instanceFile)
                } else {
                    formLoader.loadForm(formInputStream)
                }
            } catch (e: Exception) {
                Timber.e(e)
                throw IllegalStateException("unable to parse form entry controller for file: $formPath")
            }

            Timber.d("form name is %s", fec.model.form.name)
            return fec
        }

    }

    private fun transformAnswer(control: BaseControl<*>): IAnswerData? {
        return when (control) {
            is TextControl -> {
                control.answer.answer?.let { StringData(it) }
            }
            is ExternalControl -> {
                control.answer.answer?.let { StringData(it) }
            }
            is MultipleChoiceControl -> {
                if (control.multiSelect) {
                    control.answer.answer?.let {
                        var idx = 0

                        SelectMultiData(it.map {
                            Selection(SelectChoice(it, it).apply {
                                index = idx++
                            })
                        }.toList())
                    }
                } else {
                    control.answer.answer?.let {
                        control.answer.answer?.let {
                            if (it.isNotEmpty()) {
                                SelectOneData(Selection(it[0]))
                            } else null
                        }
                    }
                }
            }
            is MeasurementControl -> {
                control.answer.answer?.let { StringData(it) }
            }
            is GeoLocationControl -> {
                control.answer.answer?.let {
                    val array = it.split(";").map {
                        it.toDouble()
                    }.toTypedArray().toDoubleArray()
                    GeoPointData(array)
                }
            }
            else -> {
                null
            }
        }
    }


    fun persist(survey: Survey, modified: Boolean) {
        if (survey.instance == null) {
            throw IllegalStateException("no instance supplied for persisting survey")
        }

        survey.controls.forEach { control ->
            Timber.d("control answer: %s --> %s", control.dataName, control.answer())
            val idx = survey.indexMap.entries.first {
                //                Timber.d("data name match: %s, %s",
//                        control.dataName, it.key.dataName)
                control.dataName == it.key.dataName
            }.value

            transformAnswer(control)?.let {
                Timber.d("%s => %s", it, idx)
                val res = survey.fec.answerQuestion(idx, it, true)
                Timber.d("answering question, res: %d", res)
            } ?: survey.fec.answerQuestion(idx, StringData(""), true)
        }

        val fec = survey.fec
        val instanceFile = survey.instance ?: return


        if (modified) {
            survey.modified = DateTime().millis
        }

        val modifiedAnswer = StringData(ISODateTimeFormat.dateTime().print(DateTime(survey.modified)))

        val created = StringData(ISODateTimeFormat.dateTime().print(survey.created))
        val uploaded = StringData(survey.uploaded.toString())
        val uid = StringData(userId())
        val organizationId = StringData(survey.organizationId ?: "")

        Timber.d("uploaded is: %s", uploaded.displayText)

        val root = fec.model.form.instance.root
        val meta = root.getChild("meta", 0)

        val modifiedChild = meta.getChild("modified", 0)
        if (modifiedChild == null) {
            val c = TreeElement("modified", 0)
            c.setAnswer(modifiedAnswer)
            c.isRelevant = true
            meta.addChild(c)
        } else {
            modifiedChild.setAnswer(modifiedAnswer)
        }

        // update in memory modified


        val createdChild = meta.getChild("created", 0)
        if (createdChild == null) {
            val c = TreeElement("created", 0)
            c.setAnswer(created)
            c.isRelevant = true
            meta.addChild(c)
        }


        meta.removeChildren("anonimzye") // first type cleanup
        meta.removeChildren("anonymize")
        val anon = TreeElement("anonymize", 0)
        anon.isRelevant = true

        var mult = 0
        survey.controls.forEach {
            (it.answer as? BaseAnswer<*>)?.let {
                if (!it.anon) {
                    return@forEach
                }
            } ?: return@forEach

            val aChild = TreeElement("id", mult++)
            Timber.d("data name is %s", it.dataName)
            aChild.setAnswer(StringData(it.dataName))
            aChild.isRelevant = true
            anon.addChild(aChild)
        }
        if (anon.hasChildren()) {
            meta.addChild(anon)
        }

        val uploadedChild = meta.getChild("uploaded", 0)
        if (uploadedChild == null) {
            val c = TreeElement("uploaded", 0)
            c.setAnswer(uploaded)
            c.isRelevant = true
            meta.addChild(c)
        } else {
            uploadedChild.setAnswer(uploaded)
        }


        val userId = meta.getChild("userID", 0)
        if (userId == null) {
            val c = TreeElement("userID", 0)
            c.setAnswer(uid)
            c.isRelevant = true
            meta.addChild(c)
        } else {
            userId.setAnswer(uid)
        }

        val org = meta.getChild("organizationId", 0)
        if (org == null) {
            val c = TreeElement("organizationId", 0)
            c.setAnswer(organizationId)
            c.isRelevant = true
            meta.addChild(c)
        } else {
            org.setAnswer(organizationId)
        }


        formLoader.persist(fec, instanceFile)
    }

    fun fromFormEntryController(fec: FormEntryController, instance: File? = null): Pair<Survey,
            Map<BaseControl<out Any>, FormIndex>> {

        val controlIndexMap = hashMapOf<BaseControl<out Any>, FormIndex>()
        val groupIndexMap = hashMapOf<Group, FormIndex>()
        var idx = fec.model.formIndex
        val element = fec.model.form.getChild(idx)

        val questions = arrayListOf<BaseControl<out Any>>()
        val groups = arrayListOf<Group>()

        val root = fec.model.form.instance.root
        val meta = root.getChild("meta", 0)

        val anonIds = instance?.let { AnonReader(it).read() } ?: emptyList()

        Timber.d("anonids: %s", anonIds.joinToString(", "))

        eventLoop@ while (true) {

            val e = Event.fromEvent(fec.model.getEvent(idx)) ?: break

            when (e) {
                Event.EVENT_BEGINNING_OF_FORM -> {

                }
                Event.EVENT_END_OF_FORM -> break@eventLoop
                Event.EVENT_PROMPT_NEW_REPEAT -> {
                }
                Event.EVENT_QUESTION -> {


                    val prompt = fec.model.questionPrompt
                    val dataName = prompt.question.textID

                    Timber.d("data name is: %s", dataName)

                    val anon = anonIds.find { it == dataName }?.let { true } ?: false

                    val currentGroup = (fec.model.captionHierarchy.getOrNull(0)?.formElement as? GroupDef)?.textID?.let { grpId ->
                        Timber.d("question belongs to group: %s", this)
                        groups.find { g -> g.id == grpId }?.let {
                            it.subjects += dataName
                            it
                        }
                    }

                    Timber.d("moving on")

                    when (ControlType.from(prompt.controlType)) {
                        ControlType.UNTYPED -> null
                        ControlType.INPUT -> {
                            when {
                                prompt.dataType == Constants.DATATYPE_GEOPOINT -> GeoLocationAdapter.produce(prompt, currentGroup)
                                ExternalControlAdapter.qualifies(prompt.helpText) -> ExternalControlAdapter.produce(prompt, currentGroup)
                                else -> TextAdapter.produce(prompt, currentGroup)
                            }
                        }
                        ControlType.SELECT_ONE -> MultipleChoiceAdapter.produce(prompt, currentGroup)
                        ControlType.SELECT_MULTI -> MultipleChoiceAdapter.produce(prompt, currentGroup)
                        ControlType.TEXTAREA -> null
                        ControlType.SECRET -> null
                        ControlType.RANGE -> null
                        ControlType.UPLOAD -> null
                        ControlType.SUBMIT -> null
                        ControlType.TRIGGER -> null
                        ControlType.IMAGE_CHOOSE -> null
                        ControlType.LABEL -> null
                        ControlType.AUDIO_CAPTURE -> null
                        ControlType.VIDEO_CAPTURE -> MeasurementAdapter.produce(prompt, currentGroup)
                        ControlType.OSM_CAPTURE -> null
                        ControlType.FILE_CAPTURE -> MeasurementAdapter.produce(prompt, currentGroup)
                    }?.let {
                        it.answer.anon = anon
                        controlIndexMap[it] = idx
                        questions.add(it)
                    }

//                    val drivletId = fec.model.questionPrompt.getSpecialFormQuestionText("drivlet")
//                    fec.answerQuestion(idx, StringData("hello"), true)
//                    println("drivlet is: " + drivletId)
//                    println(fec.model.questionPrompt.helpText)
//                    println(fec.model.questionPrompt.answerText)
//                    println()
                }
                Event.EVENT_GROUP -> {
                    Timber.d("group")
                    (fec.model.form.getChild(idx) as? GroupDef)?.let {
                        val args = fec.model.captionHierarchy[0].longText.split(";")
                        val label = args[0]
                        val anon = args.getOrNull(1)?.let { it == "anon" } ?: false
                        val group = Group(it.textID, label, anon, mutableListOf())
                        groupIndexMap[group] = idx
                        groups += group
                    }
                }
                Event.EVENT_REPEAT -> {
                }
                Event.EVENT_REPEAT_JUNCTURE -> {
                }
                Event.EVENT_UNKNOWN -> {
                }
            }

            idx = fec.model.incrementIndex(idx)
            fec.model.setQuestionIndex(idx)
        }


        val name = try {
            fec.model.formTitle.split(";")[0]
        } catch (e: Exception) {
            fec.model.formTitle
        }

        val description = try {
            fec.model.formTitle.split(";")[1]
        } catch (e: Exception) {
            Timber.d("no description found inside title")
            ""
        }

        val summaryScript = try {
            fec.model.form.submissionProfile.action
        } catch (e: Exception) {
            Timber.d("no description found inside metadata")
            ""
        } ?: ""


        val instanceId = try {
            fec.model.form.instance.root
                    .getChild("meta", 0)
                    .getChild("instanceID", 0).value.displayText
        } catch (e: Exception) {
            Timber.d("no description found inside metadata")
            ""
        } ?: ""

        val timeStart = try {
            fec.model.form.instance.root
                    .getChild("meta", 0)
                    .getChild("timeStart", 0).value.displayText
        } catch (e: Exception) {
            Timber.d("no description found inside metadata")
            ""
        } ?: ""


        var uploaded = ""
        var modified = ""
        var organizationId = ""
        var created = ""

        instance?.let {
            val reader = MetaReader(instance)
            uploaded = reader.read("uploaded", "") ?: ""
            modified = reader.read("modified", "") ?: ""
            organizationId = reader.read("organizationId", "") ?: ""
            created = reader.read("created", "") ?: ""
        }


        val formId = try {
            fec.model.form.mainInstance.root.getAttributeValue("", "id")
        } catch (e: Exception) {
            Timber.e(e)
            Timber.d("error getting formId: %s".format(e.message))
            ""
        } ?: ""


//        val modifiedChild = meta.getChild("created", 0)
//        if (modifiedChild == null) {
//            val c = TreeElement("created", 0)
//            c.setAnswer(StringData(ISODateTimeFormat.dateTime().print(DateTime())))
//            c.isRelevant = true
//            meta.addChild(c)
//        }

        val parser = ISODateTimeFormat.dateTimeParser().withOffsetParsed()


        return Survey(
                controls = questions.toTypedArray(),
                groups = groups.toTypedArray(),
                name = name,
                fec = fec,
                instance = instance,
                description = description,
                instanceID = instanceId,
                formId = formId,
                startTime = timeStart.toLongOrNull() ?: DateTime().millis,
                created = if (created.isEmpty()) DateTime().millis else try {
                    parser.parseDateTime(created).millis
                } catch (e: Exception) {
                    Timber.e(e)
                    DateTime().millis
                },
                modified = if (modified.isEmpty()) DateTime().millis else try {
                    parser.parseDateTime(modified).millis
                } catch (e: Exception) {
                    Timber.e(e)
                    DateTime().millis
                },
                uploaded = uploaded.toBoolean(),
                indexMap = controlIndexMap,
                organizationId = organizationId,
                summaryScript = summaryScript,
                groupIndexMap = groupIndexMap
        ) to controlIndexMap
    }

    fun surveyId(model: FormEntryModel): String = try {
        model.form.mainInstance.root.getAttributeValue("", "id")
    } catch (e: Exception) {
        Timber.e(e)
        Timber.d("error getting formId: %s".format(e.message))
        ""
    } ?: ""

    inner class PathSpec(surveyName: String) {
        val formFile = File(File(surveyDirectory, surveyName), "survey.xml")
        val instanceDir = File(File(surveyDirectory, surveyName), "instances")
    }

    class MetaReader(val file: File) {
        val factory: XmlPullParserFactory = XmlPullParserFactory.newInstance().apply {
            isNamespaceAware = false
        }

        val parser: XmlPullParser = factory.newPullParser()

        fun read(property: String, default: String? = null): String? {
            var result = default

            file.inputStream().use { inputStream ->
                parser.setInput(inputStream, "utf-8")


                var found = false;
                while (parser.next() != XmlPullParser.END_DOCUMENT) {

                    if (parser.eventType != XmlPullParser.START_TAG) {
                        continue
                    }

                    if (parser.name == "meta") {
                        found = true
                        break
                    }
                }

                if (!found) {
                    throw IllegalStateException("unable to find meta tag")
                }


                while (parser.next() != XmlPullParser.END_TAG) {
                    if (parser.eventType != XmlPullParser.START_TAG) {
                        continue
                    }

                    if (parser.name == property) {
                        result = parser.nextText()
                        break
                    }

                    while (parser.next() != XmlPullParser.END_TAG) { // move to next end tag
                    }
                }
            }

            return result
        }
    }

    class AnonReader(val file: File) {

        val factory: XmlPullParserFactory = XmlPullParserFactory.newInstance().apply {
            isNamespaceAware = false
        }

        val parser: XmlPullParser = factory.newPullParser()

        fun read(): List<String>? {
            val result = mutableListOf<String>()

            file.inputStream().use { inputStream ->
                parser.setInput(inputStream, "utf-8")

                var inMeta = false
                var inAnon = false

                while (parser.next() != XmlPullParser.END_DOCUMENT) {


                    if (inAnon && parser.eventType == XmlPullParser.END_TAG) {
                        return result
                    }

                    if (parser.eventType != XmlPullParser.START_TAG) {
                        continue
                    }

                    if (parser.name == "meta") {
                        inMeta = true
                    }

                    if (inMeta && parser.name == "anonymize") {
                        inAnon = true
                    }

                    if (inAnon && parser.name == "id") {
                        result.add(parser.nextText())
                    }
                }

            }

            return result
        }
    }

    fun listAllSurveyFiles(): List<File> {
        return surveyDirectory.list().mapNotNull {
            val file = File(surveyDirectory, it)
            if (!file.isDirectory) {
                return@mapNotNull null
            }

            val pathSpec = PathSpec(it)

            if (!pathSpec.formFile.exists()) {
                return@mapNotNull null
            }

            pathSpec.formFile
        }
    }

    fun listAllSurveyIds(): List<String> {
        return surveyDirectory.list().mapNotNull {
            val file = File(surveyDirectory, it)
            if (!file.isDirectory) {
                return@mapNotNull null
            }

            val pathSpec = PathSpec(it)

            if (!pathSpec.formFile.exists()) {
                return@mapNotNull null
            }

            it
        }
    }

    fun surveyFileFromId(id: String): File? {
        val file = File(surveyDirectory, id)
        if (!file.isDirectory) {
            return null
        }

        val pathSpec = PathSpec(id)

        if (!pathSpec.formFile.exists()) {
            return null
        }

        return pathSpec.formFile
    }

    fun newInstancePath(surveyId: String): File {
        val dateTime = DateTime().toString("yyyy-MM-dd HH:mm:ss")
        val fileName = "instance_$dateTime.xml"
        initSurveyDirectory()
        val dir = PathSpec(surveyId).instanceDir
        if (!dir.exists()) dir.mkdirs()
        return File(dir, fileName)
    }


}