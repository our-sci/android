package org.oursci.android.common.ui.survey.mutliplechoice

import org.oursci.android.common.BaseApplication
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceControl
import org.oursci.android.common.ui.survey.ControlViewModel

/**
 * Created by Manuel Di Cerbo on 22.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class MultipleChoiceViewModel(app: BaseApplication) : ControlViewModel<MultipleChoiceControl>(app) {

}