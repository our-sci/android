package org.oursci.android.common

import org.junit.Before
import org.junit.Test
import org.oursci.android.common.webapi.Api
import org.oursci.android.common.webapi.FarmOSClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber


/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 14.03.19.
 */
class FarmOSTest {


    private lateinit var farmOSClient: FarmOSClient
    private var oursciClient: Api = Retrofit.Builder().baseUrl("https://app.our-sci.net/api/")
            .addConverterFactory(MoshiConverterFactory.create())
            .build().create(Api::class.java)!!

    @Before
    fun init() {
        Timber.tag("FarmOSTest")
        Timber.plant(object : Timber.Tree() {
            override fun log(priority: Int, tag: String?, message: String?, t: Throwable?) {
                println("$tag: $message")
            }
        })
        farmOSClient = FarmOSClient("https://test.farmos.net", "Tester", "Organic1")
    }

    @Test
    fun fetchCredentials() {
        // TODO attach token to test
        val res = oursciClient.fetchTpCredentials("").execute()
        println(res.body()?.joinToString())
    }


    @Test
    fun extractAreas() {
        val areas = farmOSClient.fetchAllAreas() ?: throw Exception("can't fetch areas")
        println(areas.joinToString("\n"))
    }
}