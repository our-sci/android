package org.oursci.android.common

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.joda.time.DateTime
import org.joda.time.Period
import org.joda.time.format.PeriodFormatterBuilder
import org.junit.Before
import org.junit.Test
import org.oursci.android.common.survey.Palette
import org.oursci.android.common.webapi.Api
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.File
import java.io.FileInputStream
import java.util.*

/**
 * Created by Manuel Di Cerbo on 13.10.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */


class AnswerTest {

    private lateinit var api: Api

    @Before
    fun initApi() {
        api = Retrofit.Builder()
                .baseUrl("http://oursci-dryrun.us-east-2.elasticbeanstalk.com/api/")
                .addConverterFactory(MoshiConverterFactory.create())
                .build().create(Api::class.java)!!
    }


    @Test
    fun testDownload() {
        val output = File("./_common/sampledata/from_api.zip")
        val name = "bfa_reflectometer_droplet"
        val info = api.getScriptInformation(name).execute();

        if (!info.isSuccessful) {

        }

        val res = api.fetchScriptZip(name).execute()
        if (!res.isSuccessful) {
            System.err.println("error downloading: %s".format(res.message()))
        }

        res.body()?.let {
            println("body available, len: %d".format(it.contentLength()))
            it.byteStream().buffered().use { buffered ->
                output.outputStream().use {
                    buffered.copyTo(it)
                }
            }
        }

    }

    @Test
    fun testUpload() {
        val results = listOf(
                "{hello:world}",
                "{or:not}"
        ).mapIndexed { index: Int, s: String ->
            MultipartBody.Part.createFormData("files", "result_%02d.json".format(index), RequestBody.create(MediaType.parse("text/xml"), s))
        }

        val survey = MultipartBody.Part.createFormData("files", "survey.xml", RequestBody.create(MediaType.parse("text/xml"), "<xml>it is XML</xml>"))
        val override = MultipartBody.Part.createFormData("override", "true")

        api.uploadSurveyResult(MultipartBody.Part.createFormData(null, ""), survey, override, results).execute().let {
            if (!it.isSuccessful) {
                System.err.println("error uploading: %s".format(it.errorBody()?.string()))
            }

            println("success: %s".format(it.body()?.string() ?: "<empty body>"))
        }
    }


    @Test
    fun testAnswer() {
        println("hello")
        println("hey,hello,what,is,on,".trimEnd(','))

        var input = "/data/sample_id:label"
        input = input.replaceFirst("/data/", "").let {
            if (it.endsWith(":label")) {
                it.substring(0, it.length - ":label".length)
            } else {
                it
            }
        }

        println(input)


        val found = false
        val filePath = "file,/measurements/to/a/file.json,safkjaslfkjasfl"
        val startMarker = "file,"
        val startIdx = startMarker.length
        val path = if (!found && filePath.startsWith(startMarker)) {
            val idx = filePath.indexOf(',', startIdx)
            if (idx > 0) {
                filePath.substring(startIdx, idx)
            } else {
                filePath.substring(startIdx)
            }
        } else {
            ""
        }

        println(path)
    }


    @Test
    fun series() {


        val n = 4
        val r = 16

        (0..30).forEach { i ->
            val c = i % r
            val idx = ((c * n) + c / n) % r
            println(idx)
        }
    }

    data class Vendor(val id: Int, val name: String)
    data class Product(val id: Int, val name: String, val vendor: Vendor)

    fun info(vid: Int, pid: Int): String {
        val vendor = productList.map {
            it.vendor
        }.firstOrNull {
            vid == it.id
        }

        val product = productList.firstOrNull {
            it.id == pid && it.vendor.id == vid
        }

        return product?.let {
            "%s, %s".format(it.vendor.name, it.name)
        } ?: vendor?.let {
            "%s, <unknown product>".format(it.name)
        } ?: "<unknown vendor>, <unknown prduct>"

    }

    @Test
    fun testusbIds() {
        productList
    }


    private val productList by lazy {
        var currentVendor: Vendor? = null
        val list: MutableList<Product> = mutableListOf()
        FileInputStream("./_common/sampledata/usb-ids.txt").bufferedReader().useLines { lines: Sequence<String> ->
            lines.forEach {
                if (it.startsWith("\t")) {
                    val vendor = currentVendor ?: return@forEach
                    try {
                        Product(it.substring(1, 5).toInt(16),
                                it.substring(7, it.length), vendor)
                    } catch (e: Exception) {
                        null
                    }?.let {
                        list.add(it)
                    }
                } else {
                    currentVendor = try {
                        Vendor(it.substring(0, 4).toInt(16), it.substring(6, it.length))
                    } catch (e: Exception) {
                        null
                    }
                }
            }
        }

        list
    }


    @Test
    fun splitTest() {
        val formatter = PeriodFormatterBuilder()
                .appendYears().appendSuffix("#years ")
                .appendMonths().appendSuffix("#months ")
                .appendDays().appendSuffix("#days ")
                .appendHours().appendSuffix("#h ")
                .appendMinutes().appendSuffix("#min ")
                .appendSeconds().appendSuffix("#s ")
                .printZeroNever()
                .toFormatter()


        val time = DateTime().minus(Period.days(5)).minus(Period.minutes(2)).minus(Period.months(5)).minus(Period.years(2))

        val ago = formatter.print(Period(time, DateTime())).split(" ").take(2).joinToString(" ") {
            it.split("#").joinToString(" ")
        }
        println(ago)
    }


}
