package org.oursci.android.soilsci.ui

import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.ui.intro.BaseIntroViewModel
import org.oursci.android.common.vo.IntroSlide
import org.oursci.android.soilsci.App
import org.oursci.android.soilsci.R

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 25.07.17.
 */

class SoilIntroViewModel(app: App) : BaseIntroViewModel(app) {

    val loginLiveData = ConsumableLiveData<(Boolean) -> Unit>()

    var loginSlide = IntroSlide(R.drawable.ic_navigate_next_white_24dp
            , "Connect", "In order to store measurements use an existing Social Network account " +
            "or Sign up to the SoilSci and OurSci network", "Log in or Sign up", {

        loginLiveData.value = {
            success ->
            if (success) {
                val v = slides.value
                v?.let {
                    v[3] = loginSuccessSlide()
                    slides.value = v
                }
            }
        }
    })

    fun loginSuccessSlide() =
            IntroSlide(R.drawable.ic_navigate_next_white_24dp,
                    "Greetings ${app.user.username}", "you are successfully Logged in, continue to get started.")


    init {
        val slideArray = arrayOf(
                IntroSlide(R.drawable.ic_carrot, "Bio Nutrient Meter",
                        "Welcome to the Bio Nutrient Meter!\n\n" +
                                "This Intro will tell you the basics you need to know to get started."),
                IntroSlide(R.drawable.flowers
                        , "Food Healthy = Healthy Food", "The Nutrient Meter allows you to research and contribute measurements" +
                        " of vegetable and fruit. Thanks to new data, advancement in research becomes possible"),
                IntroSlide(R.drawable.user
                        , "Collecting in Collective", "When members of the community upload data you will get access" +
                        " to it.\n\n" +
                        "The other way around your data will be accessible to all members of the community"),
                if (loggedIn()) loginSuccessSlide() else loginSlide,
                IntroSlide(R.drawable.ic_navigate_next_white_24dp
                        , "Supercharge the App", "Mark your data with Geo Location information for later evaluation.\n\n" +
                        "The App will ask you for permission improve your data."))

        slides.value = slideArray
    }

    fun loggedIn(): Boolean {
        return !app.user.username.isEmpty()
    }
}