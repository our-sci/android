# Our-Sci Android Application Repo

ODK Build (build.opendatakit.org) is used to create forms used in the Our Sci Android application.  Our Sci's app supports some, but not all, of the native ODK form functionality in terms of question logic and question types.  Below is a description of the supported features.

## Supported ODK question types

- text input
- Choose one
- Select multiple
- text input w/ auto-generated dropdown list
  - add more details here!
- Numeric
  - whole number, decimal
- Date/time
  - full date, year/month, year
- Measurement scripts
  - to use measurement scripts, in your form set question type -> `media`, kind -> `image`
  - write in `hint` whatever you want to add as a hint, followed by `;` then the slug (`id` in manifest) of the script that you're running
  - use VS code dev environment to create script (clone https://gitlab.com/our-sci/measurement-scripts, create new folder, edit sensor.js, processor.js, and manifest.json, note `id` field in manifest.json must be unique - using same `id` will replace that script!)
  - in VS code `build tasks` -> `build project`, then copy from `dist` folder sensor.js, processor.js, manifest.js into https://app.our-sci.net/#/script/create to save.
  - note that forms are updated on android devices when you click on `forms` -> form name - this refreshes the form and any scripts associated with the form

## Supported ODK logic types

* if/then
* groups