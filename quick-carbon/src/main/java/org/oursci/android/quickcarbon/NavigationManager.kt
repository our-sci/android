package org.oursci.android.quickcarbon

import org.oursci.android.common.BaseApplication
import org.oursci.android.common.BaseNavigationManager
import org.oursci.android.common.ui.welcome.WelcomeFragment
import org.oursci.android.quickcarbon.ui.MainFragment
import org.oursci.android.quickcarbon.ui.SettingsFragment
import org.oursci.android.quickcarbon.ui.AppIntroFragment
import timber.log.Timber

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 02.07.17.
 */
class NavigationManager(mainContainer: Int)
    : BaseNavigationManager(mainContainer) {


    fun intro(animate: Boolean) {
        Timber.d("moved to ic_intro")

        if (animate) {
            mainActivity!!.supportFragmentManager!!.popBackStack("intro", 0)
            return
        }


        currentFragment()?.let {
            Timber.d("current fragment: %s", it::class)
//            mainActivity?.supportFragmentManager?.beginTransaction()
//                    ?.remove(currentFragment())?.commit()
        }

        if (currentFragment() == null) {
            Timber.d("currentfragment is null")
        }

        val introFragment = AppIntroFragment()
        introFragment.nav(name = "intro")
    }

    fun settings() {
        val settingsFragment = SettingsFragment()
        settingsFragment.nav(name = "settings")
    }

    fun main() {
        val mainFragment = WelcomeFragment()
        mainFragment.nav(name = "home", disableEntryAnimation = true)
    }


    companion object {
        fun from(app: BaseApplication): NavigationManager = app.navigationManager as NavigationManager
    }
}