package org.oursci.android.quickcarbon.ui

import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.ui.intro.BaseIntroViewModel
import org.oursci.android.common.vo.IntroSlide
import org.oursci.android.quickcarbon.App
import org.oursci.android.quickcarbon.R

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 25.07.17.
 */

class AppIntroViewModel(app: App) : BaseIntroViewModel(app) {

    val loginLiveData = ConsumableLiveData<(Boolean) -> Unit>()

    var loginSlide = IntroSlide(R.drawable.ic_quick_carbon_splash
            , "Connect", "To start contributing log in " +
            "or sign up to the Quick Carbon network", "Log in or Sign up", {

        loginLiveData.value = {
            success ->
            if (success) {
                val v = slides.value
                v?.let {
                    v[2] = loginSuccessSlide()
                    slides.value = v
                }
            }
        }
    })

    fun loginSuccessSlide() =
            IntroSlide(R.drawable.ic_quick_carbon_splash,
                    "Greetings ${app.user.username}", "you are successfully Logged in, continue to get started.")


    init {
        val slideArray = arrayOf(
                IntroSlide(R.drawable.ic_quick_carbon_splash, "Quick Carbon",
                        "Rapid, Landscape-Scale Soil Carbon Assessment"),
                if (loggedIn()) loginSuccessSlide() else loginSlide,
                IntroSlide(R.drawable.ic_quick_carbon_splash
                        , "Supercharge the App", "Mark your data with Geo Location information for later evaluation.\n\n" +
                        "The App will ask you for permission improve your data."))

        slides.value = slideArray
    }

    fun loggedIn(): Boolean {
        return !app.user.username.isEmpty()
    }
}