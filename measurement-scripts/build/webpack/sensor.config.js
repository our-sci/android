var webpack = require('webpack');

module.exports = {
  externals: {
    // require("jquery") is external and available
    //  on the global var jQuery
    "serialport": "SerialPort",
    "fs" : "fs",
    "./config/port.json" : "serialDevice",
    "../config/port.json" : "serialDevice",
    "../data/result-prototype.json" : "d"        
  },
  resolve : {
    modules : [ "./build/node_modules" ]
  }
};

