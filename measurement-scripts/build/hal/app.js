module.exports = {
    result: function (res) {
        var tmp = JSON.stringify(res, null, 2);

        if (!isInBrowser()) {
            console.log("saving result to ./data/result.json:\n" + tmp);
            var fs = require('fs');
            if (!fs.existsSync("./data")) {
                fs.mkdirSync("./data")
            }
            fs.writeFileSync("./data/result.json", tmp);
            process.exit(0);
        } else {
            android.result(tmp);
        }

    },
    progress: function (pct) {
        if (!isInBrowser()) {
            console.log("progress is " + pct + " %")
        } else {
            android.progress(pct)
        }

    },
    onSerialDataIntercepted: function (data) {
        console.log("serial data intercepted: " + data);
        if (isInBrowser()) {
            android.onSerialDataIntercepted(data);
        }
    },
    csvExport: function (key, value) {
        console.log("exporting key value pair to csv: " + key + " => " + value);
        if (isInBrowser()) {
            if (typeof android == 'undefined') {
                console.log("exporting " + key + " => " + value)
            } else {
                android.csvExport(key, value);
            }
        }
    },
    csvExportValue: function (value) {
        console.log("exporting value to CSV " + value);
        if (isInBrowser()) {
            if (typeof android == 'undefined') {
                console.log("exporting " + value)
            } else {
                android.csvExport(value);
            }
        }
    },
    getRequest: function(url){
        if(isInBrowser()){
            if (typeof android == 'undefined') {
                console.log("get request " + url)
            } else {
                return android.getRequest(url);
            } 
        }
    },
    getAnswer: function(dataName){
        if(isInBrowser()){
            if (typeof android == 'undefined') {
                console.log("get data name " + dataName)
                return null
            } else {
                return android.getAnswer(dataName);
            } 
        }
    },
    save: function(){
        if(isInBrowser()){
            if (typeof android == 'undefined') {
                console.log("saving env")
                return null
            } else {
                return android.save();
            } 
        }
    }
}


function isInBrowser() {
    if (typeof window === 'undefined') {
        return false;
    }
    return true
}

