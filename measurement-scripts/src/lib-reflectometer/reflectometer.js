module.exports.object = pulse({
    object_type: "object",
    brightness: [800, 800, 800, 800, 800, 700, 600, 50, 50, 50],
    recall: ["userdef[0]", "userdef[1]", "userdef[2]", "userdef[3]", "userdef[4]", "userdef[5]", "userdef[6]", "userdef[7]", "userdef[8]", "userdef[9]", "userdef[10]", "userdef[11]", "userdef[12]", "userdef[13]", "userdef[14]", "userdef[15]", "userdef[16]", "userdef[17]", "userdef[18]", "userdef[19]"]
});
module.exports.cuvette = pulse({
    object_type: "cuvette",
    brightness: [1100, 1100, 1100, 1100, 1100, 750, 650, 60, 60, 60],
    recall: ["userdef[20]", "userdef[21]", "userdef[22]", "userdef[23]", "userdef[24]", "userdef[25]", "userdef[26]", "userdef[27]", "userdef[28]", "userdef[29]", "userdef[30]", "userdef[31]", "userdef[32]", "userdef[33]", "userdef[34]", "userdef[35]", "userdef[36]", "userdef[37]", "userdef[38]", "userdef[39]"]
});
module.exports.droplet = pulse({
    object_type: "droplet",
    brightness: [800, 800, 800, 800, 800, 700, 600, 50, 50, 50],
    recall: ["userdef[40]", "userdef[41]", "userdef[42]", "userdef[43]", "userdef[44]", "userdef[45]", "userdef[46]", "userdef[47]", "userdef[48]", "userdef[49]", "userdef[50]", "userdef[51]", "userdef[52]", "userdef[53]", "userdef[54]", "userdef[55]", "userdef[56]", "userdef[57]", "userdef[58]", "userdef[59]"]
});
// Calibration protocols for each object type
module.exports.object_shiney = pulse({
    brightness: [800, 800, 800, 800, 800, 700, 600, 50, 50, 50],
    object_type: "object",
    calibration: "yes"
});
module.exports.object_black = pulse({
    brightness: [800, 800, 800, 800, 800, 700, 600, 50, 50, 50],
    object_type: "object",
    calibration: "yes"
});
module.exports.cuvette_shiney = pulse({
    brightness: [1100, 1100, 1100, 1100, 1100, 750, 650, 60, 60, 60],
    object_type: "cuvette",
    calibration: "yes"
});
module.exports.cuvette_black = pulse({
    brightness: [1100, 1100, 1100, 1100, 1100, 750, 650, 60, 60, 60],
    object_type: "cuvette",
    calibration: "yes"
});
module.exports.droplet_shiney = pulse({
    brightness: [800, 800, 800, 800, 800, 700, 600, 50, 50, 50],
    object_type: "droplet",
    calibration: "yes"
});
module.exports.droplet_black = pulse({
    brightness: [800, 800, 800, 800, 800, 700, 600, 50, 50, 50],
    object_type: "droplet",
    calibration: "yes"
});


module.exports.pulse = function(arg){
    return pulse(arg);
}

function pulse(options) {

    if (options == null) {
        options = {};
    }

    var args = {
        pulses: options.pulses || 60,
        pulse_length: options.pulse_length | 7,
        pulse_distance: options.pulse_distance || 1500,
        order: options.order || [3, 4, 5, 9, 6, 7, 8, 1, 2, 10],
        brightness: options.brightness || [1000, 1000, 1000, 1000, 1000, 700, 600, 50, 50, 50],
        detectors: options.detectors || [1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
        dac_lights: options.dac_lights || 1,
        averages: options.averages || 5,
        protocols: options.protocols || 1,
        recall: options.recall || [],
        object_type: options.object_type || "object",
        calibration: options.calibration || "no"
    }


    var proto = {
        pulses: [],
        pulse_length: [],
        pulse_distance: [],
        pulsed_lights: [],
        pulsed_lights_brightness: [],
        detectors: [],
        dac_lights: 1,
        averages: 5,
        protocols: 1,
        object_type: "object",
        calibration: "no",
        protocols: 1,
        recall: []
    }

    for (var i = 0; i < 10; i++) {
        proto.pulses.push(args.pulses);
        proto.pulse_length.push([args.pulse_length]);
        proto.pulse_distance.push(args.pulse_distance);
        proto.pulsed_lights.push([args.order[i]])
        proto.pulsed_lights_brightness.push([args.brightness[i]])
        proto.detectors.push([args.detectors[i]])
    }

    proto.dac_lights = args.dac_lights
    proto.averages = args.averages
    proto.protocols = args.protocols
    proto.recall = args.recall
    proto.calibration = args.calibration
    proto.object_type = args.object_type

    console.log(JSON.stringify([proto], null, 2));

    return [proto];

    //    console.log(testJSON); 
    //    return [testJSON];
}