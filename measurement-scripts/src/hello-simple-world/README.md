# How to write your first Measurement Script

Welcome to the entry point to data acquisition and processing within the Our Sci platform. This first example will show, how data can be sent from an Arduino compatible development device and further processed inside an Android application.

The Our Sci platform supports quick hard- and software development for the purpose of collecting, processing and interpreting data of measurements. As a key feature, "measurement scripts" allow for rapid prototyping and at the same time keeping processing platform independant.

This first example aims to demonstrate how to get started with writing "measurement scripts", the idea behind them and setting up the development environment of Our Sci.

## What is a "Measurement Script"?

The goal of "measurement scripts" is to tell the Android application how to acquire data from a source (for instance an Arduino board with a sensor in this case). Once the data has been received by the "sensor" part of the script, it is then passed to the "processor" part, which visualizes the data to the user.

This measurement script is executed inside the Android application. Therefore with the Our Sci platform, it is not necessary to write or modify an Android application for the sole purpose of acquiring, processing and storing arbitrary data collected from external hardware.

### Sensor Script (sensor.js)

The sensor script is the first entry point into a measurement. It's sole purpose is to read out data, store it within a JS json object and tell the Android application when it is finished, passing the result with `app.result(data)`. The contents of the `data` object passed, will then be stored into a file, so that it remains available for later processing.

The stored `data` onbject serves as the basis for the processor script `processor.js`.


### Processor Script (processor.js)

The processor script has the task of visualizing and computing results from the data stored by the `sensor.js` script. Therefore it is **stateless**.

Attached to the processor script is a WebView on Android, allowing for full customization of visualizing data. Possibilites range from a simple graph to a custom dashboard like experience, allowing for further user interaction.

### Manifest, adding meta information

In order for the user to see what the script does and how it is called, the `manifest.json` file allows to commit additional information to the measurement script and builds the third and last pillar beside `sensor.js` and `processor.js`.

```
{
    // the ID which is used inside other components such as surveys
    // in order to reference the measurement script
    "id" : "hello-simple-world",

    // The name which is used to display the script
    "name" : "A Hello World Script",

    // The description for visualization
    "description" : "Simple Script that connects to an Arduino to 
    acquire a measurement",

    // a version to distinguish updates
    "version" : "1.0",

    // the text for the button in the Android app
    // which starts the sensor.js script
    "action" : "Run Measurement",

    // wether or not the sensor scripts prompts the user
    // to first connect to a device before running
    // (for instance, to do a POST or GET request to acquire data
    // a device connection is not necessary)
    "requireDevice" : "true"
}
```

### Concrete example for a sensor and processor script

Let's take a look at a possible example for a `measurement script` consisting of `sensor.js` and `processor.js`.

*Measuring RMS (root mean square) of a Volatage Signal with an Arduino*

Use the Arduino to sample Voltage at an ADC input, calculate RMS and display it / save it to a measurement and survey.

*Tasks of the `sensor script`, this is only pseudo code*

```
// write some data to serial in order to tell the Arduino to start measureing
device.serialWrite("start");

// read from serial and parse data as JSON
// contents would be something like this {"samples" : [0, 1023, 500, 720 ...]}
var data = device.readLine();
var json = JSON.stringify(data);


// tell the application that we are done
// and store the output

app.result(json);
```

The collected data samples are now stored inside a file and made accessible in the processor.

*Task of the `processor script`, again only pseudo code*

```
// First show a plot with the samples inside the webview
addPlot(result.samples);

// Now process and calculate RMS
var rms = calculateRMS(result.samples);

// Display value inside the webview
info("The RMS is "+rms);

// export the value into a CSV for later download
app.csv("rms", rms);

// tell the Android app we are done
app.save();
```

