serialConfig = {
    port: "/dev/ttyACM0",
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: "none"
}


serial = require("./lib/serial.js").Serial;
app = require("./lib/app.js").App;
utils = require("./lib/utils.js");

(async function () {

    // use await keyword with sleep
    await utils.sleep(500);


    // tell arduino to start
    serial.write("a");

    // read incoming json
    var res = await serial.readJson();

    // again one more round
    serial.write("a");

    
    var expected = 1024;
    var count = 0;

    // this time get notified for every value
    res = await serial.readStreamingJson("data[*]", function (item) {
        console.log("item " + count + " / " + expected + "  = " + item);
        count++;
    });

    console.log("res " + JSON.stringify(res, 0, 2));

    app.result(res);
    
})();
