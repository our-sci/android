var $ = require("jquery");
sprintf = require("sprintf-js").sprintf;
app = require("./lib/app.js").App;
ui = require("./lib/ui.js");
math = require("./lib/math.js");

require("./css/style.css");
require("materialize-css/dist/css/materialize.min.css");

require("chartist/dist/chartist.min.css");
require("chartist-plugin-fill-donut");
require("materialize-css");


if (typeof result === "undefined") {

    // the result is loaded from the fs, allowing to debug
    // actual data from the device

    try {
        result = require("../data/result.json");
    } catch (err) {
        result = {
            data: [1, 3, 2, 1, 3, 2, 2, 2, 5]
        };
    }
}


var len = result.data.length;

var voltage = [];
for (var i = 0; i < len; i++) {
    var v = result.data[i] / 1024.0 * 3.3; // convert to [V] unit
    voltage.push(v);
}

// plot first 50 measured voltage levels
var data = {
    series: [voltage.slice(0, 50)]
};
ui.plot(data, "ADC Voltage in [V]");


// calculate rms
var rms = 0;
for (var i = 0; i < len; i++) {
    var sqr = voltage[i] * voltage[i];
    rms += 1 / len * sqr;
}
// create donut with rms value, just set the meter to 75%
ui.donut("RMS", sprintf("%.3f", rms), 0.75, "multiline_chart");


rms = Math.sqrt(rms);
avg = 0;
// calculate average
for (var i = 0; i < len; i++) {
    avg += voltage[i] / len;
}
ui.donut("Average",  sprintf("%.3f", avg), avg / 3.3, "pie_chart");


app.csvExport("avg", avg);
app.csvExport("rms", rms);

ui.info("average calculated", "average has been calcuatedand exported");
ui.info("rms calculated", "rms has been calcuatedand exported");
ui.warning("rms out of range", sprintf("rms %.3f seems to be out of range", rms));
ui.error("avg looks weird", sprintf("avg %.3f is clearly funky", avg));



app.save();