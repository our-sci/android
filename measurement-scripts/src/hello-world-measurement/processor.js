
var $ = require('jquery')
var Plotly = require('plotly.js/lib/core');
var Math = require('./lib/math.js');

// add styles to ../css/style.css, imports will be resolved
require("./css/style.css");

Math.MathLINREG([1,3,1], [1,1,1]);

// result is set on the android device
// depending on the success of the reading it could still
// be empty, however it will always be defined
if (typeof result == "undefined") {

    // the result is loaded from the fs, allowing to debug
    // actual data from the device

    try {
        result = require("./data/result.json");
    } catch (err) { 
        result = require("./data/result-prototype.json");
    }
}

// following loop creates a couple of plots
// see the online documentation of plotly for details
// also in order for this to work, the processor.js
// module needs to be compiled with the build script
// buildscripts/build-processor.js

var container = $("<div></div>");
container.attr("id", "message-container");
container.prependTo($("body"));

info("measurement done");
//warning("ohoh there is a problem");
//error("however this is fatal!");

//info("what about this?");

sample_values_raw = result.sample[0].data_raw.slice(0,10);

warning("dangerous!? _ " + Math.MathROUND(5.45346346, 2) + "," + sample_values_raw[1] + "," + sample_values_raw[2]);
warning("dangerous!? _ ");

for (var i = 0; i < 1; i++) {
    var plot = document.createElement("div")
    plot.setAttribute("class", "plot");
    document.body.appendChild(plot)


    Plotly.plot(plot, [{
        y: result.sample[0].data_raw
    }], {
            margin: {
                t: 0,
                pad: 0,
                l: 40,
                r: 40,
                b: 40
            }
        });
}


// jquery use
$(".plot").css("margin-bottom", "16px");

function info(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message");
    var t = $("<p></p>").appendTo(info);
    t.text(text);
}

function warning(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message warning");
    var t = $("<p></p>").appendTo(info);
    t.text(text);}

function error(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message error");
    var t = $("<p></p>").appendTo(info);
    t.text(text);
}
