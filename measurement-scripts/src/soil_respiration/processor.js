
var $ = require('jquery')
var Plotly = require('plotly.js/lib/core');
var Math = require('mathjs');   // javascript math library
var MathMore = require("./lib/math.js");  // specialty functions library
app = require("../../build/hal/app.js");


// add styles to ../css/style.css, imports will be resolved
require("./css/style.css");



// Create functions for displaying individual values text or numbers
// info(), warning(), and error()

$(".plot").css("margin-bottom", "16px");


function csv(key, value) {
    app.csvExport(key, value)
}
function save(){
    app.save()
}

function info(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message");
    var t = $("<p></p>").appendTo(info);
    t.text(text);
}


function warning(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message warning");
    var t = $("<p></p>").appendTo(info);
    t.text(text);
}

function error(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message error");
    var t = $("<p></p>").appendTo(info);
    t.text(text);
}



/////////////////////////////////////////////////////////
// result is set on the android device
// depending on the success of the reading it could still
// be empty, however it will always be defined
if (typeof result == "undefined") {

    // the result is loaded from the fs, allowing to debug
    // actual data from the device

    try {
        result = require("./data/result.json");
    } catch (err) {
        result = require("./data/result-prototype.json");
    }
}



//////////////// NOTES ABOUT FUNCTIONALITY ///////////////
/*
FIXED! JSON.stringify() You also cannot string values together into a single 'info' over time (as you could with output)... we may need this
Use the toString() function to print an array - like this < info("this output: " + anArray.toString())
FIXED! The div needs to overflow in case things are  really long... is let's make some div options which overflow

What about sending a command, receiving results, sending new command based on results, etc.?
Definitely important for calibration protocols (calibrate+43.23+23432 ... )


////////////////////  CREATE MACRO  //////////////////////
/*
The data is contained in result.sample.  Each protocol is a separate element in the result.sample array
for example, to access the first protocol you ran, use result.sample[0], the second is result.sample[1], etc.
is it possible to prompt the user with a question based

This macro does a few things:
1) it separates out each wavelength based on the # of pulses per wavelength
2) it then subtracts the high 
2) it addresses LED heating by ignoring the first 2/3 of the pulses to reduce standard deviation caused by heating
3) it then tries to flatten the remining 1/3 of pulses
4) then it chooses the median, calculates standard deviation, # of bits for those pulses. 
5a) then saves the raw median to the device for the shiney (0 - 9) or black (10 - 19) values in the calibration version
5b) then it calculates the median, stdev and bits in the normal measurement version
6) note that bits_actual is the bits accounting for the actual range (max reflectance - min reflectance) as compared to bits which assumes the full 16 bit range.  bits_actual is a more accurate representation of the quality of the overall setup (hardware + case + optics)
*/


/////////////////////////////////////////////////////////
// Set up HTML objects (div's) to display macro outputs in HTML
// accessible in the /out folder as processor.html
var container = $("<div></div>");
container.attr("id", "message-container");
container.prependTo($("body"));
// Make easy to remember names for frequently called variables from the json
json = result.sample[0];
data = json.set;
/////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////
// Get the CO2 and time data in arrays so we can do stuff with them.  Make a 2nd time variable which is zeroed to the initial time point (for graphing)
// Also, check to make sure that the 'time' and 'co2' variables are there and valid - if not, just skip that value.
var period = 5 // number of seconds between samples if the device isn't outputing time values
var x_time = [];
var x_time_zero = [];
var co2 = [];
var missing_data = 0;
for (var i = 0; i < data.length; i++) {
//    if (typeof data[i].time != "undefined" && typeof data[i].co2 != "undefined") {
    if (typeof data[i].co2 != "undefined" && data[i].co2 < 10001) {
//        x_time.push(i*5);
//        x_time_zero.push((data[i].time - data[0].time)/1000);
        x_time_zero.push(period*i)
        co2.push(data[i].co2);
    }
    else {
        missing_data ++;
    }
}
// for some reason the first set contains no co2, so there's always one missing data point
if (missing_data > 1) {
    warning("there were: " + missing_data-1 + " co2 data points missing")
    warning("Some co2 data points were missing, check the co2 by time graph and raw data to identify problem")
}
/////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////
// Get the CO2 and time data in arrays so we can do stuff with them.  Make a 2nd time variable which is zeroed to the initial time point (for graphing)
var co2max = MathMore.MathMAX(co2);
var co2min = MathMore.MathMIN(co2);
var co2increase = co2max - co2min;
var Headspace = 460;
var Temp = 298;
var Rconstant = 82.05;
//conversion factor is assuming 50g soil (soil G x 1000)
var conversionfactor = 50000;
//converts to weight of C only and not of CO2
var conversionfactor2 = 12000;
var ugCgsoil = MathMore.MathROUND((((co2increase * Headspace) / conversionfactor) / (Temp * Rconstant)) * conversionfactor2);
if (co2max >= 10000) {
    warning("the CO2 level is too high and has saturated the sensor, try diluting the sample");
}
    info("ug C per g soil: " + ugCgsoil);
    info("CO2 increase: " + co2increase);
    info("Max CO2: " + co2max);
    info("Min CO2: " + co2min);

    info("time_array: " + x_time_zero);
    info("co2_array: " + co2);

    csv("ugCgsoil", ugCgsoil);
    csv("co2_increase", co2increase);
    csv("co2_max", co2max);
    csv("co2_min", co2min);
    csv("co2_array", co2);
    csv("time", x_time_zero);
/////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////
// Finally, graph the standard deviation at each wavelength the raw trace for the entire measurement (detector response by time)...
var layout = {
    title: "CO2 by Time",
    yaxis: { title: "CO2 ppm" },
    xaxis: { title: "Time (s)" }
}
    var trace = {
    x: x_time_zero,
    y: co2,
    mode: "lines"
}
var plotData = [trace]
var plot = document.createElement("div")
plot.setAttribute("class", "plot");
document.body.appendChild(plot)
Plotly.plot(plot, plotData, layout);
/////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////
// REALLY IMPORTANT!  save the resulting information to csv
save();