
var $ = require('jquery')
var Plotly = require('plotly.js/lib/core');

// add styles to ../css/style.css, imports will be resolved
require("./css/style.css");

// result is set on the android device
// depending on the success of the reading it could still
// be empty, however it will always be defined
if (typeof result == "undefined") {

    // the result is loaded from the fs, allowing to debug
    // actual data from the device

    try {
        result = require("./data/result.json");
    } catch (err) { 
        result = require("./data/result-prototype.json");
    }
}

var container = $("<div></div>");
container.attr("id", "message-container");
container.prependTo($("body"));

info("measurement done");


var plot = document.createElement("div")
plot.setAttribute("class", "plot");
document.body.appendChild(plot)


Plotly.plot(plot, [{
    y: result.data.slice(0, 128)
}], {
        margin: {
            t: 0,
            pad: 0,
            l: 40,
            r: 40,
            b: 40
        }
    });


/*
for (var i = 0; i < 4; i++) {
    var plot = document.createElement("div")
    plot.setAttribute("class", "plot");
    document.body.appendChild(plot)


    Plotly.plot(plot, [{
        y: result.data.slice(i*4096, i*4096+400)
    }], {
            margin: {
                t: 0,
                pad: 0,
                l: 40,
                r: 40,
                b: 40
            }
        });
}*/


// jquery use
$(".plot").css("margin-bottom", "16px");

function info(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message");
    var t = $("<p></p>").appendTo(info);
    t.text(text);
}

function warning(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message warning");
    var t = $("<p></p>").appendTo(info);
    t.text(text);}

function error(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message error");
    var t = $("<p></p>").appendTo(info);
    t.text(text);
}
