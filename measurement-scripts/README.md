# Measurement Scripts

Measurement scripts consist of two parts:

Sensorscript | sensor.js

Responsible for acquiring samples of the hardware using serial.read / serial.write.
Produces a result json
Stateful

Processorscript | processor.js
Displays the result with plots, calculates output / indicator data
Stateless

Both scripts are written in JavaScript and can be run within nodejs and / or the
browser.

Install NPM package manager for yor OS

First always run in this directory
$ npm install

# Debugging on the Desktop

Configure the serial interface
$ nano config.port.json

example content:

{
    "serialport" : "/dev/ttyACM0"
}

Run the sensor script src/sensor.js with node
$ node src/sensor.js

Output data will be stored in ./data/result.json

Display the processor output as a web page
$ buildscripts/build-processor.sh

Open in Browser
out/processor.html


# Testing the Measurementscript on Android

Build the zip
$ buildscripts/build-project.sh

Enable webserver on android in the settings

Copy the zip to Android (see the IP of the android device in the app settings)
curl -vvv [ANDROID_IP]:9095/upload_measurement --data-binary @out/measurement_script.zip